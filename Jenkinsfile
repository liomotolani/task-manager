def agentLabel
def branchName = BRANCH_NAME
if (branchName == "staging") {
    agentLabel = "staging-node"
} else if(branchName == "master") {
    agentLabel = "prod-node"
} else {
    agentLabel = "local-slave"
}
pipeline {

    agent {label agentLabel}
    tools {
        maven 'maven'
        jdk 'jdk'
    }
    stages {
        stage ('Initialize') {
            steps {
                script {
                    def msg = "STARTED: ${env.JOB_NAME} #${env.BUILD_NUMBER}: \n${env.BUILD_URL}"
                    slackSend color: '#D4DADF', message: msg
                }
                sh '''
                    echo "PATH = ${PATH}"
                    echo "JAVA_HOME = ${JAVA_HOME}"
                    echo "M2_HOME = ${M2_HOME}"
                '''
            }
        }

        stage('Test') {
            steps{
                sh 'rm -f ./src/main/resources/application.properties'
                sh 'cp /opt/tm.application-test.properties ./src/main/resources/application.properties'
                sh "mvn test"
            }
        }

        stage ('Build') {
            steps {
                sh 'rm -f ./src/main/resources/application.properties'
                sh 'cp /opt/tm.application.properties ./src/main/resources/application.properties'
                sh 'mvn install -DskipTests'
            }
        }

        stage('Deployment') {
            when { anyOf { branch 'dev'; branch 'master'; branch 'staging' } }
            stages {
                   stage('Build & Push') {
                       steps{
                           script {
                               docker.withRegistry('https://registry.quabbly.com', 'quabbly-registry') {

                                   def customImage = docker.build("quabblytaskmanager-${env.BRANCH_NAME}:${env.BUILD_ID}")
                                   customImage.push('latest')
                               }
                           }
                       }
                   }
                   stage('Start Service') {
                       environment {
                           TAG = "${env.BRANCH_NAME}"
                       }
                       steps{
                           //sh "docker rm -f mongotask 2> /dev/null"
                           //sh "docker rm -f taskmanagerapi 2> /dev/null"
                           sh "docker-compose up -d"
                       }
                   }
            }

        }

    }
    post {
        success {
            slackSend color: '#BDFFC3', message: "SUCCESS: ${env.JOB_NAME} #${env.BUILD_NUMBER}:\n${env.BUILD_URL}"
        }

        failure {
            slackSend color: '#FF9FA1', message: "FAILURE: ${env.JOB_NAME} #${env.BUILD_NUMBER}:\n${env.BUILD_URL}"
        }

        unstable {
            slackSend color: '#FFFE89', message: "UNSTABLE: ${env.JOB_NAME} #${env.BUILD_NUMBER}:\n${env.BUILD_URL}"
        }


    }
}