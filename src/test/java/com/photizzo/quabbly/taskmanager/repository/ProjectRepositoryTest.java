///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.photizzo.quabbly.taskmanager.repository;
//
///**
// *
// * @author CROWN INTERACTIVE
// */
//
//import com.photizzo.quabbly.taskmanager.dto.input.ProjectInputDTO;
//import com.photizzo.quabbly.taskmanager.model.Project;
//import com.photizzo.quabbly.taskmanager.repositories.ProjectRepository;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//import org.junit.After;
//import org.junit.AfterClass;
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotNull;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//import org.springframework.test.context.junit4.SpringRunner;
//
//
//@RunWith(SpringRunner.class)
//public class ProjectRepositoryTest {
//
//    @Mock
//    private ProjectRepository projectRepository;
//    
//    
//    @BeforeClass
//    public  static void configuration() {
//        System.setProperty("spring.data.mongodb.uri", "mongodb://localhost:27017/quabbly_test");       
//    }
//    @Before
//    public void setup(){
//        createProject();
//    }
//    
//    @Test
//    public void whenFindByName_thenReturnProject() {
//        Project found = projectRepository.findByid(createProject().getId());
//
//        // then
//      assertNotNull(found.getName());
//        assertEquals(createProject().getName(),
//                found.getName());
//    }
//
//    private Project createProject(){
//        Project project = new Project("dummyProject", new Date());
//        projectRepository.save(project);
//        return project;
//    }
//    
//    @After
//    public void destroyAll(){
//        projectRepository.deleteAll();
//    }
//}
//
