package com.photizzo.quabbly.taskmanager.service;

import com.google.common.collect.Lists;
import com.photizzo.quabbly.taskmanager.config.TokenProvider;
import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.dto.input.CreateRoleInputDTO;
import com.photizzo.quabbly.taskmanager.dto.output.RoleResponseDTO;
import com.photizzo.quabbly.taskmanager.model.Role;
import com.photizzo.quabbly.taskmanager.repositories.RoleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class AdminServiceImplTest {
    @InjectMocks
    private AdminServiceImpl adminService;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private TokenProvider tokenProvider;

    private void initMocksForCreateRole(Role role) {

        when(roleRepository.save(any())).thenReturn(role);

        when(tokenProvider.getUserId()).thenReturn("USER_ID");

        when(tokenProvider.getTenantId()).thenReturn("TENANT_ID");
    }

    private CreateRoleInputDTO getCreateRoleInputDto(){
        CreateRoleInputDTO inputDTO = new CreateRoleInputDTO();
        inputDTO.setName("DEFAULT");
        inputDTO.setCapabilities(Lists.newArrayList("GENERAL"));
        return inputDTO;
    }

    @Test
    public void createRole_whenExecuted_andIdIsNull_returnsInternalErrorStatus() {

        Role role = new Role();

        initMocksForCreateRole(role);

        RoleResponseDTO obj = adminService.createRole(getCreateRoleInputDto());

        assertEquals(Status.INTERNAL_ERROR, obj.getStatus());

    }

    @Test
    public void createRole_whenExecuted_returnsRoleDtoWithSuccessStatus()  {

        Role role = new Role();
        role.setId("ID");

        initMocksForCreateRole(role);
        RoleResponseDTO obj = adminService.createRole(getCreateRoleInputDto());

        assertEquals(Status.CREATED, obj.getStatus());
    }

}
