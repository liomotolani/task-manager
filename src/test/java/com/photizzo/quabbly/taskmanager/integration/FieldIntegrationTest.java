package com.photizzo.quabbly.taskmanager.integration;

import com.google.gson.Gson;
import com.photizzo.quabbly.taskmanager.config.TokenProvider;
import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.dto.input.*;
import com.photizzo.quabbly.taskmanager.dto.output.ErrorResponseDTO;
import com.photizzo.quabbly.taskmanager.dto.output.FieldListResponseDTO;
import com.photizzo.quabbly.taskmanager.dto.output.FieldsResponseDTO;
import com.photizzo.quabbly.taskmanager.dto.output.ProjectResponseDTO;
import com.photizzo.quabbly.taskmanager.repositories.FieldsRepository;
import com.photizzo.quabbly.taskmanager.repositories.ProjectRepository;
import com.photizzo.quabbly.taskmanager.repositories.TaskRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FieldIntegrationTest extends AbstractIntegrationTest {
    private CreateTaskInputDTO createTaskInputDTO;
    private CreateProjectInputDTO createProjectInputDTO;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private FieldsRepository fieldsRepository;

    @Autowired
    private Environment env;


    @LocalServerPort
    private int localServerPort;

    @Mock
    private TokenProvider tokenProvider;

    private String createProjectUrl;

    private String createFieldsUrl;

    private String fetchFieldsUrl;

    private String updateFieldUrl;

    @BeforeClass
    public static void configuration() {
        System.setProperty("mongodb.host", "localhost");
        System.setProperty("mongodb.port", "27017");
    }

    @Before
    public void setUp(){
        createProjectUrl = "http://localhost:" + localServerPort + "/v1/admin/project/create";
        createFieldsUrl = "http://localhost:" + localServerPort + "/v1/project/{0}/fields/";
        fetchFieldsUrl ="http://localhost:" + localServerPort + "/v1/project/{0}/fields/";
        updateFieldUrl ="http://localhost:" + localServerPort + "/v1/project/{0}/field/";
        createAndlogInUser();

    }

    private ProjectResponseDTO createProject(){
        String projectName = "dummyProject";

        CreateProjectInputDTO projectInputDTO = new CreateProjectInputDTO();
        projectInputDTO.setName(projectName);

        HttpEntity projectEntity = new HttpEntity<>(projectInputDTO, headers);

        ResponseEntity<String> projectResponseEntity =
                restTemplate.postForEntity(createProjectUrl, projectEntity, String.class);
        assertEquals(HttpStatus.CREATED, projectResponseEntity.getStatusCode());

        ProjectResponseDTO responseDTO = new Gson().fromJson(projectResponseEntity.getBody(), ProjectResponseDTO.class);
        return responseDTO;
    }
    private ResponseEntity<String> createField(String name,String description,String type,String projectId){
        CreateFieldsInputDTO createFieldsInputDTO = new CreateFieldsInputDTO();
        createFieldsInputDTO.setName(name);
        createFieldsInputDTO.setDescription(description);
        createFieldsInputDTO.setType(type);

        HttpEntity fieldEntity = new HttpEntity<>(createFieldsInputDTO, headers);

        ResponseEntity<String> fieldResponseEntity =
                restTemplate.postForEntity(MessageFormat.format(createFieldsUrl, projectId), fieldEntity, String.class);
        return fieldResponseEntity;
    }

    @Test
    public void when_create_fields_with_valid_data_returns_successful() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Task";
        String description = "Welcome";
        String type = "money";
        ResponseEntity<String> fieldResponseEntity = createField(name,description,type,projectId);

        assertEquals(HttpStatus.CREATED, fieldResponseEntity.getStatusCode());
        FieldsResponseDTO fieldsResponseDTO = readValue(fieldResponseEntity.getBody(),  FieldsResponseDTO.class);

        assertNotNull(fieldsResponseDTO.getFields().getCreatedTime());
        assertNotNull(fieldsResponseDTO.getFields().getId());
        assertNotNull(fieldsResponseDTO.getFields().getFieldsSecret());
        assertNotNull(fieldsResponseDTO.getFields().getFieldsSecret().getName());
        assertNotNull(fieldsResponseDTO.getFields().getFieldsSecret().getDescription());
        assertNotNull(fieldsResponseDTO.getFields().getFieldsSecret().getType());

        assertEquals(name,fieldsResponseDTO.getFields().getFieldsSecret().getName());
        assertEquals(description,fieldsResponseDTO.getFields().getFieldsSecret().getDescription());
        assertEquals(type,fieldsResponseDTO.getFields().getFieldsSecret().getType().toLowerCase());

    }

    @Test
    public void when_create_fields_with_null_name_returns_valid_error() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = null;
        String description = "Welcome";
        String type = "number";
        ResponseEntity<String> fieldResponseEntity = createField(name, description, type, projectId);
        assertEquals(HttpStatus.BAD_REQUEST, fieldResponseEntity.getStatusCode());
        ErrorResponseDTO response = readValue(fieldResponseEntity.getBody(), ErrorResponseDTO.class);
        assertEquals(Status.FAILED_VALIDATION, response.getStatus());
        assertNotNull(response.getData());
        assertEquals(1, response.getData().size());
        ApiFieldError error = response.getData().get(0);
        assertEquals("name", error.getField());
        assertEquals("name is required", error.getMessage());
    }
    @Test
    public void when_create_fields_with_name_below_length_returns_valid_error() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "";
        String description = "Welcome";
        String type = "text";
        ResponseEntity<String> fieldResponseEntity= createField(name,description,type,projectId);
        assertEquals(HttpStatus.BAD_REQUEST, fieldResponseEntity.getStatusCode());
        ErrorResponseDTO response = readValue(fieldResponseEntity.getBody(), ErrorResponseDTO.class);
        assertEquals(Status.FAILED_VALIDATION, response.getStatus());
        assertNotNull(response.getData());
        assertEquals(1, response.getData().size());
        ApiFieldError error = response.getData().get(0);
        assertEquals("name", error.getField());
        assertEquals("name should be between 3 and 30 characters", error.getMessage());
    }
    @Test
    public void when_create_fields_with_name_above_length_returns_valid_error() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "AWEJDNDJNJNKSLNSDNDJKNJNFHNJSNJNSJKNNFDJDMSSKMSMKSJSJSNDNDNDJNDJN";
        String description = "Welcome";
        String type = "date";
        ResponseEntity<String> fieldResponseEntity= createField(name,description,type,projectId);
        assertEquals(HttpStatus.BAD_REQUEST, fieldResponseEntity.getStatusCode());
        ErrorResponseDTO response = readValue(fieldResponseEntity.getBody(), ErrorResponseDTO.class);
        assertEquals(Status.FAILED_VALIDATION, response.getStatus());
        assertNotNull(response.getData());
        assertEquals(1, response.getData().size());
        ApiFieldError error = response.getData().get(0);
        assertEquals("name", error.getField());
        assertEquals("name should be between 3 and 30 characters", error.getMessage());
    }

    @Test
    public void when_create_fields_with_description_below_length_returns_valid_error() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Well";
        String description = "";
        String type = "Date";
        ResponseEntity<String> fieldResponseEntity = createField(name, description, type, projectId);
        assertEquals(HttpStatus.BAD_REQUEST, fieldResponseEntity.getStatusCode());
        ErrorResponseDTO response = readValue(fieldResponseEntity.getBody(), ErrorResponseDTO.class);
        assertEquals(Status.FAILED_VALIDATION, response.getStatus());
        assertNotNull(response.getData());
        assertEquals(1, response.getData().size());
        ApiFieldError error = response.getData().get(0);
        assertEquals("description", error.getField());
        assertEquals("description should be between 3 and 50 characters", error.getMessage());
    }

    @Test
    public void when_create_fields_with_description_above_length_returns_valid_error() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Well";
        String description = "ASDDJJJDLKJFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF";
        String type = "Text";
        ResponseEntity<String> fieldResponseEntity = createField(name, description, type, projectId);
        assertEquals(HttpStatus.BAD_REQUEST, fieldResponseEntity.getStatusCode());
        ErrorResponseDTO response = readValue(fieldResponseEntity.getBody(), ErrorResponseDTO.class);
        assertEquals(Status.FAILED_VALIDATION, response.getStatus());
        assertNotNull(response.getData());
        assertEquals(1, response.getData().size());
        ApiFieldError error = response.getData().get(0);
        assertEquals("description", error.getField());
        assertEquals("description should be between 3 and 50 characters", error.getMessage());
    }

    @Test
    public void when_create_fields_with_null_type_returns_valid_error() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Well";
        String description = "Sing";
        String type = null;
        ResponseEntity<String> fieldResponseEntity = createField(name, description, type, projectId);
        assertEquals(HttpStatus.BAD_REQUEST, fieldResponseEntity.getStatusCode());
        ErrorResponseDTO response = readValue(fieldResponseEntity.getBody(), ErrorResponseDTO.class);
        assertEquals(Status.FAILED_VALIDATION, response.getStatus());
        assertNotNull(response.getData());
        assertEquals(1, response.getData().size());
        ApiFieldError error = response.getData().get(0);
        assertEquals("type", error.getField());
        assertEquals("type is required", error.getMessage());
    }


    @Test
    public void when_create_fields_with_invalid_type_returns_valid_error() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Well";
        String description = "Sing";
        String type = "jjjjj";
        ResponseEntity<String> fieldResponseEntity = createField(name, description, type, projectId);
        assertEquals(HttpStatus.BAD_REQUEST, fieldResponseEntity.getStatusCode());
        ErrorResponseDTO response = new Gson().fromJson(fieldResponseEntity.getBody(), ErrorResponseDTO.class);
        assertEquals(Status.FAILED_VALIDATION, response.getStatus());
        assertNotNull(response.getData());
        assertEquals(1, response.getData().size());
        ApiFieldError error = response.getData().get(0);
        assertEquals("type", error.getField());
        assertEquals("type can either be number or text or money or date", error.getMessage());
    }

    @Test
    public void when_fetch_Fields_Return_ValidData(){
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        HttpEntity entity = new HttpEntity<>(headers);


        ResponseEntity<String> responseEntity =
                restTemplate.exchange(MessageFormat.format(fetchFieldsUrl,projectId), HttpMethod.GET, entity, String.class);

        assertEquals(HttpStatus.OK ,responseEntity.getStatusCode());

        FieldListResponseDTO fieldListResponseDTO =  new Gson().fromJson(responseEntity.getBody(), FieldListResponseDTO.class);

        assertNotNull(fieldListResponseDTO);
        assertEquals(Status.SUCCESS ,fieldListResponseDTO.getStatus());
    }

    @Test
    public void when_update_field_with_name_returns_successful(){
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Well";
        String description = "Sing";
        String type = "text";
        ResponseEntity<String> fieldResponseEntity = createField(name, description, type, projectId);
        assertEquals(HttpStatus.CREATED, fieldResponseEntity.getStatusCode());
        FieldsResponseDTO fieldResponseDTO = new Gson().fromJson(fieldResponseEntity.getBody(), FieldsResponseDTO.class);
        String fieldId = fieldResponseDTO.getFields().getId();
        String updatedName = "bright";
        ResponseEntity<String> updateFieldResponseEntity = updateName(projectId,fieldId,updatedName);
        assertEquals(HttpStatus.OK ,  updateFieldResponseEntity.getStatusCode());
        FieldsResponseDTO updateFieldResponseDTO = new Gson().fromJson( updateFieldResponseEntity.getBody(),  FieldsResponseDTO.class);
        assertEquals(updatedName,updateFieldResponseDTO.getFields().getFieldsSecret().getName());
    }

    private ResponseEntity<String> updateName(String projectId, String fieldId, String name) {
        UpdateFieldInputDTO updateFieldInputDTO = new UpdateFieldInputDTO();
        updateFieldInputDTO.setName(name);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("name");
        updateFieldInputDTO.setUpdatedFields(updatedFields);

        HttpEntity updateFieldEntity = new HttpEntity<>(updateFieldInputDTO, headers);

        ResponseEntity<String> updateFieldResponseEntity =
                restTemplate.exchange(MessageFormat.format(updateFieldUrl, projectId) + fieldId, HttpMethod.PUT, updateFieldEntity ,String.class);
        return updateFieldResponseEntity;
    }

    @Test
    public void when_update_field_with_description_returns_successful(){
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Well";
        String description = "Sing";
        String type = "text";
        ResponseEntity<String> fieldResponseEntity = createField(name, description, type, projectId);
        assertEquals(HttpStatus.CREATED, fieldResponseEntity.getStatusCode());
        FieldsResponseDTO fieldResponseDTO = new Gson().fromJson(fieldResponseEntity.getBody(), FieldsResponseDTO.class);
        String fieldId = fieldResponseDTO.getFields().getId();
        String updatedDescription = "bright and beautiful";
        ResponseEntity<String> updateFieldResponseEntity = updateDescription(projectId,fieldId,updatedDescription);
        assertEquals(HttpStatus.OK ,  updateFieldResponseEntity.getStatusCode());
        FieldsResponseDTO updateFieldResponseDTO = new Gson().fromJson( updateFieldResponseEntity.getBody(),  FieldsResponseDTO.class);
        assertEquals(updatedDescription,updateFieldResponseDTO.getFields().getFieldsSecret().getDescription());
    }

    private ResponseEntity<String> updateDescription(String projectId, String fieldId, String description) {
        UpdateFieldInputDTO updateFieldInputDTO = new UpdateFieldInputDTO();
        updateFieldInputDTO.setDescription(description);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("description");
        updateFieldInputDTO.setUpdatedFields(updatedFields);

        HttpEntity updateFieldEntity = new HttpEntity<>(updateFieldInputDTO, headers);

        ResponseEntity<String> updateFieldResponseEntity =
                restTemplate.exchange(MessageFormat.format(updateFieldUrl, projectId) + fieldId, HttpMethod.PUT, updateFieldEntity ,String.class);
        return updateFieldResponseEntity;
    }


    @Test
    public void when_update_field_with_type_returns_successful(){
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Well";
        String description = "Sing";
        String type = "text";
        ResponseEntity<String> fieldResponseEntity = createField(name, description, type, projectId);
        assertEquals(HttpStatus.CREATED, fieldResponseEntity.getStatusCode());
        FieldsResponseDTO fieldResponseDTO = new Gson().fromJson(fieldResponseEntity.getBody(), FieldsResponseDTO.class);
        String fieldId = fieldResponseDTO.getFields().getId();
        String updatedType = "text";
        ResponseEntity<String> updateFieldResponseEntity = updateType(projectId,fieldId,updatedType);
        assertEquals(HttpStatus.OK ,  updateFieldResponseEntity.getStatusCode());
        FieldsResponseDTO updateFieldResponseDTO = new Gson().fromJson( updateFieldResponseEntity.getBody(),  FieldsResponseDTO.class);
        assertEquals(updatedType,updateFieldResponseDTO.getFields().getFieldsSecret().getType().toLowerCase());
    }
    private ResponseEntity<String> updateType(String projectId, String fieldId, String type) {
        UpdateFieldInputDTO updateFieldInputDTO = new UpdateFieldInputDTO();
        updateFieldInputDTO.setType(type);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("type");
        updateFieldInputDTO.setUpdatedFields(updatedFields);

        HttpEntity updateFieldEntity = new HttpEntity<>(updateFieldInputDTO, headers);

        ResponseEntity<String> updateFieldResponseEntity =
                restTemplate.exchange(MessageFormat.format(updateFieldUrl, projectId) + fieldId, HttpMethod.PUT, updateFieldEntity ,String.class);
        return updateFieldResponseEntity;
    }

    @Test
    public void when_update_field_with_value_returns_successful(){
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Well";
        String description = "Sing";
        String type = "text";
        ResponseEntity<String> fieldResponseEntity = createField(name, description, type, projectId);
        assertEquals(HttpStatus.CREATED, fieldResponseEntity.getStatusCode());
        FieldsResponseDTO fieldResponseDTO = new Gson().fromJson(fieldResponseEntity.getBody(), FieldsResponseDTO.class);
        String fieldId = fieldResponseDTO.getFields().getId();
        String value = "3";
        ResponseEntity<String> updateFieldResponseEntity = updateValue(projectId,fieldId,value);
        assertEquals(HttpStatus.OK ,  updateFieldResponseEntity.getStatusCode());
        FieldsResponseDTO updateFieldResponseDTO = new Gson().fromJson( updateFieldResponseEntity.getBody(),  FieldsResponseDTO.class);
        assertEquals(value,updateFieldResponseDTO.getFields().getFieldsSecret().getValue());
    }
    private ResponseEntity<String> updateValue(String projectId, String fieldId, String value) {
        UpdateFieldInputDTO updateFieldInputDTO = new UpdateFieldInputDTO();
        updateFieldInputDTO.setValue(value);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("value");
        updateFieldInputDTO.setUpdatedFields(updatedFields);

        HttpEntity updateFieldEntity = new HttpEntity<>(updateFieldInputDTO, headers);

        ResponseEntity<String> updateFieldResponseEntity =
                restTemplate.exchange(MessageFormat.format(updateFieldUrl, projectId) + fieldId, HttpMethod.PUT, updateFieldEntity ,String.class);
        return updateFieldResponseEntity;
    }

    @After
    public void cleanUp() {
        projectRepository.deleteAll();
        fieldsRepository.deleteAll();
    }

}
