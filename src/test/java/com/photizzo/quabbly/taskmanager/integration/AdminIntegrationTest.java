package com.photizzo.quabbly.taskmanager.integration;


import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.photizzo.quabbly.taskmanager.config.TokenProvider;
import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.dto.input.CreateRoleInputDTO;
import com.photizzo.quabbly.taskmanager.dto.output.BasicResponseDTO;
import com.photizzo.quabbly.taskmanager.dto.output.CapabilitiesListResponseDTO;
import com.photizzo.quabbly.taskmanager.dto.output.RoleListResponseDTO;
import com.photizzo.quabbly.taskmanager.dto.output.RoleResponseDTO;
import com.photizzo.quabbly.taskmanager.model.extras.Capabilities;
import com.photizzo.quabbly.taskmanager.repositories.ProjectRepository;
import com.photizzo.quabbly.taskmanager.repositories.RoleRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AdminIntegrationTest extends AbstractIntegrationTest {

    private CreateRoleInputDTO roleInputDTO;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ProjectRepository projectRepository;



    @LocalServerPort
    private int localServerPort;


    @Mock
    private TokenProvider tokenProvider;

    private String createRoleUrl;

    private String fetchRolesUrl;

    private String fetchCapabilitiesUrl;




    private String updateRoleUrl;

    @BeforeClass
    public static void configuration() {
        System.setProperty("mongodb.host", "localhost");
        System.setProperty("mongodb.port", "27017");
        System.setProperty("database.name", "taskmanager_test");
    }

    @Before
    public void setUp() {
        createRoleUrl = "http://localhost:" + localServerPort + "/v1/admin/role";
        fetchRolesUrl = "http://localhost:"+localServerPort+"/v1/admin/roles";
        fetchCapabilitiesUrl = "http://localhost:"+localServerPort+"/v1/admin/capabilities";
        updateRoleUrl = "http://localhost:" + localServerPort + "/v1/admin/role/{0}";

        createAndlogInUser();
    }

    @Test
    public void when_create_role_with_valid_data_returns_successful() {
        createRoleAndReturnDTO();
    }

    private RoleResponseDTO createRoleAndReturnDTO(){

        String name = "dummyrole";
        List<String> capabilities = Lists.newArrayList("GENERAL");

        CreateRoleInputDTO roleInputDTO = new CreateRoleInputDTO();
        roleInputDTO.setName(name);
        roleInputDTO.setCapabilities(capabilities);

        HttpEntity entity = new HttpEntity<>(roleInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity( createRoleUrl, entity, String.class);


        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        RoleResponseDTO responseDTO = readValue(responseEntity.getBody(), RoleResponseDTO.class);
        assertEquals(Status.CREATED, responseDTO.getStatus());
        assertNotNull(responseDTO.getRole());
        assertNotNull(responseDTO.getRole().getId());
        assertNotNull(responseDTO.getRole().getCreatedTime());
        assertNotNull(responseDTO.getRole().getRoleSecret());
        assertEquals(name, responseDTO.getRole().getRoleSecret().getName());
        assertEquals(capabilities, responseDTO.getRole().getRoleSecret().getCapabilities());

        return responseDTO;
    }

    @Test
    public void when_create_role_with_null_name_returns_valid_error() {
        String name = null;
        List<String> capabilities = Lists.newArrayList("CREATE_PROJECT");

        CreateRoleInputDTO roleInputDTO = new CreateRoleInputDTO();
        roleInputDTO.setName(name);
        roleInputDTO.setCapabilities(capabilities);

        HttpEntity entity = new HttpEntity<>(roleInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity( createRoleUrl, entity, String.class);


        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, readValue(responseEntity.getBody(), BasicResponseDTO.class).getStatus());
        assertError("name", "name is required", roleInputDTO.getName(), responseEntity.getBody());
    }

    @Test
    public void when_create_role_with_name_below_length_returns_valid_error() {
        String name = "";
        List<String> capabilities = Lists.newArrayList("GENERAL");

        CreateRoleInputDTO roleInputDTO = new CreateRoleInputDTO();
        roleInputDTO.setName(name);
        roleInputDTO.setCapabilities(capabilities);

        HttpEntity entity = new HttpEntity<>(roleInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity( createRoleUrl, entity, String.class);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, readValue(responseEntity.getBody(), BasicResponseDTO.class).getStatus());
        assertError("name", "name should be between 2 and 50 characters", name, responseEntity.getBody());
    }

    @Test
    public void when_create_role_with_name_above_length_returns_valid_error() {
        String name = "";
        for (int i = 1; i <= 51; i++) {
            name += "a";
        }

        List<String> capabilities = Lists.newArrayList("GENERAL");

        CreateRoleInputDTO roleInputDTO = new CreateRoleInputDTO();
        roleInputDTO.setName(name);
        roleInputDTO.setCapabilities(capabilities);

        HttpEntity entity = new HttpEntity<>(roleInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity( createRoleUrl, entity, String.class);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, readValue(responseEntity.getBody(), BasicResponseDTO.class).getStatus());
        assertError("name", "name should be between 2 and 50 characters", name, responseEntity.getBody());
    }

    @Test
    public void when_create_role_with_duplicate_name_returns_already_exists() {
        String name = "dummyRole1";
        List<String> capabilities = Lists.newArrayList("GENERAL");

        CreateRoleInputDTO roleInputDTO = new CreateRoleInputDTO();
        roleInputDTO.setName(name);
        roleInputDTO.setCapabilities(capabilities);

        HttpEntity entity = new HttpEntity<>(roleInputDTO, headers);

        ResponseEntity<String> firstEntity = restTemplate.postForEntity( createRoleUrl, entity, String.class);
        assertNotNull(firstEntity);
        assertEquals(HttpStatus.CREATED, firstEntity.getStatusCode());

        ResponseEntity<String> secondEntity = restTemplate.postForEntity( createRoleUrl, entity, String.class);

        assertEquals(HttpStatus.BAD_REQUEST, secondEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, readValue(secondEntity.getBody(), BasicResponseDTO.class).getStatus());
        assertErrorSize(1, secondEntity.getBody());
        assertError("name", "The supplied name has already been taken", roleInputDTO.getName(), secondEntity.getBody(), 0);

    }

    @Test
    public void when_create_role_with_null_capabilities_returns_valid_error() {
        String name = "dummyRole";
        List<String> capabilities = null;

        CreateRoleInputDTO roleInputDTO = new CreateRoleInputDTO();
        roleInputDTO.setName(name);
        roleInputDTO.setCapabilities(capabilities);

        HttpEntity entity = new HttpEntity<>(roleInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity( createRoleUrl, entity, String.class);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, readValue(responseEntity.getBody(), BasicResponseDTO.class).getStatus());
        assertError("capabilities", "role requires at least one capability selected", null, responseEntity.getBody());
    }

    @Test
    public void testFetchCapabilitiesReturnsValidData(){

        HttpEntity entity = new HttpEntity<>(headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange( fetchCapabilitiesUrl , HttpMethod.GET, entity ,String.class);

        assertEquals(HttpStatus.OK ,responseEntity.getStatusCode());

        CapabilitiesListResponseDTO responseDTO =  readValue(responseEntity.getBody(),CapabilitiesListResponseDTO.class);

        assertNotNull(responseDTO);
        assertEquals(Status.SUCCESS ,responseDTO.getStatus());
        assertEquals(new ArrayList<>(EnumSet.allOf(Capabilities.class)), responseDTO.getCapabilities());
    }

    @Test
    public void testFetchRolesReturnsValidData(){

        HttpEntity entity = new HttpEntity<>(headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange( fetchRolesUrl , HttpMethod.GET, entity ,String.class);

        assertEquals(HttpStatus.OK ,responseEntity.getStatusCode());

        RoleListResponseDTO responseDTO =  new Gson().fromJson(responseEntity.getBody(),RoleListResponseDTO.class);

        assertNotNull(responseDTO);
        assertEquals(Status.SUCCESS ,responseDTO.getStatus());
    }



    @After
    public void cleanUp() {
        roleRepository.deleteAll();
    }


}
