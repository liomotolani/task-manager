package com.photizzo.quabbly.taskmanager.integration;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.photizzo.quabbly.taskmanager.config.TokenProvider;
import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.dto.input.*;
import com.photizzo.quabbly.taskmanager.dto.output.BasicResponseDTO;
import com.photizzo.quabbly.taskmanager.dto.output.ProjectListResponseDTO;
import com.photizzo.quabbly.taskmanager.dto.output.ProjectResponseDTO;
import com.photizzo.quabbly.taskmanager.dto.output.RoleResponseDTO;
import com.photizzo.quabbly.taskmanager.repositories.ProjectRepository;
import com.photizzo.quabbly.taskmanager.repositories.ProjectUserRepository;
import com.photizzo.quabbly.taskmanager.repositories.RoleRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProjectIntegrationTest extends AbstractIntegrationTest {

    private ColumnInputDTO columnInputDTO;
    private CreateProjectInputDTO projectInputDTO;
    
    @Autowired
    private  ProjectRepository projectRepository;

    @Autowired
    private ProjectUserRepository projectUserRepository;

    @Autowired
    private RoleRepository roleRepository;

    @LocalServerPort
    private int localServerPort;

    @Mock
    private TokenProvider tokenProvider;

    private String testRoleId;

    private String createProjectUrl;

    private String projectUrl;

    private String fetchProjectUrl;

    private String createRoleUrl;

    private String  fetchProjectsUrl;

    private String  renameColumnUrl;

    private String fetchUserProjectUrl;

    @BeforeClass
    public static void configuration() {
        System.setProperty("mongodb.host", "localhost");
        System.setProperty("mongodb.port", "27017");
    }

    @Before
    public void setUp(){
        createProjectUrl = "http://localhost:" + localServerPort + "/v1/admin/project/create";
        projectUrl = "http://localhost:"+localServerPort+"/v1/project/{0}/";
        fetchProjectUrl = "http://localhost:" + localServerPort + "/v1/admin/project/{0}/";
        createRoleUrl = "http://localhost:" + localServerPort + "/v1/admin/role";
        fetchProjectsUrl = "http://localhost:" + localServerPort + "/v1/admin/projects";
        renameColumnUrl = "http://localhost:"+localServerPort+"/v1/project/{0}/column/";
        fetchUserProjectUrl = "http://localhost:"+localServerPort+"/v1/projects/{0}";
        createAndlogInUser();
        testRoleId = createRole("dummyRole1");
    }

    private ResponseEntity<String> createProject(String projectName,String projectDescription){
        CreateProjectInputDTO projectInputDTO = new CreateProjectInputDTO();
        projectInputDTO.setName(projectName);
        projectInputDTO.setDescription(projectDescription);


        HttpEntity projectEntity = new HttpEntity<>(projectInputDTO, headers);

        ResponseEntity<String> projectResponseEntity =
                restTemplate.postForEntity(createProjectUrl, projectEntity, String.class);
        return projectResponseEntity;


    }

    @Test
    public void when_create_project_with_valid_name_returns_successful() {
        String projectName = "dummyProject";
        String projectDescription ="dummy project here";
        ResponseEntity<String> projectResponseEntity = createProject(projectName,projectDescription);
        assertEquals(HttpStatus.CREATED, projectResponseEntity.getStatusCode());
        ProjectResponseDTO responseDTO =readValue(projectResponseEntity.getBody(), ProjectResponseDTO.class);
        assertEquals(Status.CREATED, responseDTO.getStatus());
        assertNotNull(responseDTO.getProject());
        assertNotNull(responseDTO.getProject().getUniqueKey());
        assertNotNull(responseDTO.getProject().getId());
        assertNotNull(responseDTO.getProject().getCreatedTime());
        assertNotNull(responseDTO.getColumnList());
        assertEquals(3, responseDTO.getColumnList().size());

        assertNotNull(responseDTO.getColumnList().get(0).getId());
        assertEquals(0, responseDTO.getColumnList().get(0).getPosition());

        assertNotNull(responseDTO.getColumnList().get(1).getId());
        assertEquals(1, responseDTO.getColumnList().get(1).getPosition());

        assertNotNull(responseDTO.getColumnList().get(2).getId());
        assertEquals(2, responseDTO.getColumnList().get(2).getPosition());
    }

    @Test
    public void when_create_project_with_invalid_name_size_returns_valid_error() {
        String name = "";
        String projectDescription ="dummy project here";
        ResponseEntity<String> responseEntity = createProject(name,projectDescription);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, readValue(responseEntity.getBody(), ProjectResponseDTO.class).getStatus());
        assertError("name", "project name should be between 3 and 50 characters", name, responseEntity.getBody());
    }

    @Test
    public void when_create_project_with_null_name_returns_valid_error_() {
        String projectDescription ="dummy project here";
        ResponseEntity<String> responseEntity = createProject(null,projectDescription);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, readValue(responseEntity.getBody(), BasicResponseDTO.class).getStatus());
        assertError("name", "name is required", null, responseEntity.getBody());

    }

    @Test
    public void when_create_project_with_duplicate_name_returns_already_exists() {
        String name = "dummyProject";
        String projectDescription ="dummy project here";
        ResponseEntity<String> firstEntity = createProject(name,projectDescription);
        assertNotNull(firstEntity);
        assertEquals(HttpStatus.CREATED, firstEntity.getStatusCode());

        ResponseEntity<String> secondEntity = createProject(name,projectDescription);

        assertEquals(HttpStatus.BAD_REQUEST, secondEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, readValue(secondEntity.getBody(), BasicResponseDTO.class).getStatus());
        assertError("name", "please choose a different value. this one already exists",name, secondEntity.getBody(), 0);

    }

    @Test
    public void when_update_project_with_valid_name_returns_successful() {
        String name = "testProject";
        String projectDescription ="dummy project here";

        ResponseEntity<String> responseEntity = createProject(name,projectDescription);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        ProjectResponseDTO responseDTO = readValue(responseEntity.getBody(), ProjectResponseDTO.class);
        assertEquals(Status.CREATED, responseDTO.getStatus());
        String projectId = responseDTO.getProject().getId();
        String newName = "New Project Name";

        ResponseEntity<String> updateProjectResponseEntity = updateName(projectId,newName);
        assertEquals(HttpStatus.OK,updateProjectResponseEntity.getStatusCode());
        ProjectResponseDTO updateProjectResponseDTO = readValue(updateProjectResponseEntity.getBody(), ProjectResponseDTO.class);

        assertEquals(Status.SUCCESS, updateProjectResponseDTO.getStatus());
        assertNotNull(updateProjectResponseDTO.getProject());
        assertNotNull(updateProjectResponseDTO.getProject().getId());
        assertNotNull(updateProjectResponseDTO.getProject().getCreatedTime());

    }



    @Test
    public void when_update_project_with_valid_description_returns_successful() {
        String name = "testProject";
        String projectDescription ="dummy project here";

        ResponseEntity<String> responseEntity = createProject(name,projectDescription);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        ProjectResponseDTO responseDTO = new Gson().fromJson(responseEntity.getBody(), ProjectResponseDTO.class);
        assertEquals(Status.CREATED, responseDTO.getStatus());
        String projectId = responseDTO.getProject().getId();
        String newDescription = "new dummy project";

        ResponseEntity<String> updateProjectResponseEntity = updateDescription(projectId,newDescription);
        assertEquals(HttpStatus.OK,updateProjectResponseEntity.getStatusCode());
        ProjectResponseDTO updateProjectResponseDTO = readValue(updateProjectResponseEntity.getBody(), ProjectResponseDTO.class);

        assertEquals(Status.SUCCESS, updateProjectResponseDTO.getStatus());
        assertNotNull(updateProjectResponseDTO.getProject());
        assertNotNull(updateProjectResponseDTO.getProject().getId());
        assertNotNull(updateProjectResponseDTO.getProject().getCreatedTime());

    }
    @Test
    public void when_update_project_with_invalid_project_id_returns_404_error() {
        String name = "testProject";
        String projectDescription ="dummy project here";

        ResponseEntity<String> responseEntity = createProject(name,projectDescription);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        ProjectResponseDTO responseDTO = new Gson().fromJson(responseEntity.getBody(), ProjectResponseDTO.class);
        assertEquals(Status.CREATED, responseDTO.getStatus());
        String projectId = "invalid projectId";
        String newName = "new project name";



        ResponseEntity<String> updateProjectResponseEntity = updateName(projectId,newName);
        assertEquals(HttpStatus.NOT_FOUND,updateProjectResponseEntity.getStatusCode());
        ProjectResponseDTO updateProjectResponseDTO = new Gson().fromJson(updateProjectResponseEntity.getBody(), ProjectResponseDTO.class);
        assertEquals(Status.NOT_FOUND, updateProjectResponseDTO.getStatus());
    }

    private ResponseEntity<String> updateName(String projectId,String newName){
        UpdateProjectInputDTO updateProjectInputDTO = new UpdateProjectInputDTO();
        updateProjectInputDTO.setName(newName);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("newName");
        updateProjectInputDTO.setUpdatedFields(updatedFields);
        HttpEntity updateProjectEntity = new HttpEntity<>(updateProjectInputDTO, headers);
        ResponseEntity<String> updateProjectResponseEntity =
                restTemplate.exchange(MessageFormat.format(projectUrl,projectId) + "/update", HttpMethod.PUT, updateProjectEntity ,String.class);
        return updateProjectResponseEntity;
    }
    private ResponseEntity<String> updateDescription(String projectId,String newDescription){
        UpdateProjectInputDTO updateProjectInputDTO = new UpdateProjectInputDTO();
        updateProjectInputDTO.setDescription(newDescription);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("newDescription");
        updateProjectInputDTO.setUpdatedFields(updatedFields);
        HttpEntity updateProjectEntity = new HttpEntity<>(updateProjectInputDTO, headers);
        ResponseEntity<String> updateProjectResponseEntity =
                restTemplate.exchange(MessageFormat.format(projectUrl,projectId) + "/update", HttpMethod.PUT, updateProjectEntity ,String.class);
        return updateProjectResponseEntity;
    }

    @Test
    public void when_fetch_project_with_valid_Id_returns_successful() {
        String name = "dummyProject";
        String description ="dummy project here";

        ResponseEntity<String> responseEntity = createProject(name,description);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        ProjectResponseDTO responseDTO = readValue(responseEntity.getBody(), ProjectResponseDTO.class);
        assertEquals(Status.CREATED, responseDTO.getStatus());
        String projectId = responseDTO.getProject().getId();
        ProjectResponseDTO fetchProjectResponseDTO = fetchProject(projectId);
          assertNotNull(fetchProjectResponseDTO.getProject().getId());
          assertNotNull(fetchProjectResponseDTO.getProject().getCreatedTime());
          assertNotNull(fetchProjectResponseDTO.getProject().getProjectSecret());
          assertNotNull(fetchProjectResponseDTO.getProject().getProjectSecret().getName());
          assertNotNull(fetchProjectResponseDTO.getProject().getProjectSecret().getDescription());
          assertEquals(projectId,fetchProjectResponseDTO.getProject().getId());
          assertEquals(name,fetchProjectResponseDTO.getProject().getProjectSecret().getName());
          assertEquals(description,fetchProjectResponseDTO.getProject().getProjectSecret().getDescription());

    }

    private ProjectResponseDTO fetchProject(String projectId){

        HttpEntity projectEntity = new HttpEntity<>(headers);
        ResponseEntity<String> fetchProjectResponseEntity =
                restTemplate.exchange(MessageFormat.format(projectUrl,projectId), HttpMethod.GET, projectEntity ,String.class);
        assertEquals(HttpStatus.OK ,fetchProjectResponseEntity.getStatusCode());
        ProjectResponseDTO fetchProjectResponseDTO =  readValue(fetchProjectResponseEntity.getBody(),ProjectResponseDTO.class);
        return  fetchProjectResponseDTO;
    }


    @Test
    public void when_fetch_project_with_Invalid_Id_returns_successful() {
        String name = "dummyProject";
        String description ="dummy project here";

        ResponseEntity<String> responseEntity = createProject(name,description);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        ProjectResponseDTO responseDTO = readValue(responseEntity.getBody(), ProjectResponseDTO.class);
        assertEquals(Status.CREATED, responseDTO.getStatus());
        String projectId = "invaliid projectId";
        HttpEntity projectEntity = new HttpEntity<>(headers);
        ResponseEntity<String> fetchProjectResponseEntity =
                restTemplate.exchange(MessageFormat.format(projectUrl,projectId) , HttpMethod.GET, projectEntity ,String.class);
        assertEquals(HttpStatus.NOT_FOUND ,fetchProjectResponseEntity.getStatusCode());
        ProjectResponseDTO fetchProjectResponseDTO =  readValue(fetchProjectResponseEntity.getBody(),ProjectResponseDTO.class);
        assertEquals(Status.NOT_FOUND,fetchProjectResponseDTO.getStatus());
    }


    @Test
    public void when_fetch_projects_returns_successful() {
        String name = "project1";
        String description ="dummy project here";
        ResponseEntity<String> responseEntity = createProject(name,description);

        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        ProjectResponseDTO firstProjectResponseDTO = readValue(responseEntity.getBody(), ProjectResponseDTO.class);
        assertEquals(Status.CREATED, firstProjectResponseDTO.getStatus());

        String secondName = "project2";
        String secondDescription ="dummy project here";
        ResponseEntity<String> secondProjectResponseEntity = createProject(secondName,secondDescription);

        assertEquals(HttpStatus.CREATED, secondProjectResponseEntity.getStatusCode());
        ProjectResponseDTO secondProjectResponseDTO = readValue(responseEntity.getBody(), ProjectResponseDTO.class);
        assertEquals(Status.CREATED, secondProjectResponseDTO.getStatus());

        HttpEntity fetchProjectsEntity = new HttpEntity<>(headers);
        ResponseEntity<String> fetchProjectResponseEntity =
                restTemplate.exchange( fetchProjectsUrl, HttpMethod.GET, fetchProjectsEntity, String.class);
        assertEquals(HttpStatus.OK ,fetchProjectResponseEntity.getStatusCode());

        ProjectListResponseDTO projects = readValue(fetchProjectResponseEntity.getBody(), ProjectListResponseDTO.class);
        assertEquals(Status.SUCCESS,projects.getStatus());
        assertNotNull(projects.getProjectList());
        assertNotNull(projects.getProjectList().get(0));
        assertNotNull(projects.getProjectList().get(0).getId());
        assertNotNull(projects.getProjectList().get(0).getCreatedTime());
        assertNotNull(projects.getProjectList().get(0).getProjectSecret());
        assertNotNull(projects.getProjectList().get(1));
        assertNotNull(projects.getProjectList().get(1).getId());
        assertNotNull(projects.getProjectList().get(1).getCreatedTime());
        assertNotNull(projects.getProjectList().get(1).getProjectSecret());

        assertEquals(name,projects.getProjectList().get(0).getProjectSecret().getName());
        assertEquals(secondName,projects.getProjectList().get(1).getProjectSecret().getName());
        assertEquals(description,projects.getProjectList().get(0).getProjectSecret().getDescription());
        assertEquals(secondDescription,projects.getProjectList().get(1).getProjectSecret().getDescription());
    }


//    @Test
//    public void when_attach_user_to_project_returns_successful() {
//
//        String projectId = getProjectId("dummyProject2");
//
//        AttachUserToProjectInputDTO attachUserToProjectInputDTO = getAttachUserToProjectPojo();
//
//        HttpEntity roleHttPEntity = new HttpEntity<>(attachUserToProjectInputDTO, headers);
//
//        ResponseEntity<String> entity =
//                restTemplate.postForEntity(MessageFormat.format(fetchProjectUrl,projectId) + "/attach_user", roleHttPEntity, String.class);
//
//        assertNotNull(entity);
//
//        BasicResponseDTO responseDTO = readValue(entity.getBody(), BasicResponseDTO.class);
//        assertEquals(Status.SUCCESS, responseDTO.getStatus());
//    }

    @Test
    public void when_attach_user_to_project_with_invalid_project_id_returns_not_found() {

        String projectId = "an-invalid-project-id";

        AttachUserToProjectInputDTO attachUserToProjectInputDTO = getAttachUserToProjectPojo();

        HttpEntity roleHttPEntity = new HttpEntity<>(attachUserToProjectInputDTO, headers);

        ResponseEntity<String> entity =
                restTemplate.postForEntity(MessageFormat.format(fetchProjectUrl,projectId) + "/attach_user", roleHttPEntity, String.class);

        assertNotNull(entity);

        BasicResponseDTO responseDTO = readValue(entity.getBody(), BasicResponseDTO.class);
        assertEquals(Status.NOT_FOUND, responseDTO.getStatus());
    }

    @Test
    public void when_attach_user_to_project_with_missing_user_id_returns_validation_error() {

        String projectId = "dummyId";

        AttachUserToProjectInputDTO attachUserToProjectInputDTO = getAttachUserToProjectPojo();
        attachUserToProjectInputDTO.setUserId(null);

        HttpEntity roleHttPEntity = new HttpEntity<>(attachUserToProjectInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format(fetchProjectUrl,projectId) + "/attach_user", roleHttPEntity, String.class);

        assertNotNull(responseEntity);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, readValue(responseEntity.getBody(), BasicResponseDTO.class).getStatus());
        assertErrorSize(1, responseEntity.getBody());
        assertError("userId", "user is required", null, responseEntity.getBody());
    }

//    @Test
//    public void when_attach_user_to_project_with_duplicate_user_id_returns_already_attached_validation_error() {
//
//        String projectId = getProjectId("dummyProject3");
//
//        AttachUserToProjectInputDTO attachUserToProjectInputDTO = getAttachUserToProjectPojo();
//
//        HttpEntity entity = new HttpEntity<>(attachUserToProjectInputDTO, headers);
//
//        ResponseEntity<String> responseEntity =
//                restTemplate.postForEntity(MessageFormat.format(fetchProjectUrl,projectId)+ "/attach_user", entity, String.class);
//
//        assertNotNull(responseEntity);
//
//        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
//        assertEquals(Status.SUCCESS, readValue(responseEntity.getBody(), BasicResponseDTO.class).getStatus());
//
//        HttpEntity entity2 = new HttpEntity<>(attachUserToProjectInputDTO, headers);
//
//        ResponseEntity<String> responseEntity2 =
//                restTemplate.postForEntity(MessageFormat.format(fetchProjectUrl,projectId) + "/attach_user", entity2, String.class);
//
//        assertNotNull(responseEntity2);
//
//        assertEquals(HttpStatus.BAD_REQUEST, responseEntity2.getStatusCode());
//        assertEquals(Status.FAILED_VALIDATION, readValue(responseEntity2.getBody(), BasicResponseDTO.class).getStatus());
//        assertErrorSize(1, responseEntity2.getBody());
//        assertError("userId", "The user has been attached to this project", null, responseEntity2.getBody());
//
//    }

    @Test
    public void when_attach_user_to_project_with_missing_role_id_returns_required_validation_error() {

        String projectId = "dummyId";

        AttachUserToProjectInputDTO attachUserToProjectInputDTO = getAttachUserToProjectPojo();
        attachUserToProjectInputDTO.setRoleId(null);

        HttpEntity roleHttPEntity = new HttpEntity<>(attachUserToProjectInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format(fetchProjectUrl,projectId) + "/attach_user", roleHttPEntity, String.class);

        assertNotNull(responseEntity);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, readValue(responseEntity.getBody(), BasicResponseDTO.class).getStatus());
        assertErrorSize(1, responseEntity.getBody());
        assertError("roleId", "role is required", null, responseEntity.getBody());
    }

    @Test
    public void when_attach_user_to_project_with_invalid_role_id_returns_invalid_role_validation_error() {

        String projectId = "dummyId";

        AttachUserToProjectInputDTO attachUserToProjectInputDTO = getAttachUserToProjectPojo();
        attachUserToProjectInputDTO.setRoleId("an-invalid-role-id");

        HttpEntity roleHttPEntity = new HttpEntity<>(attachUserToProjectInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format(fetchProjectUrl,projectId)+ "/attach_user", roleHttPEntity, String.class);

        assertNotNull(responseEntity);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, readValue(responseEntity.getBody(), BasicResponseDTO.class).getStatus());
        assertErrorSize(1, responseEntity.getBody());
        assertError("roleId", "selected role is not a valid role", null, responseEntity.getBody());
    }

    @Test
    public void when_attach_user_to_project_with_missing_user_fullname_returns_required_validation_error() {

        String projectId = "dummyId";

        AttachUserToProjectInputDTO attachUserToProjectInputDTO = getAttachUserToProjectPojo();
        attachUserToProjectInputDTO.setUserFullname(null);

        HttpEntity roleHttPEntity = new HttpEntity<>(attachUserToProjectInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format(fetchProjectUrl,projectId)+ "/attach_user", roleHttPEntity, String.class);

        assertNotNull(responseEntity);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, readValue(responseEntity.getBody(), BasicResponseDTO.class).getStatus());
        assertErrorSize(1, responseEntity.getBody());
        assertError("userFullname", "fullname of user is required", null, responseEntity.getBody());
    }

    @Test
    public void when_attach_user_to_project_with_missing_user_email_returns_required_validation_error() {

        String projectId = "dummyId";

        AttachUserToProjectInputDTO attachUserToProjectInputDTO = getAttachUserToProjectPojo();
        attachUserToProjectInputDTO.setUserEmail(null);

        HttpEntity roleHttPEntity = new HttpEntity<>(attachUserToProjectInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format(fetchProjectUrl,projectId)+ "/attach_user", roleHttPEntity, String.class);

        assertNotNull(responseEntity);

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, readValue(responseEntity.getBody(), BasicResponseDTO.class).getStatus());
        assertErrorSize(1, responseEntity.getBody());
        assertError("userEmail", "email of user is required", null, responseEntity.getBody());
    }

    @Test
    public void when_delete_project_return_successful(){
        String projectName = "dummyProject";
        String projectDescription = "dummy project here";
        ResponseEntity<String> projectResponseEntity = createProject(projectName,projectDescription);
        assertEquals(HttpStatus.CREATED, projectResponseEntity.getStatusCode());
        ProjectResponseDTO responseDTO =readValue(projectResponseEntity.getBody(), ProjectResponseDTO.class);
        assertEquals(Status.CREATED, responseDTO.getStatus());
        String projectId = responseDTO.getProject().getId();
        HttpEntity entity = new HttpEntity<>(headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(MessageFormat.format(projectUrl,projectId),HttpMethod.DELETE,entity,String.class);
        assertEquals(HttpStatus.OK,responseEntity.getStatusCode());
    }

    private AttachUserToProjectInputDTO getAttachUserToProjectPojo() {
        AttachUserToProjectInputDTO attachUserToProjectInputDTO = new AttachUserToProjectInputDTO();
        attachUserToProjectInputDTO.setUserEmail("dummyuser@quabbly.com");
        attachUserToProjectInputDTO.setRoleId(testRoleId);
        attachUserToProjectInputDTO.setUserFullname("dummy user");
        attachUserToProjectInputDTO.setUserId("aaaaa-bbbbb-11111-22222");

        return attachUserToProjectInputDTO;
    }

    private String createRole(String name) {
        List<String> capabilities = Lists.newArrayList("GENERAL");

        CreateRoleInputDTO roleInputDTO = new CreateRoleInputDTO();
        roleInputDTO.setName(name);
        roleInputDTO.setCapabilities(capabilities);

        HttpEntity roleHttPEntity = new HttpEntity<>(roleInputDTO, headers);

        ResponseEntity<String> roleEntity =
                restTemplate.postForEntity(createRoleUrl, roleHttPEntity, String.class);

        assertEquals(HttpStatus.CREATED, roleEntity.getStatusCode());
        RoleResponseDTO roleResponseDTO = readValue(roleEntity.getBody(), RoleResponseDTO.class);
        assertEquals(Status.CREATED, roleResponseDTO.getStatus());

        return roleResponseDTO.getRole().getId();
    }

    private String getProjectId(String name) {
        CreateProjectInputDTO projectInputDTO = new CreateProjectInputDTO();
        projectInputDTO.setName(name);

        HttpEntity entity = new HttpEntity<>(projectInputDTO, headers);

        ResponseEntity<String> projectEntity = restTemplate.postForEntity(createProjectUrl, entity, String.class);
        assertNotNull(projectEntity);

        ProjectResponseDTO responseDTO = readValue(projectEntity.getBody(), ProjectResponseDTO.class);
        assertEquals(Status.CREATED, responseDTO.getStatus());
        return responseDTO.getProject().getId();
    }

    @Test
    public void when_mark_column_as_hidden_returns_successful(){
        String projectName = "dummyProject";
        String projectDescription = "dummy project here";
        ResponseEntity<String> projectResponseEntity = createProject(projectName,projectDescription);
        assertEquals(HttpStatus.CREATED, projectResponseEntity.getStatusCode());
        ProjectResponseDTO responseDTO =readValue(projectResponseEntity.getBody(), ProjectResponseDTO.class);
        assertEquals(Status.CREATED, responseDTO.getStatus());
        String projectId = responseDTO.getProject().getId();
        String columnId = responseDTO.getColumnList().get(0).getId();
        MarkColumnInputDTO markColumnInputDTO = new MarkColumnInputDTO();
        markColumnInputDTO.setUserId(Lists.newArrayList("one"));
        HttpEntity entity = new HttpEntity<>(markColumnInputDTO,headers);
        ResponseEntity<String> projectEntity = restTemplate.exchange(MessageFormat.format( renameColumnUrl,projectId)+ columnId +"/hide",HttpMethod.PATCH, entity, String.class);
        assertEquals(HttpStatus.OK,projectEntity.getStatusCode());
    }

    @Test
    public void when_mark_column_as_hidden_with_null_userId_return_valid_error(){
        String projectName = "dummyProject";
        String projectDescription = "dummy project here";
        ResponseEntity<String> projectResponseEntity = createProject(projectName,projectDescription);
        assertEquals(HttpStatus.CREATED, projectResponseEntity.getStatusCode());
        ProjectResponseDTO responseDTO =readValue(projectResponseEntity.getBody(), ProjectResponseDTO.class);
        assertEquals(Status.CREATED, responseDTO.getStatus());
        String projectId = responseDTO.getProject().getId();
        String columnId = responseDTO.getColumnList().get(0).getId();
        MarkColumnInputDTO markColumnInputDTO = new MarkColumnInputDTO();
        markColumnInputDTO.setUserId(null);
        HttpEntity entity = new HttpEntity<>(markColumnInputDTO,headers);
        ResponseEntity<String> projectEntity = restTemplate.exchange(MessageFormat.format( renameColumnUrl,projectId)+ columnId +"/hide",HttpMethod.PATCH, entity, String.class);
        assertEquals(HttpStatus.BAD_REQUEST,projectEntity.getStatusCode());
    }


    @Test
    public void when_mark_column_as_visible_returns_successful(){
        String projectName = "dummyProject";
        String projectDescription = "dummy project here";
        ResponseEntity<String> projectResponseEntity = createProject(projectName,projectDescription);
        assertEquals(HttpStatus.CREATED, projectResponseEntity.getStatusCode());
        ProjectResponseDTO responseDTO =readValue(projectResponseEntity.getBody(), ProjectResponseDTO.class);
        assertEquals(Status.CREATED, responseDTO.getStatus());
        String projectId = responseDTO.getProject().getId();
        String columnId = responseDTO.getColumnList().get(0).getId();
        MarkColumnInputDTO markColumnInputDTO = new MarkColumnInputDTO();
        markColumnInputDTO.setUserId(Lists.newArrayList("one"));
        HttpEntity entity = new HttpEntity<>(markColumnInputDTO,headers);
        ResponseEntity<String> projectEntity = restTemplate.exchange(MessageFormat.format( renameColumnUrl,projectId)+ columnId +"/hide",HttpMethod.PATCH, entity, String.class);
        assertEquals(HttpStatus.OK,projectEntity.getStatusCode());
        HttpEntity newEntity = new HttpEntity<>(markColumnInputDTO,headers);
        ResponseEntity<String> visibleProjectEntity = restTemplate.exchange(MessageFormat.format( renameColumnUrl,projectId)+ columnId +"/visible",HttpMethod.PATCH, newEntity, String.class);
        assertEquals(HttpStatus.OK,visibleProjectEntity .getStatusCode());
    }

    @Test
    public void when_mark_column_as_visible_with_null_userId_return_valid_error(){
        String projectName = "dummyProject";
        String projectDescription = "dummy project here";
        ResponseEntity<String> projectResponseEntity = createProject(projectName,projectDescription);
        assertEquals(HttpStatus.CREATED, projectResponseEntity.getStatusCode());
        ProjectResponseDTO responseDTO =readValue(projectResponseEntity.getBody(), ProjectResponseDTO.class);
        assertEquals(Status.CREATED, responseDTO.getStatus());
        String projectId = responseDTO.getProject().getId();
        String columnId = responseDTO.getColumnList().get(0).getId();
        MarkColumnInputDTO markColumnInputDTO = new MarkColumnInputDTO();
        markColumnInputDTO.setUserId(null);
        HttpEntity entity = new HttpEntity<>(markColumnInputDTO,headers);
        ResponseEntity<String> projectEntity = restTemplate.exchange(MessageFormat.format( renameColumnUrl,projectId)+ columnId +"/visible",HttpMethod.PATCH, entity, String.class);
        assertEquals(HttpStatus.BAD_REQUEST,projectEntity.getStatusCode());
    }


    @After
    public void cleanUp() {
        projectRepository.deleteAll();
        projectUserRepository.deleteAll();
        roleRepository.deleteAll();
    }
}
