package com.photizzo.quabbly.taskmanager.integration;

import com.google.gson.Gson;
import com.photizzo.quabbly.taskmanager.config.TokenProvider;
import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.dto.input.ApiFieldError;
import com.photizzo.quabbly.taskmanager.dto.input.CreateProjectInputDTO;
import com.photizzo.quabbly.taskmanager.dto.input.CreateTaskInputDTO;
import com.photizzo.quabbly.taskmanager.dto.input.LogHourInputDTO;
import com.photizzo.quabbly.taskmanager.dto.output.*;
import com.photizzo.quabbly.taskmanager.model.Task;
import com.photizzo.quabbly.taskmanager.repositories.LogHourRepository;
import com.photizzo.quabbly.taskmanager.repositories.ProjectRepository;
import com.photizzo.quabbly.taskmanager.repositories.TaskRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class LogHourIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private LogHourRepository logHourRepository;

    @Autowired
    private Environment env;


    @LocalServerPort
    private int localServerPort;

    @Mock
    private TokenProvider tokenProvider;

    private String createProjectUrl;

    private String createTaskUrl;

    private String createLogHourUrl;

    private String fetchLogHourUrl;

    @BeforeClass
    public static void configuration() {
        System.setProperty("mongodb.host", "localhost");
        System.setProperty("mongodb.port", "27017");
    }

    @Before
    public void setUp(){
        createProjectUrl = "http://localhost:" + localServerPort + "/v1/admin/project/create";
        createTaskUrl = "http://localhost:" + localServerPort + "/v1/project/{0}/task/";
        createLogHourUrl ="http://localhost:" + localServerPort + "/v1/project/{0}/task/log-hour";
        fetchLogHourUrl ="http://localhost:" + localServerPort + "/v1/project/{0}/task/";
        createAndlogInUser();

    }

    private ProjectResponseDTO createProject(){
        String projectName = "dummyProject";

        CreateProjectInputDTO projectInputDTO = new CreateProjectInputDTO();
        projectInputDTO.setName(projectName);

        HttpEntity projectEntity = new HttpEntity<>(projectInputDTO, headers);

        ResponseEntity<String> projectResponseEntity =
                restTemplate.postForEntity(createProjectUrl, projectEntity, String.class);
        assertEquals(HttpStatus.CREATED, projectResponseEntity.getStatusCode());

        ProjectResponseDTO responseDTO = readValue(projectResponseEntity.getBody(), ProjectResponseDTO.class);
        return responseDTO;
    }
    private ResponseEntity<String> createTask(String taskSummary,String projectId){
        CreateTaskInputDTO createTaskInputDTO = new CreateTaskInputDTO();
        createTaskInputDTO.setSummary(taskSummary);

        HttpEntity taskEntity = new HttpEntity<>(createTaskInputDTO, headers);

        ResponseEntity<String> taskResponseEntity =
                restTemplate.postForEntity(MessageFormat.format(createTaskUrl, projectId), taskEntity, String.class);
        return taskResponseEntity;
    }
    private ResponseEntity<String> createLogHour(int logNumber, Date logDate, String type, String projectId,String taskId){
        LogHourInputDTO logHourInputDTO = new LogHourInputDTO();
        logHourInputDTO.setLogNumber(logNumber);
        logHourInputDTO.setLogType(type);
        logHourInputDTO.setLogDate(logDate);

        HttpEntity logHourEntity = new HttpEntity<>(logHourInputDTO, headers);

        ResponseEntity<String> logHourResponseEntity =
                restTemplate.postForEntity(MessageFormat.format(createTaskUrl, projectId) + taskId + "/log-hour", logHourEntity, String.class);
        return logHourResponseEntity;
    }

    @Test
    public void when_create_log_hour_with_valid_data_returns_successful() throws ParseException {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Task";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);

        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
        assertNotNull(taskResponseDTO.getTask().getId());
        String taskId = taskResponseDTO.getTask().getId();

        int number = 1;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date logDate = format.parse("2019-02-28");
        String type = "h";
        ResponseEntity<String> logHourResponseEntity = createLogHour(number,logDate,type,projectId,taskId);

        assertEquals(HttpStatus.CREATED, logHourResponseEntity.getStatusCode());
        BasicResponseDTO logHourResponseDTO = readValue(logHourResponseEntity.getBody(),BasicResponseDTO.class);

        assertNotNull(logHourResponseDTO.getData());

    }

    @Test
    public void when_create_logHour_with_future_logDate_returns_valid_error() throws ParseException {
        deleteAll();
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Hem";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);

        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
        assertNotNull(taskResponseDTO.getTask().getId());
        String taskId = taskResponseDTO.getTask().getId();
        int number = 2;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date logDate = format.parse("2019-05-28");
        String type = "d";
        ResponseEntity<String> logHourResponseEntity = createLogHour(number, logDate, type, projectId,taskId);
        assertEquals(HttpStatus.BAD_REQUEST, logHourResponseEntity.getStatusCode());
        ErrorResponseDTO response = readValue(logHourResponseEntity.getBody(), ErrorResponseDTO.class);
        assertEquals(Status.FAILED_VALIDATION, response.getStatus());
        assertNotNull(response.getData());
        assertEquals(1, response.getData().size());
        ApiFieldError error = response.getData().get(0);
        assertEquals("logDate", error.getField());
        assertEquals("date should be past or present", error.getMessage());
    }

    @Test
    public void when_create_logHour_with_null_type_returns_valid_error() throws ParseException {
        deleteAll();
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Hem";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);

        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
        assertNotNull(taskResponseDTO.getTask().getId());
        String taskId = taskResponseDTO.getTask().getId();
        int number = 2;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date logDate = format.parse("2019-02-28");
        String type = null;
        ResponseEntity<String> logHourResponseEntity = createLogHour(number, logDate, type, projectId,taskId);
        assertEquals(HttpStatus.BAD_REQUEST, logHourResponseEntity.getStatusCode());
        ErrorResponseDTO response = readValue(logHourResponseEntity.getBody(), ErrorResponseDTO.class);
        assertEquals(Status.FAILED_VALIDATION, response.getStatus());
        assertNotNull(response.getData());
        assertEquals(1, response.getData().size());
        ApiFieldError error = response.getData().get(0);
        assertEquals("logType", error.getField());
        assertEquals("type is required", error.getMessage());
    }

    @Test
    public void when_create_logHour_with_wrong_type_returns_valid_error() throws ParseException {
        deleteAll();
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Hem";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);

        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
        assertNotNull(taskResponseDTO.getTask().getId());
        String taskId = taskResponseDTO.getTask().getId();
        int number = 2;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date logDate = format.parse("2019-02-28");
        String type = "j";
        ResponseEntity<String> logHourResponseEntity = createLogHour(number, logDate, type, projectId,taskId);
        assertEquals(HttpStatus.BAD_REQUEST, logHourResponseEntity.getStatusCode());
        ErrorResponseDTO response = readValue(logHourResponseEntity.getBody(), ErrorResponseDTO.class);
        assertEquals(Status.FAILED_VALIDATION, response.getStatus());
        assertNotNull(response.getData());
        assertEquals(1, response.getData().size());
        ApiFieldError error = response.getData().get(0);
        assertEquals("logType", error.getField());
        assertEquals("type can either be h(hour) or d(day) or w(week).", error.getMessage());
    }

    @Test
    public void when_fetch_logHour_with_returns_successful() throws ParseException {

        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Hem";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary, projectId);

        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
        assertNotNull(taskResponseDTO.getTask().getId());
        String taskId = taskResponseDTO.getTask().getId();
        int number = 2;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date logDate = format.parse("2019-02-28 ");
        String type = "h";
        ResponseEntity<String> logHourResponseEntity = createLogHour(number, logDate, type, projectId, taskId);
        assertEquals(HttpStatus.CREATED, logHourResponseEntity.getStatusCode());
        int number1 = 3;
        SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
        Date logDate1 = format2.parse("2019-02-28");
        String type1 = "d";
        ResponseEntity<String> logHour1ResponseEntity = createLogHour(number1, logDate1, type1, projectId, taskId);
        assertEquals(HttpStatus.CREATED, logHour1ResponseEntity.getStatusCode());
        int number2 = 4;
        SimpleDateFormat format3 = new SimpleDateFormat("yyyy-MM-dd");
        Date logDate2 = format3.parse("2019-02-28");
        String type2 = "w";
        ResponseEntity<String> logHour2ResponseEntity = createLogHour(number2, logDate2, type2, projectId, taskId);
        assertEquals(HttpStatus.CREATED, logHour2ResponseEntity.getStatusCode());
        int number3 = 4;
        SimpleDateFormat format4 = new SimpleDateFormat("yyyy-MM-dd");
        Date logDate3 = format4.parse("2019-02-28");
        String type3 = "h";
        ResponseEntity<String> logHour3ResponseEntity = createLogHour(number3, logDate3, type3, projectId, taskId);
        assertEquals(HttpStatus.CREATED, logHour3ResponseEntity.getStatusCode());
        int number4 = 3;
        SimpleDateFormat format5 = new SimpleDateFormat("yyyy-MM-dd");
        Date logDate4 = format5.parse("2019-02-28");
        String type4 = "d";
        ResponseEntity<String> logHour4ResponseEntity = createLogHour(number4, logDate4, type4, projectId, taskId);
        assertEquals(HttpStatus.CREATED, logHour4ResponseEntity.getStatusCode());
        HttpEntity newEntity = new HttpEntity<>(headers);
        ResponseEntity<String> fetchLogHourResponseEntity =
                restTemplate.exchange(MessageFormat.format(fetchLogHourUrl, projectId) + taskId + "/log-hour", HttpMethod.GET, newEntity, String.class);
        assertEquals(HttpStatus.OK, fetchLogHourResponseEntity.getStatusCode());
        assertEquals(Status.SUCCESS, readValue(fetchLogHourResponseEntity.getBody(), BasicResponseDTO.class).getStatus());
        BasicResponseDTO fetchLoghourResponseDTO = readValue(fetchLogHourResponseEntity.getBody(), BasicResponseDTO.class);
        assertNotNull(fetchLoghourResponseDTO.getData());
    }

    private void deleteAll() {
        logHourRepository.deleteAll();

    }

    @After
    public void cleanUp() {
        projectRepository.deleteAll();
        logHourRepository.deleteAll();
    }
}
