package com.photizzo.quabbly.taskmanager.integration;


import com.photizzo.quabbly.taskmanager.dto.output.StandardResponseDTO;

public class RegisterUserResponseDTO extends StandardResponseDTO {

    private RegisterUser user;

    public RegisterUser getUser() {
        return user;
    }

    public void setUser(RegisterUser user) {
        this.user = user;
    }
}
