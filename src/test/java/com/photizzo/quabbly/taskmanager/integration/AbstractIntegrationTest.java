package com.photizzo.quabbly.taskmanager.integration;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.dto.input.LoginUser;
import com.photizzo.quabbly.taskmanager.dto.output.BasicResponseDTO;
import com.photizzo.quabbly.taskmanager.dto.output.LoginResponseDTO;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@TestPropertySource(locations = "classpath:test-config.properties")
public class AbstractIntegrationTest {


    protected static TestRestTemplate restTemplate = new TestRestTemplate();

    protected static HttpHeaders headers;

    protected static HttpHeaders headersEncoded;

    protected static String testUserFirstname = null;

    protected static String testUserLastname = null;

    protected static String testUserEmail = null;

    @Value("${users.url}")
    private String userApiBaseUrl;


    protected void createAndlogInUser() {

        if (testUserEmail != null) {
            return;
        }

        String firstname = RandomStringUtils.randomAlphabetic(6).toLowerCase();
        String lastname = RandomStringUtils.randomAlphabetic(6).toLowerCase();
        String email = firstname + "." + lastname + "@quabbly.com";
        String password = RandomStringUtils.randomAlphanumeric(10);
        String companyName = "Quabbly Inc";

        testUserFirstname = firstname;
        testUserLastname = lastname;
        testUserEmail = email;

        RegisterUser registerUser = new RegisterUser(firstname, lastname, email, password, companyName);
        ResponseEntity<RegisterUserResponseDTO> registerResponseEntity =
                restTemplate.postForEntity(userApiBaseUrl + "/auth/register", registerUser, RegisterUserResponseDTO.class);

        assertNotNull(registerResponseEntity);
        assertEquals(HttpStatus.OK, registerResponseEntity.getStatusCode());

        LoginUser u = new LoginUser();
        u.setUsername(email);
        u.setPassword(password);

        ResponseEntity<LoginResponseDTO> responseEntity =
                restTemplate.postForEntity(userApiBaseUrl + "/auth/login", u, LoginResponseDTO.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        LoginResponseDTO responseDTO = responseEntity.getBody();

        assertNotNull(responseDTO);
        assertNotNull(responseDTO.getToken());
        assertNotNull(responseDTO.getUser());
        assertEquals(responseDTO.getUser().getEmail(), email);

        headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + responseDTO.getToken());

        headersEncoded = new HttpHeaders();
        headersEncoded.set(HttpHeaders.AUTHORIZATION, "Bearer " + responseDTO.getToken());
    }


    public <T> T readValue(String content, Class<T> valueType)  {
        try {
            ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return objectMapper.readValue(content, valueType);
        }catch(Exception ex){
            ex.printStackTrace();
            throw new RuntimeException("could not map json response to "+valueType.getName());
        }
    }

    protected void assertErrorSize(int size, String result) {
        JsonArray errors = new JsonParser().parse(result).getAsJsonObject().getAsJsonArray("data");
        assertEquals(size, errors.size());
    }

    protected void assertError(String field, String message, String rejectedValue, String result) {
        assertErrorSize(1, result);
        assertError(field, message, rejectedValue, result, 0);
    }

    protected void assertError(String field, String message, String rejectedValue, String result, int position) {
        BasicResponseDTO response = readValue(result, BasicResponseDTO.class);
        Assert.assertEquals(Status.FAILED_VALIDATION, response.getStatus());

        JsonArray errors = new JsonParser().parse(result).getAsJsonObject().getAsJsonArray("data");


        assertEquals(field, errors.get(position).getAsJsonObject().get("field").getAsString());
        assertEquals(message, errors.get(position).getAsJsonObject().get("message").getAsString());

        if (rejectedValue != null) {
            assertEquals(rejectedValue, errors.get(position).getAsJsonObject().get("rejectedValue").getAsString());
        }
    }
}
