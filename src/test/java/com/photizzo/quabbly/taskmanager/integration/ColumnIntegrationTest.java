package com.photizzo.quabbly.taskmanager.integration;


import com.google.gson.Gson;
import com.photizzo.quabbly.taskmanager.config.TokenProvider;
import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.dto.input.AddColumnInputDTO;
import com.photizzo.quabbly.taskmanager.dto.input.CreateProjectInputDTO;
import com.photizzo.quabbly.taskmanager.dto.input.RenameColumnInputDTO;
import com.photizzo.quabbly.taskmanager.dto.input.RepositionColumnInputDTO;
import com.photizzo.quabbly.taskmanager.dto.output.BasicResponseDTO;
import com.photizzo.quabbly.taskmanager.dto.output.ColumnListResponseDTO;
import com.photizzo.quabbly.taskmanager.dto.output.ColumnResponseDTO;
import com.photizzo.quabbly.taskmanager.dto.output.ProjectResponseDTO;
import com.photizzo.quabbly.taskmanager.model.Column;
import com.photizzo.quabbly.taskmanager.repositories.ColumnRepository;
import com.photizzo.quabbly.taskmanager.repositories.ProjectRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ColumnIntegrationTest extends AbstractIntegrationTest {
    private AddColumnInputDTO addColumnInputDTO;
    private CreateProjectInputDTO projectInputDTO;



    @Autowired
    private ColumnRepository columnRepository;

    @Autowired
    private  ProjectRepository projectRepository;


    @LocalServerPort
    private int localServerPort;


    @Mock
    private TokenProvider tokenProvider;

    private String createProjectUrl;

    private String projectUrl;

    private String renameColumnUrl;

    private String fetchColumnUrl;


    @BeforeClass
    public static void configuration() {
        System.setProperty("mongodb.host", "localhost");
        System.setProperty("mongodb.port", "27017");
    }

    @Before
    public void setUp(){
        createProjectUrl = "http://localhost:" + localServerPort + "/v1/admin/project/create";
        projectUrl = "http://localhost:"+localServerPort+"/v1/project/{0}/";
        renameColumnUrl = "http://localhost:"+localServerPort+"/v1/project/{0}/column/";
        fetchColumnUrl = "http://localhost:"+localServerPort+"/v1/project/{0}/columns";
        createAndlogInUser();

    }

    @Test
    public void when_add_column_to_project_returns_successful(){
        ColumnResponseDTO columnResponseDTO = addColumn();
        assertNotNull(columnResponseDTO.getColumn());
        assertNotNull(columnResponseDTO.getColumn().getId());
        assertNotNull(columnResponseDTO.getColumn().getCreatedTime());
        assertEquals(3,columnResponseDTO.getColumn().getPosition());

    }

    @Test
    public void when_add_column_to_project_with_duplicate_name_returns_already_exists() {
        ProjectResponseDTO responseDTO = createProject();
        String projectId = responseDTO.getProject().getId();
        String columnName = "dummyColumn";
        AddColumnInputDTO addColumnInputDTO = new AddColumnInputDTO();
        addColumnInputDTO.setName(columnName);

        HttpEntity entity = new HttpEntity<>(addColumnInputDTO, headers);

        ResponseEntity<String> firstEntity =
                restTemplate.exchange(MessageFormat.format(projectUrl, projectId), HttpMethod.PUT, entity ,String.class);
        assertNotNull(firstEntity);
        assertEquals(HttpStatus.CREATED, firstEntity.getStatusCode());

        ResponseEntity<String> secondEntity =
                restTemplate.exchange(MessageFormat.format(projectUrl, projectId), HttpMethod.PUT, entity ,String.class);

        assertEquals(HttpStatus.BAD_REQUEST, secondEntity.getStatusCode());


    }

    @Test
    public void when_add_column_to_project_with_null_name_returns_valid_error() {
        ProjectResponseDTO responseDTO = createProject();
        String projectId = responseDTO.getProject().getId();
        ResponseEntity<String> responseEntity = addNewColumn(null,projectId);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, readValue(responseEntity.getBody(), BasicResponseDTO.class).getStatus());

    }

    @Test
    public void when_add_column_to_project_with_name_above_max_value_returns_valid_error() {
        ProjectResponseDTO responseDTO = createProject();
        String projectId = responseDTO.getProject().getId();
        String name = "jhwfucuf;ohcufohcohwofubckbwkoeuugkcbqudierf;furi;qugfyfqxahxbkuibcqxdwgqigcvqcqufflqchvuyqeuyfrfcxveuqoyuj  lgdeut  ljdyl   duc x   gvdxv    ";
        ResponseEntity<String> responseEntity = addNewColumn(name,projectId);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, readValue(responseEntity.getBody(), BasicResponseDTO.class).getStatus());
    }

    @Test
    public void when_add_column_to_project_with_name_below_min_value_returns_valid_error() {
        ProjectResponseDTO responseDTO = createProject();
        String projectId = responseDTO.getProject().getId();
        String columnName = "j";
        ResponseEntity<String> responseEntity = addNewColumn(columnName,projectId);
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, readValue(responseEntity.getBody(), BasicResponseDTO.class).getStatus());
    }

    @Test
    public void when_rename_column_with_valid_parameters_returns_successful(){
        ProjectResponseDTO responseDTO = createProject();
        String projectId = responseDTO.getProject().getId();
        String columnId = responseDTO.getColumnList().get(0).getId();
        String columnName = "dummyColumn";
        ResponseEntity<String> columnResponseEntity = renameColumn(columnName,projectId,columnId);
        assertEquals(HttpStatus.OK, columnResponseEntity.getStatusCode());
        ColumnResponseDTO columnResponseDTO = readValue(columnResponseEntity.getBody(), ColumnResponseDTO.class);
        assertNotNull(columnResponseDTO.getColumn());
        assertNotNull(columnResponseDTO.getColumn().getId());
        assertNotNull(columnResponseDTO.getColumn().getCreatedTime());
    }

    private ResponseEntity<String> renameColumn(String newColumnName ,String projectId,String columnId){
        RenameColumnInputDTO renameColumnInputDTO = new RenameColumnInputDTO();
        renameColumnInputDTO.setName(newColumnName);
        HttpEntity entity = new HttpEntity<>(renameColumnInputDTO, headers);
        ResponseEntity<String> columnResponseEntity =
                restTemplate.exchange(MessageFormat.format(renameColumnUrl,projectId)+ columnId, HttpMethod.PUT, entity, String.class);
        return columnResponseEntity;
    }


    @Test
    public void when_rename_column_with_name_above_max_character_returns_valid_error(){
        ProjectResponseDTO responseDTO = createProject();
        String projectId = responseDTO.getProject().getId();
        String columnId = responseDTO.getColumnList().get(0).getId();
        String columnName = "Hardwalknvg;;kvfk;dncir;eovnuhtknnkgnnebuitkghvbknxfiuvbeiuuievgkbecfbvleigvbjuihwiuhpre";
        ResponseEntity<String> columnResponseEntity = renameColumn(columnName,projectId,columnId);
        assertEquals(HttpStatus.BAD_REQUEST, columnResponseEntity.getStatusCode());
        ColumnResponseDTO columnResponseDTO = readValue(columnResponseEntity.getBody(), ColumnResponseDTO.class);
    }


    @Test
    public void when_rename_column_with_name_below_min_character_returns_valid_error(){
        ProjectResponseDTO responseDTO = createProject();
        String projectId = responseDTO.getProject().getId();
        String columnId = responseDTO.getColumnList().get(0).getId();
        String columnName = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789011121331415161718192021222324252627282930";
        ResponseEntity<String> columnResponseEntity = renameColumn(columnName,projectId,columnId);
        assertEquals(HttpStatus.BAD_REQUEST, columnResponseEntity.getStatusCode());
        ColumnResponseDTO columnResponseDTO = readValue(columnResponseEntity.getBody(), ColumnResponseDTO.class);

    }


    @Test
    public void when_reposition_columns_with_valid_columns_in_project_returns_successful(){
        ProjectResponseDTO CreateProjectresponseDTO = createProject();
        String projectId = CreateProjectresponseDTO.getProject().getId();
        String columnName = "testColumn";
        ResponseEntity<String> addColumnResponseEntity = addNewColumn(columnName,projectId);
        assertEquals(HttpStatus.CREATED, addColumnResponseEntity.getStatusCode());
        ColumnResponseDTO columnResponseDTO = readValue(addColumnResponseEntity.getBody(), ColumnResponseDTO.class);
        assertNotNull(columnResponseDTO.getColumn());
        assertNotNull(columnResponseDTO.getColumn().getId());
        assertNotNull(columnResponseDTO.getColumn().getCreatedTime());
        List<Column> columnList = CreateProjectresponseDTO.getColumnList();
        ResponseEntity<String> repositionColumnResponseEntity = repositionColumnValid(columnList,projectId);
        assertEquals(HttpStatus.OK,repositionColumnResponseEntity.getStatusCode());
        ColumnListResponseDTO columnListResponseDTO = readValue(repositionColumnResponseEntity.getBody(),ColumnListResponseDTO.class);
        assertEquals(4,columnListResponseDTO.getColumnList().size());
    }

    @Test
    public void when_reposition_columns_with_inValid_columns_in_project_return_validation_error(){
        ProjectResponseDTO CreateProjectresponseDTO = createProject();
        String projectId = CreateProjectresponseDTO.getProject().getId();
        String columnName = "testColumn";
        ResponseEntity<String> addColumnResponseEntity = addNewColumn(columnName,projectId);
        assertEquals(HttpStatus.CREATED, addColumnResponseEntity.getStatusCode());
        ColumnResponseDTO columnResponseDTO = readValue(addColumnResponseEntity.getBody(), ColumnResponseDTO.class);
        assertNotNull(columnResponseDTO.getColumn());
        assertNotNull(columnResponseDTO.getColumn().getId());
        assertNotNull(columnResponseDTO.getColumn().getCreatedTime());
        List<Column> columnList = CreateProjectresponseDTO.getColumnList();
        ResponseEntity<String> repositionColumnResponseEntity = repositionColumnInValid(columnList,projectId);
        assertEquals(HttpStatus.NOT_FOUND,repositionColumnResponseEntity.getStatusCode());
        ColumnListResponseDTO columnListResponseDTO = readValue(repositionColumnResponseEntity.getBody(),ColumnListResponseDTO.class);
    }
    @Test
    public  void when_delete_column_return_successful(){
        ColumnResponseDTO columnResponseDTO = addColumn();
        String columnId = columnResponseDTO.getColumn().getId();
        String projectId = columnResponseDTO.getColumn().getProjectId();
        HttpEntity entity = new HttpEntity<>(headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(MessageFormat.format(renameColumnUrl,projectId)+columnId +"/delete",HttpMethod.DELETE,entity,String.class);
        assertEquals(HttpStatus.OK,responseEntity.getStatusCode());

    }

    @Test
    public void when_delete_invalid_column_return_not_found(){
        ColumnResponseDTO columnResponseDTO = addColumn();
        String columnId =  "invalidColumnId";
        String projectId = columnResponseDTO.getColumn().getProjectId();
        HttpEntity entity = new HttpEntity<>(headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(MessageFormat.format(renameColumnUrl,projectId)+columnId +"/delete" ,HttpMethod.DELETE,entity,String.class);
        assertEquals(HttpStatus.NOT_FOUND,responseEntity.getStatusCode());
    }



    private ResponseEntity<String> repositionColumnValid(List<Column> columnList, String projectId){
        RepositionColumnInputDTO repositionColumnInputDTO = new RepositionColumnInputDTO();
        List<String> columnIdList = new ArrayList<String>();
        for (Column column : columnList ){
            columnIdList.add(column.getId());
        }
        repositionColumnInputDTO.setColumnIdList(columnIdList);

        HttpEntity repositionColumnEntity = new HttpEntity<>(repositionColumnInputDTO,headers);
        ResponseEntity<String> repositionColumnResponseEntity =
                restTemplate.exchange( MessageFormat.format(projectUrl,projectId) + "/column/reposition", HttpMethod.PUT, repositionColumnEntity, String.class);
        return repositionColumnResponseEntity;
    }

    private ResponseEntity<String> repositionColumnInValid(List<Column> columnList, String projectId){
        RepositionColumnInputDTO repositionColumnInputDTO = new RepositionColumnInputDTO();
        List<String> columnIdList = new ArrayList<String>();
        for (Column column : columnList ){
            columnIdList.add(column.getId());
            columnIdList.add("invalidColumnId");
        }
        repositionColumnInputDTO.setColumnIdList(columnIdList);

        HttpEntity repositionColumnEntity = new HttpEntity<>(repositionColumnInputDTO,headers);
        ResponseEntity<String> repositionColumnResponseEntity =
                restTemplate.exchange(MessageFormat.format(projectUrl,projectId) + "/column/reposition", HttpMethod.PUT, repositionColumnEntity, String.class);
        return repositionColumnResponseEntity;
    }
    private ResponseEntity<String> addNewColumn(String columnName,String projectId){
        AddColumnInputDTO addColumnInputDTO = new AddColumnInputDTO();
        addColumnInputDTO.setName(columnName);

        HttpEntity entity = new HttpEntity<>(addColumnInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.exchange(MessageFormat.format(projectUrl,projectId), HttpMethod.PUT, entity ,String.class);
        return responseEntity;
    }
    private ProjectResponseDTO createProject(){
        String projectName = "dummyProject";

        CreateProjectInputDTO projectInputDTO = new CreateProjectInputDTO();
        projectInputDTO.setName(projectName);

        HttpEntity projectEntity = new HttpEntity<>(projectInputDTO, headers);

        ResponseEntity<String> projectResponseEntity =
                restTemplate.postForEntity(createProjectUrl, projectEntity, String.class);
        assertEquals(HttpStatus.CREATED, projectResponseEntity.getStatusCode());

        ProjectResponseDTO responseDTO = readValue(projectResponseEntity.getBody(), ProjectResponseDTO.class);
        return responseDTO;
    }

    private ColumnResponseDTO addColumn(){
        ProjectResponseDTO responseDTO = createProject();

        String projectId = responseDTO.getProject().getId();
        String columnName = "Eleazar";

        AddColumnInputDTO addColumnInputDTO = new AddColumnInputDTO();
        addColumnInputDTO.setName(columnName);
        HttpEntity entity = new HttpEntity<>(addColumnInputDTO, headers);

        ResponseEntity<String> columnResponseEntity =
                restTemplate.exchange(MessageFormat.format(projectUrl,projectId), HttpMethod.PUT, entity ,String.class,projectId);
        assertEquals(HttpStatus.CREATED, columnResponseEntity.getStatusCode());
        ColumnResponseDTO columnResponseDTO = readValue(columnResponseEntity.getBody(), ColumnResponseDTO.class);
        return columnResponseDTO;
    }

    @Test
    public void when_fetch_columns_returns_ValidData(){
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();

        HttpEntity entity = new HttpEntity<>(headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(MessageFormat.format(fetchColumnUrl, projectId),HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK ,responseEntity.getStatusCode());

        ColumnListResponseDTO columnListResponseDTO =  new Gson().fromJson(responseEntity.getBody(), ColumnListResponseDTO .class);

        assertNotNull( columnListResponseDTO);
        assertEquals(Status.SUCCESS , columnListResponseDTO.getStatus());
    }

    @Test
    public void when_fetch_columns_with_invalid_project_id_returns_valid_error(){

        String projectId = "invalid id";

        HttpEntity entity = new HttpEntity<>(headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(MessageFormat.format(fetchColumnUrl, projectId),HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.NOT_FOUND ,responseEntity.getStatusCode());

        ColumnListResponseDTO columnListResponseDTO =  new Gson().fromJson(responseEntity.getBody(), ColumnListResponseDTO .class);

        assertNotNull( columnListResponseDTO);
        assertEquals(Status.NOT_FOUND , columnListResponseDTO.getStatus());
    }



    @After
    public void cleanUp() {
        projectRepository.deleteAll();
        columnRepository.deleteAll();

    }


}
