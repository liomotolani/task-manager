package com.photizzo.quabbly.taskmanager.integration;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.photizzo.quabbly.taskmanager.config.TokenProvider;
import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.dto.input.*;
import com.photizzo.quabbly.taskmanager.dto.output.ProjectResponseDTO;
import com.photizzo.quabbly.taskmanager.dto.output.TaskListResponseDTO;
import com.photizzo.quabbly.taskmanager.dto.output.TaskResponseDTO;
import com.photizzo.quabbly.taskmanager.dto.remote.request.SendEmailRequestDTO;
import com.photizzo.quabbly.taskmanager.model.extras.Assignee;
import com.photizzo.quabbly.taskmanager.model.extras.Watchers;
import com.photizzo.quabbly.taskmanager.repositories.ProjectRepository;
import com.photizzo.quabbly.taskmanager.repositories.TaskRepository;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TaskIntegrationTest extends AbstractIntegrationTest {

    private CreateTaskInputDTO createTaskInputDTO;
    private CreateProjectInputDTO createProjectInputDTO;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private Environment env;


    @LocalServerPort
    private int localServerPort;

    @Mock
    private TokenProvider tokenProvider;


    private String notificationUrl;

    private String createProjectUrl;

    private String createTaskUrl;

    private String fetchTasksUrl;

    private String createCommentUrl;

    private String createSubtaskUrl;

    private String projectUrl;


    @BeforeClass
    public static void configuration() {
        System.setProperty("mongodb.host", "localhost");
        System.setProperty("mongodb.port", "27017");
    }

    @Before
    public void setUp(){
        System.setProperty("mongodb.host", "localhost");
        System.setProperty("mongodb.port", "27017");
        createProjectUrl = "http://localhost:" + localServerPort + "/v1/admin/project/create";
        createTaskUrl = "http://localhost:" + localServerPort + "/v1/project/{0}/task/";
        fetchTasksUrl = "http://localhost:" + localServerPort + "/v1/project/{0}/tasks/";
        createCommentUrl = "http://localhost:"+localServerPort+"/v1/project/{0}/comment/";
        createSubtaskUrl = "http://localhost:"+localServerPort+"/v1/project/{0}/subtask/";
        notificationUrl = "https://p-notification-api-dev.quabbly.com/v1/send-notification-email";


        createAndlogInUser();

    }

    private ProjectResponseDTO createProject(){
        String projectName = "dummyProject";

        CreateProjectInputDTO projectInputDTO = new CreateProjectInputDTO();
        projectInputDTO.setName(projectName);

        HttpEntity projectEntity = new HttpEntity<>(projectInputDTO, headers);

        ResponseEntity<String> projectResponseEntity =
                restTemplate.postForEntity(createProjectUrl, projectEntity, String.class);
        assertEquals(HttpStatus.CREATED, projectResponseEntity.getStatusCode());

        ProjectResponseDTO responseDTO = readValue(projectResponseEntity.getBody(), ProjectResponseDTO.class);
        return responseDTO;
    }

    private ResponseEntity<String> createTask(String taskSummary,String projectId){
        CreateTaskInputDTO createTaskInputDTO = new CreateTaskInputDTO();
        createTaskInputDTO.setSummary(taskSummary);

        HttpEntity taskEntity = new HttpEntity<>(createTaskInputDTO, headers);

        ResponseEntity<String> taskResponseEntity =
                restTemplate.postForEntity(MessageFormat.format(createTaskUrl, projectId), taskEntity, String.class);
        return taskResponseEntity;
    }

    @Test
    public void when_create_task_with_valid_summary_returns_successful() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Task Summary";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);

        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);

        assertNotNull(taskResponseDTO.getTask().getCreatedTime());
        assertNotNull(taskResponseDTO.getTask().getId());
        assertNotNull(taskResponseDTO.getTask().getTaskSecret());
        assertNotNull(taskResponseDTO.getTask().getTaskSecret().getSummary());

        assertEquals(taskSummary,taskResponseDTO.getTask().getTaskSecret().getSummary());
        assertEquals(0,taskResponseDTO.getTask().getPosition());

    }


    @Test
    public void when_create_task_with_invalid_summary_returns_unsuccessful() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);

        assertEquals(HttpStatus.BAD_REQUEST, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
    }



    @Test
     public void when_fetch_task_with_valid_id_returns_successful() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Task Summary";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);

        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);

        String taskId = taskResponseDTO.getTask().getId();

        ResponseEntity<String> fetchTaskResponseEntity = fetchTask(taskId,projectId);
        assertEquals(HttpStatus.OK, fetchTaskResponseEntity.getStatusCode());
        assertEquals(Status.SUCCESS, readValue(fetchTaskResponseEntity.getBody(), TaskResponseDTO.class).getStatus());

        TaskResponseDTO fetchTaskResponseDTO = readValue(fetchTaskResponseEntity.getBody(),TaskResponseDTO.class);

        assertNotNull(fetchTaskResponseDTO.getTask().getTaskSecret());
        assertNotNull(fetchTaskResponseDTO.getTask().getTaskSecret().getSummary());
        assertNotNull(fetchTaskResponseDTO.getTask().getCreatedTime());
        assertNotNull(fetchTaskResponseDTO.getTask().getId());

        assertEquals(0,fetchTaskResponseDTO.getTask().getPosition());
        assertEquals(taskSummary,fetchTaskResponseDTO.getTask().getTaskSecret().getSummary());
        assertEquals(taskId,fetchTaskResponseDTO.getTask().getId());
    }
    @Test
    public void when_fetch_task_with_invalid_id_returns_not_found() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Task Summary";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);

        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);

        String taskId = "5c9feeeeeeeeaeeeeeeeeeee";

        ResponseEntity<String> fetchTaskResponseEntity = fetchTask(taskId,projectId);
        assertEquals(HttpStatus.NOT_FOUND, fetchTaskResponseEntity.getStatusCode());
        assertEquals(Status.NOT_FOUND, readValue(fetchTaskResponseEntity.getBody(), TaskResponseDTO.class).getStatus());
    }

    private ResponseEntity<String> fetchTask(String taskId,String projectId) {
        HttpEntity entity = new HttpEntity<>(headers);

        ResponseEntity<String> fetchTaskResponseEntity =
                restTemplate.exchange(MessageFormat.format(createTaskUrl, projectId) + taskId, HttpMethod.GET, entity, String.class);
        return fetchTaskResponseEntity;
    }


    @Test
    public void when_move_task_returns_successful() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Task Summary";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);

        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);

        String taskId = taskResponseDTO.getTask().getId();
        String newColumnId = responseDTO.getColumnList().get(1).getId();
        ResponseEntity<String> moveTaskResponseEntity = moveTask(projectId,newColumnId,taskId);

        assertEquals(HttpStatus.OK, moveTaskResponseEntity.getStatusCode());
        assertEquals(Status.SUCCESS, readValue(moveTaskResponseEntity.getBody(), TaskResponseDTO.class).getStatus());

        TaskResponseDTO moveTaskResponseDTO = readValue(moveTaskResponseEntity.getBody(),TaskResponseDTO.class);


        assertNotNull(moveTaskResponseDTO.getTask().getTaskSecret());
        assertNotNull(moveTaskResponseDTO.getTask().getTaskSecret().getSummary());
        assertNotNull(moveTaskResponseDTO.getTask().getCreatedTime());
        assertNotNull(moveTaskResponseDTO.getTask().getId());

        assertEquals(0,moveTaskResponseDTO.getTask().getPosition());
        assertEquals(taskSummary,moveTaskResponseDTO.getTask().getTaskSecret().getSummary());
        assertEquals(taskId,moveTaskResponseDTO.getTask().getId());
    } //TODO : remember to update position of tasks when moving them check your service implementation

    private ResponseEntity<String> moveTask(String projectId, String newColumnId, String taskId) {
        MoveTaskInputDTO moveTaskInputDTO = new MoveTaskInputDTO();
        moveTaskInputDTO.setColumnId(newColumnId);

        HttpEntity entity = new HttpEntity<>(moveTaskInputDTO,headers);

        ResponseEntity<String> moveTaskResponseEntity =
                restTemplate.exchange(MessageFormat.format(createTaskUrl, projectId) + taskId + "/move", HttpMethod.PUT, entity, String.class);
        return  moveTaskResponseEntity;
    }

    private static  Resource getUserFileResource() throws IOException {
        Path tempFile = Files.createTempFile("upload-test-file", ".txt");
        Files.write(tempFile, "some test content...\nline1\nline2".getBytes());
        File file = tempFile.toFile();
        return new FileSystemResource(file);
    }


    @Test
    public void when_add_attachment_to_task_returns_successful() throws IOException {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Task Summary";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);
        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
        String taskId = taskResponseDTO.getTask().getId();

        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body
                = new LinkedMultiValueMap<>();
        body.add("files", getUserFileResource());
        body.add("files", getUserFileResource());
        body.add("files", getUserFileResource());

        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, headers);

        String serverUrl = env.getProperty("file.manager.url");

        CloseableHttpClient httpClient
                = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
        HttpComponentsClientHttpRequestFactory requestFactory
                = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        RestTemplate uploadRestTemplate = new RestTemplate(requestFactory);
        ResponseEntity<String> response = uploadRestTemplate.postForEntity(serverUrl, requestEntity, String.class);
        JsonObject uploadToken =  new Gson().fromJson(response.getBody(),JsonObject.class);

        String token = uploadToken.get("data").getAsJsonObject().get("token").getAsString();
        UpdateTaskInputDTO updateTaskInputDTO = new UpdateTaskInputDTO();
        updateTaskInputDTO.setAttachments(token);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("attachments");
        updateTaskInputDTO.setUpdatedFields(updatedFields);
        headers.setContentType(MediaType.APPLICATION_JSON);
        String subject =  "file has been uploaded in task"+ taskSummary;
        //String attachmentbody = env.getProperty("update.attachments.body");
       // emailData(subject,attachmentbody);
        HttpEntity updateTaskEntity = new HttpEntity<>(updateTaskInputDTO, headers);

        ResponseEntity<String> updateTaskResponseEntity =
                restTemplate.exchange(MessageFormat.format(createTaskUrl, projectId) + taskId, HttpMethod.PUT, updateTaskEntity ,String.class,taskId);





        assertEquals(HttpStatus.OK , updateTaskResponseEntity.getStatusCode());
        TaskResponseDTO updateTaskResponseDTO = readValue(updateTaskResponseEntity.getBody(), TaskResponseDTO.class);
        assertNotNull(updateTaskResponseDTO.getTask().getId());
        assertNotNull(updateTaskResponseDTO.getTask().getCreatedTime());
        assertNotNull(updateTaskResponseDTO.getTask().getLastUpdatedTime());
        assertNotNull(updateTaskResponseDTO.getTask().getTaskSecret());
        assertNotNull(updateTaskResponseDTO.getTask().getTaskSecret().getAttachments());

  }





//    @Test
//    public void when_update_task_summary_returns_successful(){
//        ProjectResponseDTO responseDTO = createProject();
//        assertNotNull(responseDTO.getProject().getId());
//        String projectId = responseDTO.getProject().getId();
//        String taskSummary = "Task Summary";
//        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);
//        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
//        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
//        String taskId = taskResponseDTO.getTask().getId();
//        String newTaskSummary = "new Task Summary";
//        ResponseEntity<String> updateTaskResponseEntity = updateTaskSummary(projectId,taskId,newTaskSummary);
//        assertEquals(HttpStatus.OK , updateTaskResponseEntity.getStatusCode());
//        String subject = taskSummary + "has been updated from" + "to" + newTaskSummary;
//        String body = env.getProperty("update.summary.body");
//        emailData(subject,body);
//        TaskResponseDTO updateTaskResponseDTO = readValue(updateTaskResponseEntity.getBody(), TaskResponseDTO.class);
//        assertEquals(newTaskSummary,updateTaskResponseDTO.getTask().getTaskSecret().getSummary());
//    }

//    @Test
//    public void when_add_description_to_task_returns_successful(){
//        ProjectResponseDTO responseDTO = createProject();
//        assertNotNull(responseDTO.getProject().getId());
//        String projectId = responseDTO.getProject().getId();
//        String taskSummary = "Task Summary";
//        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);
//        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
//        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
//        String taskId = taskResponseDTO.getTask().getId();
//        String description = "We want make things work";
//        String subject = description+ "has been updated to in" + taskSummary;
//        String body = env.getProperty("update.description.body");
//        emailData(subject,body);
//        ResponseEntity<String> updateTaskResponseEntity = addDescription(projectId,taskId,description);
//        assertEquals(HttpStatus.OK , updateTaskResponseEntity.getStatusCode());
//        TaskResponseDTO updateTaskResponseDTO = readValue(updateTaskResponseEntity.getBody(), TaskResponseDTO.class);
//    }


    private ResponseEntity<String> addDescription(String projectId, String taskId, String taskDescription) {
        UpdateTaskInputDTO updateTaskInputDTO = new UpdateTaskInputDTO();
        updateTaskInputDTO.setDescription(taskDescription);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("description");
        updateTaskInputDTO.setUpdatedFields(updatedFields);

        HttpEntity updateTaskEntity = new HttpEntity<>(updateTaskInputDTO, headers);

        ResponseEntity<String> updateTaskResponseEntity =
                restTemplate.exchange(MessageFormat.format(createTaskUrl, projectId) + taskId, HttpMethod.PUT, updateTaskEntity ,String.class,taskId);
        return updateTaskResponseEntity;
    }

    private ResponseEntity<String> updateTaskSummary(String projectId, String taskId, String newTaskSummary) {
        UpdateTaskInputDTO updateTaskInputDTO = new UpdateTaskInputDTO();
        updateTaskInputDTO.setSummary(newTaskSummary);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("summary");
        updateTaskInputDTO.setUpdatedFields(updatedFields);

        HttpEntity updateTaskEntity = new HttpEntity<>(updateTaskInputDTO, headers);

        ResponseEntity<String> updateTaskResponseEntity =
                restTemplate.exchange(MessageFormat.format(createTaskUrl, projectId) + taskId, HttpMethod.PUT, updateTaskEntity ,String.class,taskId);
        return updateTaskResponseEntity;
    }

    @Test
    public void when_add_watcher_to_task_returns_successful(){
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Task Summary";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);
        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
        String taskId = taskResponseDTO.getTask().getId();
        boolean watcher = true;
        ResponseEntity<String> updateTaskResponseEntity = addWatcher(projectId,taskId,watcher);
        assertEquals(HttpStatus.OK , updateTaskResponseEntity.getStatusCode());
        TaskResponseDTO updateTaskResponseDTO = readValue(updateTaskResponseEntity.getBody(), TaskResponseDTO.class);
//        assertEquals(watcher,updateTaskResponseDTO.getTask().getTaskSecret().getWatchers().get(0).get);
    }

//    @Test
//    public void when_add_assignee_to_task_returns_successful(){
//        ProjectResponseDTO responseDTO = createProject();
//        assertNotNull(responseDTO.getProject().getId());
//        String projectId = responseDTO.getProject().getId();
//        String taskSummary = "Task Summary";
//        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);
//        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
//        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
//        String taskId = taskResponseDTO.getTask().getId();
//        Assignee assignee = new Assignee();
//        assignee.setUserEmail("testUser@testTenant.com");
//        assignee.setUserName("testUser");
//        assignee.setCreatedTime(new Date());
//        ResponseEntity<String> updateTaskResponseEntity = addAssignee(projectId,taskId,assignee);
//        assertEquals(HttpStatus.OK , updateTaskResponseEntity.getStatusCode());
//        String subject = assignee.getUserName() +" has been assigned a task";
//        String body = env.getProperty("update.assignee.body");
//        emailData(subject,body);
//        TaskResponseDTO updateTaskResponseDTO = readValue(updateTaskResponseEntity.getBody(), TaskResponseDTO.class);
//    }

    private void emailData(String body,String subject){
        Assignee assignee = new Assignee();
        assignee.setUserEmail("testUser@testTenant.com");
        String userEmail = assignee.getUserEmail();
        SendEmailRequestDTO sendEmailRequestDTO = new SendEmailRequestDTO();
        sendEmailRequestDTO.setRecipient(Lists.newArrayList(userEmail));
        List<String> recipientsList = new ArrayList<>();
        recipientsList.add(userEmail);
        Watchers watchers = new Watchers();
        watchers.setUserEmail("watchers@us.com");
        String watcherUserEmail = watchers.getUserEmail();
        sendEmailRequestDTO.setCc(Lists.newArrayList( watcherUserEmail));
        List<String> ccList = new ArrayList<>();
        ccList.add(watcherUserEmail);
        ResponseEntity<String> sendEmailResponseEntity = sendEmail(recipientsList,ccList,subject,body);
        assertEquals(HttpStatus.OK , sendEmailResponseEntity.getStatusCode());
    }

    private ResponseEntity<String> sendEmail(List<String> recipients, List<String> cc, String subject, String body){

        CloseableHttpClient httpClient
                = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
        HttpComponentsClientHttpRequestFactory requestFactory
                = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        RestTemplate RestTemplate = new RestTemplate(requestFactory);
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<String, String>();
        requestBody.add("htmlContent", body);
        requestBody.add("subject", subject);
        requestBody.put("recipient", recipients);
        requestBody.put("cc",cc);
        headersEncoded.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        HttpEntity emailEntity = new HttpEntity<>(requestBody, headersEncoded);

        ResponseEntity<String> emailResponseEntity =
                RestTemplate.postForEntity(notificationUrl , emailEntity, String.class);

        return emailResponseEntity;
    }


    private ResponseEntity<String> addAssignee(String projectId, String taskId, Assignee assignee) {
        UpdateTaskInputDTO updateTaskInputDTO = new UpdateTaskInputDTO();
        updateTaskInputDTO.setAssignee(assignee);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("assignee");
        updateTaskInputDTO.setUpdatedFields(updatedFields);
        HttpEntity updateTaskEntity = new HttpEntity<>(updateTaskInputDTO, headers);

        ResponseEntity<String> updateTaskResponseEntity =
                restTemplate.exchange(MessageFormat.format(createTaskUrl, projectId) + taskId, HttpMethod.PUT, updateTaskEntity ,String.class,taskId);
        return updateTaskResponseEntity;
    }

    @Test
    public void when_remove_watcher_to_task_returns_successful(){
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Task Summary";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);
        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
        String taskId = taskResponseDTO.getTask().getId();
        boolean startwatching = true;
        ResponseEntity<String> updateTaskResponseEntity = addWatcher(projectId,taskId,startwatching);
        assertEquals(HttpStatus.OK , updateTaskResponseEntity.getStatusCode());
        boolean stopwatching = false;
        ResponseEntity<String> updateTaskResponseEntity2 = addWatcher(projectId,taskId,stopwatching);
        assertEquals(HttpStatus.OK , updateTaskResponseEntity2.getStatusCode());
        TaskResponseDTO updateTaskResponseDTO = readValue(updateTaskResponseEntity.getBody(), TaskResponseDTO.class);
//        assertEquals(watcher,updateTaskResponseDTO.getTask().getTaskSecret().getWatchers().get(0).get);
    }


    private ResponseEntity<String> addWatcher(String projectId, String taskId, boolean watcher) {
        UpdateTaskInputDTO updateTaskInputDTO = new UpdateTaskInputDTO();
        updateTaskInputDTO.setWatcher(watcher);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("watcher");
        updateTaskInputDTO.setUpdatedFields(updatedFields);
        HttpEntity updateTaskEntity = new HttpEntity<>(updateTaskInputDTO, headers);

        ResponseEntity<String> updateTaskResponseEntity =
                restTemplate.exchange(MessageFormat.format(createTaskUrl, projectId) + taskId, HttpMethod.PUT, updateTaskEntity ,String.class,taskId);
        return updateTaskResponseEntity;
    }

    @Test
    public void when_add_subtask_to_task_returns_successful(){
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Task Summary";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);
        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
        String taskId = taskResponseDTO.getTask().getId();
        String subtaskSummary = "dummy Summary";
        ResponseEntity<String> updateTaskResponseEntity = addSubtask(projectId,taskId,subtaskSummary);
        assertEquals(HttpStatus.OK , updateTaskResponseEntity.getStatusCode());
        TaskResponseDTO updateTaskResponseDTO = readValue(updateTaskResponseEntity.getBody(), TaskResponseDTO.class);
        assertEquals(subtaskSummary,updateTaskResponseDTO.getTask().getTaskSecret().getSubTasks().get(0).getSummary());
    }

    @Test
    public void when_add_priority_to_task_returns_successful(){
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Task Summary";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);
        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
        String taskId = taskResponseDTO.getTask().getId();
        String priority = "high";
        ResponseEntity<String> updateTaskResponseEntity = addPriority(projectId,taskId,priority);
        assertEquals(HttpStatus.OK , updateTaskResponseEntity.getStatusCode());
        TaskResponseDTO updateTaskResponseDTO = readValue(updateTaskResponseEntity.getBody(), TaskResponseDTO.class);
        assertEquals(priority,updateTaskResponseDTO.getTask().getTaskSecret().getPriority());
    }

    @Test
    public void when_add_comment_to_task_returns_successful(){
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();


        String taskSummary = "Task Summary";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);

        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
        String taskId = taskResponseDTO.getTask().getId();
        String comment = "dummy Comment";
        ResponseEntity<String> updateTaskResponseEntity = addComment(projectId,taskId,comment);
        assertEquals(HttpStatus.OK , updateTaskResponseEntity.getStatusCode());
        TaskResponseDTO updateTaskResponseDTO = readValue(updateTaskResponseEntity.getBody(), TaskResponseDTO.class);
        assertEquals(comment,updateTaskResponseDTO.getTask().getTaskSecret().getComments().get(0).getComment());
    }


    @Test
    public void when_fetch_all_tasks_returns_successful() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Task Summary";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);
        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        String taskSummary2 = "Task summary 2";
        ResponseEntity<String> taskResponseEntity2 = createTask(taskSummary2,projectId);
        assertEquals(HttpStatus.CREATED, taskResponseEntity2.getStatusCode());
        HttpEntity entity = new HttpEntity<>(headers);
        ResponseEntity<String> responseEntity =
                restTemplate.exchange(MessageFormat.format(fetchTasksUrl, projectId), HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(Status.SUCCESS, readValue(responseEntity.getBody(), TaskListResponseDTO.class).getStatus());
        TaskListResponseDTO fetchTasksResponseDTO = readValue(responseEntity.getBody(), TaskListResponseDTO.class);
        assertNotNull(fetchTasksResponseDTO.getTasks().get(0).getCreatedTime());
        assertNotNull(fetchTasksResponseDTO.getTasks().get(0).getTaskSecret().getSummary());
        assertNotNull(fetchTasksResponseDTO.getTasks().get(0).getTaskSecret());
        assertNotNull(fetchTasksResponseDTO.getTasks().get(0).getId());

        assertNotNull(fetchTasksResponseDTO.getTasks().get(1).getCreatedTime());
        assertNotNull(fetchTasksResponseDTO.getTasks().get(1).getTaskSecret().getSummary());
        assertNotNull(fetchTasksResponseDTO.getTasks().get(1).getTaskSecret());
        assertNotNull(fetchTasksResponseDTO.getTasks().get(1).getId());

        assertEquals(0,fetchTasksResponseDTO.getTasks().get(0).getPosition());
        assertEquals(taskSummary,fetchTasksResponseDTO.getTasks().get(0).getTaskSecret().getSummary());

        assertEquals(0,fetchTasksResponseDTO.getTasks().get(1).getPosition());
        assertEquals(taskSummary2,fetchTasksResponseDTO.getTasks().get(1).getTaskSecret().getSummary());

    }

    @Test
    public void when_delete_task_returns_successful() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "task Summary";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);
        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
        String taskId = taskResponseDTO.getTask().getId();
        HttpEntity deleteTaskEntity = new HttpEntity<>( headers);
        ResponseEntity<String> delete = restTemplate.exchange(MessageFormat.format(createTaskUrl, projectId) + taskId, HttpMethod.DELETE, deleteTaskEntity, String.class, taskId);
        assertEquals(HttpStatus.OK,delete.getStatusCode());
        ResponseEntity<String> fetchDeletedTaskResponseEntity = restTemplate.exchange(MessageFormat.format(createTaskUrl, projectId) + taskId, HttpMethod.GET, deleteTaskEntity, String.class);
        assertEquals(HttpStatus.NOT_FOUND,fetchDeletedTaskResponseEntity.getStatusCode());
        TaskResponseDTO deletedTask  = readValue(fetchDeletedTaskResponseEntity.getBody(),TaskResponseDTO.class);
        assertNull(deletedTask.getTask());
    }



    @Test
    public void when_delete_comment_returns_successful() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Task Summary";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);
        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
        String taskId = taskResponseDTO.getTask().getId();
        String comment = "dummy Comment";
        ResponseEntity<String> updateTaskResponseEntity = addComment(projectId,taskId,comment);
        assertEquals(HttpStatus.OK , updateTaskResponseEntity.getStatusCode());
        TaskResponseDTO updateTaskResponseDTO = readValue(updateTaskResponseEntity.getBody(), TaskResponseDTO.class);
        String commentUuid = updateTaskResponseDTO.getTask().getTaskSecret().getComments().get(0).getUuid();
        HttpEntity deleteCommentEntity = new HttpEntity<>(headers);
        ResponseEntity deleteCommentResponseEntity =
                restTemplate.exchange("http://localhost:"+localServerPort+"/v1/project/" +projectId +"/comment/" + taskId +"/" + commentUuid,HttpMethod.DELETE,deleteCommentEntity,String.class,taskId,commentUuid);
        assertEquals(HttpStatus.OK,deleteCommentResponseEntity.getStatusCode());
    }

//    @Test
//    public void when_update_comment_returns_successful() {
//        ProjectResponseDTO responseDTO = createProject();
//        assertNotNull(responseDTO.getProject().getId());
//        String projectId = responseDTO.getProject().getId();
//        String taskSummary = "Task Summary";
//        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);
//        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
//        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
//        String taskId = taskResponseDTO.getTask().getId();
//        String comment = "dummy Comment";
//        ResponseEntity<String> updateTaskResponseEntity = addComment(projectId,taskId,comment);
//        assertEquals(HttpStatus.OK , updateTaskResponseEntity.getStatusCode());
//        TaskResponseDTO updateTaskResponseDTO = readValue(updateTaskResponseEntity.getBody(), TaskResponseDTO.class);
//        String commentUuid = updateTaskResponseDTO.getTask().getTaskSecret().getComments().get(0).getUuid();
//        String newComment = "new Comment";
//
//        String subject = comment + "has been updated to" + newComment + "in" + taskSummary;
//        String body = env.getProperty("update.comments.body");
//        emailData(subject,body);
//        ResponseEntity updateCommentResponseEntity = updateComment(projectId,taskId,commentUuid,newComment);
//        assertEquals(HttpStatus.OK,updateCommentResponseEntity.getStatusCode());
//    }

    private ResponseEntity<String> updateComment(String projectId,String taskId,String commentUuid,String newComment) {
        UpdateCommentInputDTO updateCommentInputDTO = new UpdateCommentInputDTO();
        updateCommentInputDTO.setComment(newComment);

        HttpEntity updateCommentEntity = new HttpEntity<>(updateCommentInputDTO,headers);
        ResponseEntity updateCommentResponseEntity =
                restTemplate.exchange("http://localhost:"+localServerPort+"/v1/project/" +projectId +"/comment/" + taskId +"/" + commentUuid,HttpMethod.PUT,updateCommentEntity,String.class,taskId,commentUuid);
        return updateCommentResponseEntity;
    }


    @Test
    public void when_delete_subtask_returns_successful() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Task Summary";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);
        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
        String taskId = taskResponseDTO.getTask().getId();
        String subtaskSummary = "dummy Summary";
        ResponseEntity<String> updateTaskResponseEntity = addSubtask(projectId,taskId,subtaskSummary);
        assertEquals(HttpStatus.OK , updateTaskResponseEntity.getStatusCode());
        TaskResponseDTO updateTaskResponseDTO = readValue(updateTaskResponseEntity.getBody(), TaskResponseDTO.class);
        String subtaskUuid = updateTaskResponseDTO.getTask().getTaskSecret().getSubTasks().get(0).getUuid();
        HttpEntity deleteSubtaskEntity = new HttpEntity<>(headers);
        ResponseEntity deleteSubtaskResponseEntity =
                restTemplate.exchange(MessageFormat.format(createSubtaskUrl, projectId) + taskId +"/" + subtaskUuid +"/delete",HttpMethod.DELETE,deleteSubtaskEntity,String.class,taskId,subtaskUuid);
        assertEquals(HttpStatus.OK,deleteSubtaskResponseEntity.getStatusCode());
    }

    @Test
    public void when_update_subtask_returns_successful() {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Task Summary";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);
        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
        String taskId = taskResponseDTO.getTask().getId();
        String subtaskSummary = "dummy Summary";
        ResponseEntity<String> updateTaskResponseEntity = addSubtask(projectId,taskId,subtaskSummary);
        assertEquals(HttpStatus.OK , updateTaskResponseEntity.getStatusCode());
        TaskResponseDTO updateTaskResponseDTO = readValue(updateTaskResponseEntity.getBody(), TaskResponseDTO.class);
        String subtaskUuid = updateTaskResponseDTO.getTask().getTaskSecret().getSubTasks().get(0).getUuid();
        String newSubtaskSummary = "new SubTask summary";
        ResponseEntity updateSubtaskResponseEntity = updateSubtask(projectId,taskId,subtaskUuid,newSubtaskSummary);

        assertEquals(HttpStatus.OK,updateSubtaskResponseEntity.getStatusCode());
    }

    private ResponseEntity<String> updateSubtask(String projectId, String taskId, String subtaskUuid, String newSubtaskSummary) {

        UpdateSubTaskInputDTO updateSubTaskInputDTO = new UpdateSubTaskInputDTO();
        updateSubTaskInputDTO.setSummary(newSubtaskSummary);
        HttpEntity updateSubtaskEntity = new HttpEntity<>(updateSubTaskInputDTO,headers);
        ResponseEntity updateSubtaskResponseEntity =
                restTemplate.exchange("http://localhost:"+localServerPort+"/v1/project/" +projectId +"/subtask/" + taskId +"/" + subtaskUuid,HttpMethod.PUT,updateSubtaskEntity,String.class,taskId,subtaskUuid);
        return updateSubtaskResponseEntity;
    }

    private ResponseEntity<String> addSubtask(String projectId, String taskId, String subtaskSummary) {
        UpdateTaskInputDTO updateTaskInputDTO = new UpdateTaskInputDTO();
        updateTaskInputDTO.setSubTask(subtaskSummary);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("subTask");
        updateTaskInputDTO.setUpdatedFields(updatedFields);
        HttpEntity updateTaskEntity = new HttpEntity<>(updateTaskInputDTO, headers);
        ResponseEntity<String> updateTaskResponseEntity =
                restTemplate.exchange(MessageFormat.format(createTaskUrl, projectId) + taskId, HttpMethod.PUT, updateTaskEntity ,String.class,taskId);
        return updateTaskResponseEntity;
    }
    private ResponseEntity<String> addPriority(String projectId, String taskId, String priority) {
        UpdateTaskInputDTO updateTaskInputDTO = new UpdateTaskInputDTO();
        updateTaskInputDTO.setPriority(priority);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("priority");
        updateTaskInputDTO.setUpdatedFields(updatedFields);
        HttpEntity updateTaskEntity = new HttpEntity<>(updateTaskInputDTO, headers);
        ResponseEntity<String> updateTaskResponseEntity =
                restTemplate.exchange(MessageFormat.format(createTaskUrl, projectId) + taskId, HttpMethod.PUT, updateTaskEntity ,String.class,taskId);
        return updateTaskResponseEntity;
    }

    private ResponseEntity<String> addComment(String projectId,String taskId,String comment) {
        UpdateTaskInputDTO updateTaskInputDTO = new UpdateTaskInputDTO();
        updateTaskInputDTO.setComment(comment);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("comment");
        updateTaskInputDTO.setUpdatedFields(updatedFields);

        HttpEntity updateTaskEntity = new HttpEntity<>(updateTaskInputDTO, headers);

        ResponseEntity<String> updateTaskResponseEntity =
                restTemplate.exchange(MessageFormat.format(createTaskUrl, projectId) + taskId, HttpMethod.PUT, updateTaskEntity ,String.class,taskId);
        return updateTaskResponseEntity;
    }


    private String getRandomUUID() {
        return UUID.randomUUID().toString();
    }

    @After
    public void cleanUp() {
        taskRepository.deleteAll();
        projectRepository.deleteAll();
    }

}
