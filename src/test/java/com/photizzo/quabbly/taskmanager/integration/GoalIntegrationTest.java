package com.photizzo.quabbly.taskmanager.integration;


import com.google.gson.Gson;
import com.photizzo.quabbly.taskmanager.config.TokenProvider;
import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.dto.input.*;
import com.photizzo.quabbly.taskmanager.dto.output.*;
import com.photizzo.quabbly.taskmanager.repositories.GoalRepository;
import com.photizzo.quabbly.taskmanager.repositories.ProjectRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GoalIntegrationTest extends AbstractIntegrationTest {


    private CreateProjectInputDTO createProjectInputDTO;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private GoalRepository goalRepository;

    @Autowired
    private Environment env;


    @LocalServerPort
    private int localServerPort;

    @Mock
    private TokenProvider tokenProvider;

    private String createGoalUrl;

    private String createProjectUrl;

    private String updateGoalUrl;

    private String fetchGoalsUrl;

    private String createTaskUrl;

    private String attachTaskToGoalUrl;





    @BeforeClass
    public static void configuration() {
        System.setProperty("mongodb.host", "localhost");
        System.setProperty("mongodb.port", "27017");
    }

    @Before
    public void setUp(){
        createProjectUrl = "http://localhost:" + localServerPort + "/v1/admin/project/create";
        
        createGoalUrl = "http://localhost:" + localServerPort + "/v1/project/{0}/goal";

        updateGoalUrl = "http://localhost:" + localServerPort + "/v1/project/{0}/goal/";

        fetchGoalsUrl = "http://localhost:" + localServerPort + "/v1/project/{0}/goals";

        createTaskUrl = "http://localhost:" + localServerPort + "/v1/project/{0}/task/";

        attachTaskToGoalUrl = "http://localhost:" + localServerPort + "/v1/project/{0}/goal/";



        createAndlogInUser();

    }

    private ProjectResponseDTO createProject(){
        String projectName = "dummyProject";

        CreateProjectInputDTO projectInputDTO = new CreateProjectInputDTO();
        projectInputDTO.setName(projectName);

        HttpEntity projectEntity = new HttpEntity<>(projectInputDTO, headers);

        ResponseEntity<String> projectResponseEntity =
                restTemplate.postForEntity(createProjectUrl, projectEntity, String.class);
        assertEquals(HttpStatus.CREATED, projectResponseEntity.getStatusCode());

        ProjectResponseDTO responseDTO = new Gson().fromJson(projectResponseEntity.getBody(), ProjectResponseDTO.class);
        return responseDTO;
    }

    private ResponseEntity<String> createTask(String taskSummary,String projectId){
        CreateTaskInputDTO createTaskInputDTO = new CreateTaskInputDTO();
        createTaskInputDTO.setSummary(taskSummary);

        HttpEntity taskEntity = new HttpEntity<>(createTaskInputDTO, headers);

        ResponseEntity<String> taskResponseEntity =
                restTemplate.postForEntity(MessageFormat.format(createTaskUrl, projectId), taskEntity, String.class);
        return taskResponseEntity;
    }

    @Test
    public void when_create_goal_with_valid_data_returns_successful() throws ParseException {
        createGoalAndReturnDTO();
    }


    private GoalResponseDTO createGoalAndReturnDTO() throws ParseException {
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Goal";
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = format.parse("2019-05-28 12:36:50");
        Date stopDate = format.parse("2019-07-01 12:36:50");

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format( createGoalUrl, projectId), entity, String.class);


        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        GoalResponseDTO goalResponseDTO = new Gson().fromJson(responseEntity.getBody(), GoalResponseDTO.class);
        assertEquals(Status.CREATED, goalResponseDTO.getStatus());
        assertNotNull(goalResponseDTO.getGoal());
        assertNotNull(goalResponseDTO.getGoal().getId());
        assertNotNull(goalResponseDTO.getGoal().getCreatedTime());
        assertNotNull(goalResponseDTO.getGoal().getGoalSecret());
        assertEquals(name, goalResponseDTO.getGoal().getGoalSecret().getName());
        assertEquals(description, goalResponseDTO.getGoal().getGoalSecret().getDescription());
        assertEquals(startDate, goalResponseDTO.getGoal().getGoalSecret().getStartDate());
        assertEquals(stopDate, goalResponseDTO.getGoal().getGoalSecret().getStopDate());
        return goalResponseDTO;
    }


    @Test
    public void when_create_goal_with_null_name_return_valid_error() throws ParseException{
        deleteAll();
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = null;
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = format.parse("2019-05-28 12:36:50");
        Date stopDate = format.parse("2019-07-01 12:36:50");

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format( createGoalUrl, projectId), entity, String.class);

        assertEquals(HttpStatus.BAD_REQUEST,  responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, new Gson().fromJson( responseEntity.getBody(), GoalResponseDTO.class).getStatus());
        assertErrorSize(1,  responseEntity.getBody());
        assertError("name", "goal name is required", goalInputDTO.getName(),  responseEntity.getBody(), 0);

    }
    @Test
    public void when_create_goal_with_name_below_length_return_valid_error() throws ParseException{
        deleteAll();
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "";
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = format.parse("2019-05-28 12:36:50");
        Date stopDate = format.parse("2019-07-01 12:36:50");

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format(createGoalUrl, projectId), entity, String.class);

        assertEquals(HttpStatus.BAD_REQUEST,  responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, new Gson().fromJson( responseEntity.getBody(), GoalResponseDTO.class).getStatus());
        assertErrorSize(1,  responseEntity.getBody());
        assertError("name", "name should be between 3 and 50 characters", goalInputDTO.getName(),  responseEntity.getBody(), 0);

    }

    @Test
    public void when_create_goal_with_name_above_length_return_valid_error() throws ParseException{
        deleteAll();
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "ADMNJAKLEOKDNJIUSIBDUIBUBDAJBYUEYEJMSAJKNDJNWENAKSBDDDDDDDDDDDDDDDDDDDDDUW";
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = format.parse("2019-05-28 12:36:50");
        Date stopDate = format.parse("2019-07-01 12:36:50");

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format(createGoalUrl, projectId), entity, String.class);
        assertEquals(HttpStatus.BAD_REQUEST,  responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, new Gson().fromJson( responseEntity.getBody(), GoalResponseDTO.class).getStatus());
        assertErrorSize(1,  responseEntity.getBody());
        assertError("name", "name should be between 3 and 50 characters", goalInputDTO.getName(),  responseEntity.getBody(), 0);

    }

    @Test
    public void when_create_goal_with_duplicate_name_return_valid_error() throws ParseException{
        deleteAll();
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Admin";
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = format.parse("2019-05-28 12:36:50");
        Date stopDate = format.parse("2019-07-01 12:36:50");

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);
        ResponseEntity<String> firstEntity = restTemplate.postForEntity(MessageFormat.format(createGoalUrl, projectId), entity, String.class);
        assertNotNull(firstEntity);
        assertEquals(HttpStatus.CREATED, firstEntity.getStatusCode());
        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format(createGoalUrl, projectId), entity, String.class);
        assertEquals(HttpStatus.BAD_REQUEST,  responseEntity.getStatusCode());
        assertEquals(Status.FAILED_VALIDATION, new Gson().fromJson( responseEntity.getBody(), GoalResponseDTO.class).getStatus());
        assertErrorSize(1,  responseEntity.getBody());
        assertError("name", "The name already exist", goalInputDTO.getName(),  responseEntity.getBody(), 0);

    }

    @Test
    public void when_create_goal_with_past_start_date_return_valid_error() throws ParseException{
        deleteAll();
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "ADAM";
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = format.parse("2019-02-26 12:36:50");
        Date stopDate = format.parse("2019-07-01 12:36:50");

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format(createGoalUrl, projectId), entity, String.class);

        assertEquals(HttpStatus.BAD_REQUEST,  responseEntity.getStatusCode());
        ErrorResponseDTO response = new Gson().fromJson(responseEntity.getBody(), ErrorResponseDTO.class);
        assertEquals(Status.FAILED_VALIDATION,response.getStatus());
        assertNotNull(response.getData());
        assertEquals(1, response.getData().size());
        ApiFieldError error = response.getData().get(0);
        assertEquals("startDate", error.getField());
        assertEquals("Start date should be present or future", error.getMessage());
    }
    @Test
    public void when_create_goal_with_past_stop_date_return_valid_error() throws ParseException{
        deleteAll();
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "ADAM";
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = format.parse("2019-05-28 12:36:50");
        Date stopDate = format.parse("2019-01-01 12:36:50");

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format(createGoalUrl, projectId), entity, String.class);

        assertEquals(HttpStatus.BAD_REQUEST,  responseEntity.getStatusCode());
        ErrorResponseDTO response = new Gson().fromJson(responseEntity.getBody(), ErrorResponseDTO.class);
        assertEquals(Status.FAILED_VALIDATION,response.getStatus());
        assertNotNull(response.getData());
        assertEquals(1, response.getData().size());
        ApiFieldError error = response.getData().get(0);
        assertEquals("stopDate", error.getField());
        assertEquals("Stop date should be future", error.getMessage());
    }

    @Test
    public void when_create_goal_with_start_date_equals_stop_date_return_valid_error() throws ParseException{

        deleteAll();
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "ADAM";
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = format.parse("2019-05-28 12:36:50");
        Date stopDate = format.parse("2019-05-28 12:36:50");

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format(createGoalUrl, projectId), entity, String.class);

        assertEquals(HttpStatus.BAD_REQUEST,  responseEntity.getStatusCode());
        ErrorResponseDTO response = new Gson().fromJson(responseEntity.getBody(), ErrorResponseDTO.class);
        assertEquals(Status.FAILED_VALIDATION,response.getStatus());
        assertNotNull(response.getData());
        assertEquals(1, response.getData().size());
        ApiFieldError error = response.getData().get(0);
        assertEquals("stopDate", error.getField());
        assertEquals("Stop date cannot be equal to start date", error.getMessage());
    }

    @Test
    public void when_create_goal_with_null_start_date_return_valid_error() throws ParseException{
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "ADAM";
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate= null;
        Date stopDate = format.parse("2019-07-01 12:36:50");

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format(createGoalUrl, projectId), entity, String.class);

        assertEquals(HttpStatus.BAD_REQUEST,  responseEntity.getStatusCode());
        ErrorResponseDTO response = new Gson().fromJson(responseEntity.getBody(), ErrorResponseDTO.class);
        assertEquals(Status.FAILED_VALIDATION,response.getStatus());
        assertNotNull(response.getData());
        assertEquals(1, response.getData().size());
        ApiFieldError error = response.getData().get(0);
        assertEquals("startDate", error.getField());
        assertEquals("Start date is required", error.getMessage());
    }

    @Test
    public void when_create_goal_with_null_stop_date_return_valid_error() throws ParseException{
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "ADAM";
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = format.parse("2019-07-01 12:36:50");
        Date stopDate = null;

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format(createGoalUrl, projectId), entity, String.class);

        assertEquals(HttpStatus.BAD_REQUEST,  responseEntity.getStatusCode());
        ErrorResponseDTO response = new Gson().fromJson(responseEntity.getBody(), ErrorResponseDTO.class);
        assertEquals(Status.FAILED_VALIDATION,response.getStatus());
        assertNotNull(response.getData());
        assertEquals(1, response.getData().size());
        ApiFieldError error = response.getData().get(0);
        assertEquals("stopDate", error.getField());
        assertEquals("Stop date is required", error.getMessage());
    }

    @Test
    public void when_attach_task_to_goal_returns_successful() throws ParseException{
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String taskSummary = "Task Summary";
        ResponseEntity<String> taskResponseEntity = createTask(taskSummary,projectId);

        assertEquals(HttpStatus.CREATED, taskResponseEntity.getStatusCode());
        TaskResponseDTO taskResponseDTO = readValue(taskResponseEntity.getBody(), TaskResponseDTO.class);
        assertNotNull(taskResponseDTO.getTask().getId());
        String taskId = taskResponseDTO.getTask().getId();

        String name = "Goal";
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = format.parse("2019-05-28 12:36:50");
        Date stopDate = format.parse("2019-07-01 12:36:50");

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format( createGoalUrl, projectId), entity, String.class);


        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        GoalResponseDTO goalResponseDTO = new Gson().fromJson(responseEntity.getBody(), GoalResponseDTO.class);
        assertEquals(Status.CREATED, goalResponseDTO.getStatus());
        assertNotNull(goalResponseDTO.getGoal());
        assertNotNull(goalResponseDTO.getGoal().getId());
        String goalId = goalResponseDTO.getGoal().getId();

        AttachTaskToGoalInputDTO attachTaskToGoalInputDTO = new AttachTaskToGoalInputDTO();
        attachTaskToGoalInputDTO.setTaskId(taskId);

        HttpEntity newEntity = new HttpEntity<>(attachTaskToGoalInputDTO, headers);

        ResponseEntity<String> newResponseEntity =
                restTemplate.exchange(MessageFormat.format(attachTaskToGoalUrl, projectId) + goalId + "/attach-task",HttpMethod.PUT, newEntity, String.class);


        assertEquals(HttpStatus.OK, newResponseEntity.getStatusCode());
        BasicResponseDTO goalTaskResponseDTO = new Gson().fromJson(newResponseEntity.getBody(), BasicResponseDTO.class);
        assertEquals(Status.SUCCESS, goalTaskResponseDTO.getStatus());

    }

    @Test
    public void when_fetch_goals_returns_ValidData(){
        deleteAll();
        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();

        HttpEntity entity = new HttpEntity<>(headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(MessageFormat.format(fetchGoalsUrl, projectId),HttpMethod.GET, entity, String.class);
        assertEquals(HttpStatus.OK ,responseEntity.getStatusCode());

        GoalListResponseDTO goalListResponseDTO =  new Gson().fromJson(responseEntity.getBody(),GoalListResponseDTO.class);

        assertNotNull( goalListResponseDTO);
        assertEquals(Status.SUCCESS , goalListResponseDTO.getStatus());
    }


    @Test
    public void when_update_goal_with_valid_fields_returns_successful() throws ParseException {

        deleteAll();

        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Goal";
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = format.parse("2019-05-28 12:36:50");
        Date stopDate = format.parse("2019-07-01 12:36:50");

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format( createGoalUrl, projectId), entity, String.class);


        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        GoalResponseDTO goalResponseDTO = new Gson().fromJson(responseEntity.getBody(), GoalResponseDTO.class);
        assertEquals(Status.CREATED, goalResponseDTO.getStatus());
        String goalId = goalResponseDTO.getGoal().getId();
        String newName = "Willy";
        ResponseEntity<String> updateGoalResponseEntity = addName(projectId,goalId,newName);
        assertEquals(HttpStatus.OK , updateGoalResponseEntity.getStatusCode());
        GoalResponseDTO updateGoalResponseDTO = new Gson().fromJson(updateGoalResponseEntity.getBody(), GoalResponseDTO.class);
        assertEquals(newName,updateGoalResponseDTO.getGoal().getGoalSecret().getName());

    }

    private ResponseEntity<String> addName(String projectId,String goalId,String name) {
        UpdateGoalInputDTO updateGoalInputDTO = new UpdateGoalInputDTO();
        updateGoalInputDTO.setName(name);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("name");
        updateGoalInputDTO.setUpdatedFields(updatedFields);

        HttpEntity updateGoalEntity = new HttpEntity<>(updateGoalInputDTO, headers);

        ResponseEntity<String> updateGoalResponseEntity =
                restTemplate.exchange(MessageFormat.format(updateGoalUrl, projectId) + goalId, HttpMethod.PUT, updateGoalEntity ,String.class,goalId);
        return updateGoalResponseEntity;
    }
    @Test
    public void when_update_goal_with_description_returns_successful() throws ParseException {

        deleteAll();

        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Goal";
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = format.parse("2019-05-28 12:36:50");
        Date stopDate = format.parse("2019-07-01 12:36:50");

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format( createGoalUrl, projectId), entity, String.class);


        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        GoalResponseDTO goalResponseDTO = new Gson().fromJson(responseEntity.getBody(), GoalResponseDTO.class);
        assertEquals(Status.CREATED, goalResponseDTO.getStatus());
        String goalId = goalResponseDTO.getGoal().getId();
        String newDescription = "Willy is a funny boy";
        ResponseEntity<String> updateGoalResponseEntity = addDescription(projectId,goalId,newDescription);
        assertEquals(HttpStatus.OK , updateGoalResponseEntity.getStatusCode());
        GoalResponseDTO updateGoalResponseDTO = new Gson().fromJson(updateGoalResponseEntity.getBody(), GoalResponseDTO.class);
        assertEquals(newDescription,updateGoalResponseDTO.getGoal().getGoalSecret().getDescription());

    }

    private ResponseEntity<String> addDescription(String projectId,String goalId,String description) {
        UpdateGoalInputDTO updateGoalInputDTO = new UpdateGoalInputDTO();
        updateGoalInputDTO.setDescription(description);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("description");
        updateGoalInputDTO.setUpdatedFields(updatedFields);

        HttpEntity updateGoalEntity = new HttpEntity<>(updateGoalInputDTO, headers);

        ResponseEntity<String> updateGoalResponseEntity =
                restTemplate.exchange(MessageFormat.format(updateGoalUrl, projectId) + goalId, HttpMethod.PUT, updateGoalEntity ,String.class,goalId);
        return updateGoalResponseEntity;
    }

    @Test
    public void when_update_goal_with_start_date_returns_successful() throws ParseException {

        deleteAll();

        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Goal";
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = format.parse("2019-05-28 12:36:50");
        Date stopDate = format.parse("2019-07-01 12:36:50");

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format( createGoalUrl, projectId), entity, String.class);


        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        GoalResponseDTO goalResponseDTO = new Gson().fromJson(responseEntity.getBody(), GoalResponseDTO.class);
        assertEquals(Status.CREATED, goalResponseDTO.getStatus());
        String goalId = goalResponseDTO.getGoal().getId();
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newStartDate = newFormat.parse("2019-06-28 12:36:50");
        ResponseEntity<String> updateGoalResponseEntity = addStartDate(projectId,goalId,newStartDate);
        assertEquals(HttpStatus.OK , updateGoalResponseEntity.getStatusCode());
        GoalResponseDTO updateGoalResponseDTO = new Gson().fromJson(updateGoalResponseEntity.getBody(), GoalResponseDTO.class);
        //assertEquals(newStartDate,updateGoalResponseDTO.getGoal().getGoalSecret().getStartDate());

    }
    private ResponseEntity<String> addStartDate(String projectId,String goalId,Date startDate) throws ParseException {
        UpdateGoalInputDTO updateGoalInputDTO = new UpdateGoalInputDTO();
        updateGoalInputDTO.setStartDate(startDate);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("description");
        updateGoalInputDTO.setUpdatedFields(updatedFields);

        HttpEntity updateGoalEntity = new HttpEntity<>(updateGoalInputDTO, headers);

        ResponseEntity<String> updateGoalResponseEntity =
                restTemplate.exchange(MessageFormat.format(updateGoalUrl, projectId) + goalId, HttpMethod.PUT, updateGoalEntity ,String.class,goalId);
        return updateGoalResponseEntity;
    }

    @Test
    public void when_update_goal_with_stop_date_returns_successful() throws ParseException {

        deleteAll();

        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Goal";
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = format.parse("2019-05-28 12:36:50");
        Date stopDate = format.parse("2019-07-01 12:36:50");

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format( createGoalUrl, projectId), entity, String.class);


        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        GoalResponseDTO goalResponseDTO = new Gson().fromJson(responseEntity.getBody(), GoalResponseDTO.class);
        assertEquals(Status.CREATED, goalResponseDTO.getStatus());
        String goalId = goalResponseDTO.getGoal().getId();
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newStopDate = newFormat.parse("2019-06-28 12:36:50");
        ResponseEntity<String> updateGoalResponseEntity = addStopDate(projectId,goalId,newStopDate);
        assertEquals(HttpStatus.OK , updateGoalResponseEntity.getStatusCode());
        GoalResponseDTO updateGoalResponseDTO = new Gson().fromJson(updateGoalResponseEntity.getBody(), GoalResponseDTO.class);
       // assertEquals(newStopDate,updateGoalResponseDTO.getGoal().getGoalSecret().getStopDate());

    }

    private ResponseEntity<String> addStopDate(String projectId,String goalId,Date stopDate) throws ParseException {
        UpdateGoalInputDTO updateGoalInputDTO = new UpdateGoalInputDTO();
        updateGoalInputDTO.setStopDate(stopDate);
        List<String> updatedFields = new ArrayList<String>();
        updatedFields.add("description");
        updateGoalInputDTO.setUpdatedFields(updatedFields);

        HttpEntity updateGoalEntity = new HttpEntity<>(updateGoalInputDTO, headers);

        ResponseEntity<String> updateGoalResponseEntity =
                restTemplate.exchange(MessageFormat.format(updateGoalUrl, projectId) + goalId, HttpMethod.PUT, updateGoalEntity ,String.class,goalId);
        return updateGoalResponseEntity;
    }

    @Test
    public void when_fetch_goal_with_valid_id_returns_successful() throws ParseException{
        deleteAll();

        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Goal";
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = format.parse("2019-05-28 12:36:50");
        Date stopDate = format.parse("2019-07-01 12:36:50");

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format( createGoalUrl, projectId), entity, String.class);


        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        GoalResponseDTO goalResponseDTO = new Gson().fromJson(responseEntity.getBody(), GoalResponseDTO.class);
        assertEquals(Status.CREATED, goalResponseDTO.getStatus());
        String goalId = goalResponseDTO.getGoal().getId();
        HttpEntity fetchEntity = new HttpEntity<>(headers);
        ResponseEntity<String> updateResponseEntity =
                restTemplate.exchange(MessageFormat.format(updateGoalUrl,projectId) + goalId,HttpMethod.GET, fetchEntity, String.class,goalId);
        assertEquals(HttpStatus.OK, updateResponseEntity.getStatusCode());
        GoalResponseDTO updateGoalResponseDTO = new Gson().fromJson(updateResponseEntity.getBody(), GoalResponseDTO.class);
        assertEquals(Status.SUCCESS, updateGoalResponseDTO.getStatus());
        assertNotNull(updateGoalResponseDTO.getGoal());
        assertNotNull(updateGoalResponseDTO.getGoal().getCreatedTime());
        assertNotNull(updateGoalResponseDTO.getGoal().getGoalSecret());
    }

    @Test
    public void when_fetch_goal_with_invalid_id_returns_successful() throws ParseException{
        deleteAll();

        ProjectResponseDTO responseDTO = createProject();
        assertNotNull(responseDTO.getProject().getId());
        String projectId = responseDTO.getProject().getId();
        String name = "Goal";
        String description = "Welcome";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date startDate = format.parse("2019-05-28 12:36:50");
        Date stopDate = format.parse("2019-07-01 12:36:50");

        CreateGoalInputDTO goalInputDTO = new CreateGoalInputDTO();
        goalInputDTO.setName(name);
        goalInputDTO.setDescription(description);
        goalInputDTO.setStartDate(startDate);
        goalInputDTO.setStopDate(stopDate);

        HttpEntity entity = new HttpEntity<>(goalInputDTO, headers);

        ResponseEntity<String> responseEntity =
                restTemplate.postForEntity(MessageFormat.format( createGoalUrl, projectId), entity, String.class);


        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        GoalResponseDTO goalResponseDTO = new Gson().fromJson(responseEntity.getBody(), GoalResponseDTO.class);
        assertEquals(Status.CREATED, goalResponseDTO.getStatus());
        String goalId = "invalid goal Id";
        HttpEntity fetchEntity = new HttpEntity<>(headers);
        ResponseEntity<String> updateResponseEntity =
                restTemplate.exchange(MessageFormat.format(updateGoalUrl,projectId) + goalId,HttpMethod.GET, fetchEntity, String.class,goalId);
        assertEquals(HttpStatus.NOT_FOUND, updateResponseEntity.getStatusCode());
        GoalResponseDTO updateGoalResponseDTO = new Gson().fromJson(updateResponseEntity.getBody(), GoalResponseDTO.class);
        assertEquals(Status.NOT_FOUND, updateGoalResponseDTO.getStatus());
    }

    private void deleteAll() {
        goalRepository.deleteAll();

    }
    @After
    public void cleanUp() {
        goalRepository.deleteAll();
        projectRepository.deleteAll();

    }
}
