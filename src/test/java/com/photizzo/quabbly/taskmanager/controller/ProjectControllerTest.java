///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.photizzo.quabbly.taskmanager.controller;
//
//import com.photizzo.quabbly.taskmanager.config.Mk;
//import com.photizzo.quabbly.taskmanager.service.ProjectService;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletResponse;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotNull;
//
//
//@RunWith(SpringRunner.class)
//public class ProjectControllerTest {
//
//    @InjectMocks
//    private Mk projectController = new Mk();
//
//    @Mock
//    private ProjectService projectService;
//
//    @Mock
//    private HttpServletResponse response;
//
//    @Test
//    public void testRequestMappingAnnotationExistsWithRightValue() {
//        RequestMapping requestMappingAnnotation = projectController.getClass().getAnnotation(RequestMapping.class);
//        assertNotNull(requestMappingAnnotation);
//        assertEquals(1, requestMappingAnnotation.value().length);
//        assertEquals("v1/project/{projectId}", requestMappingAnnotation.value()[0]);
//    }
//
//    @Test
//    public void testRestControllerAnnotationExists() {
//        RestController restControllerAnnotation = projectController.getClass().getAnnotation(RestController.class);
//        assertNotNull(restControllerAnnotation);
//    }
//
//
//}
//
