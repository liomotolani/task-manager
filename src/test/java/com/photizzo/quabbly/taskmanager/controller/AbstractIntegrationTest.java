package com.photizzo.quabbly.taskmanager.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.dto.output.BasicResponseDTO;
import org.junit.Assert;
import org.junit.BeforeClass;

import static org.junit.Assert.assertEquals;

public class AbstractIntegrationTest {

    @BeforeClass
    public static void configuration() {
        System.setProperty("spring.data.mongodb.uri", "mongodb://localhost:27017/quabbly_test");
    }

    protected void assertErrorSize(int size, String result) {
        JsonArray errors = new JsonParser().parse(result).getAsJsonObject().getAsJsonArray("data");
        assertEquals(size, errors.size());
    }

    protected void assertError(String field, String message, String rejectedValue, String result) {
        assertErrorSize(1, result);
        assertError(field, message, rejectedValue, result, 0);
    }

    protected void assertError(String field, String message, String rejectedValue, String result, int position) {
        BasicResponseDTO response = new Gson().fromJson(result, BasicResponseDTO.class);
        Assert.assertEquals(Status.FAILED_VALIDATION, response.getStatus());

        JsonArray errors = new JsonParser().parse(result).getAsJsonObject().getAsJsonArray("data");


        assertEquals(field, errors.get(position).getAsJsonObject().get("field").getAsString());
        assertEquals(message, errors.get(position).getAsJsonObject().get("message").getAsString());

        if (rejectedValue != null) {
            assertEquals(rejectedValue, errors.get(position).getAsJsonObject().get("rejectedValue").getAsString());
        }
    }
}
