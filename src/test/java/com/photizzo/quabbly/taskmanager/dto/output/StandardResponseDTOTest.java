package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import org.junit.*;
import static org.junit.Assert.*;

public class StandardResponseDTOTest {
    @Test
    public void testStandardResponseDTOSetter(){
        StandardResponseDTO standardResponseDTO = new StandardResponseDTO();
        standardResponseDTO.setStatus(Status.FAILED_VALIDATION);
        assertEquals(standardResponseDTO.getStatus(),Status.FAILED_VALIDATION);
    }

    @Test
    public void testStandardResponseDTOGetter(){
        StandardResponseDTO standardResponseDTO = new StandardResponseDTO(Status.SUCCESS);
        assertEquals(Status.SUCCESS , standardResponseDTO.getStatus());
    }

}
