package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import org.junit.Test;
import  static org.junit.Assert.* ;

public class BasicResponseDTOTest {
    @Test
    public void testBasicResponseDTOSetters(){
        BasicResponseDTO basicResponseDTO = new BasicResponseDTO();

        basicResponseDTO.setStatus(Status.SUCCESS);
        basicResponseDTO.setData(Object.class);

        assertEquals(basicResponseDTO.getStatus(), Status.SUCCESS);
        assertEquals(basicResponseDTO.getData(),Object.class);
    }
    @Test
    public void testBasicResponseDTOGetters(){
        BasicResponseDTO basicResponseDTO = new BasicResponseDTO(Status.FAILED_VALIDATION, Object.class);
        assertEquals(Object.class,basicResponseDTO.getData());
        assertEquals(Status.FAILED_VALIDATION ,basicResponseDTO.getStatus());
    }
}
