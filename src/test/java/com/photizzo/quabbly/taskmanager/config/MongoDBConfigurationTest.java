//package com.photizzo.quabbly.taskmanager.config;
//
//import com.bol.crypt.CryptVault;
//import com.bol.secure.CachedEncryptionEventListener;
////import com.photizzo.quabbly.hr.config.MongoDBConfiguration;
////import com.photizzo.quabbly.taskmanager.config.MongoDBConfiguration;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Scope;
//import org.springframework.context.annotation.ScopedProxyMode;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.web.context.WebApplicationContext;
//
//import java.lang.reflect.Field;
//import java.lang.reflect.Method;
//import java.util.Base64;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertNotNull;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest(classes = { MongoDBConfiguration.class })
//public class MongoDBConfigurationTest {
//
//    @InjectMocks
//    private MongoDBConfiguration mongoDBConfiguration;
//
//    @Mock
//    private CryptVault cryptVault;
//
//    @Test
//    public void testDatabaseNameProperty() throws NoSuchFieldException {
//        Field databaseNameField = MongoDBConfiguration.class.getDeclaredField("database");
//        assertNotNull(databaseNameField);
//
//        Value annotation = databaseNameField.getAnnotation(Value.class);
//        assertNotNull(annotation);
//
//        assertEquals("${database.name:quabbly}", annotation.value());
//    }
//
////    @Test
////    public void testGetDatabaseName(){
////        mongoDBConfiguration.database = "quabblytest";
////
////        assertEquals("quabblytest", mongoDBConfiguration.getDatabaseName());
////    }
//
//    @Test
//    public void testCryptVaultBean() throws NoSuchMethodException {
//        Method e = MongoDBConfiguration.class.getDeclaredMethod("cryptVault");
//
//        assertEquals(CryptVault.class, e.getReturnType());
//
//        assertNotNull(e);
//
//        Bean beanAnnotation = e.getAnnotation(Bean.class);
//        assertNotNull(beanAnnotation);
//
//        Scope scopeAnnotation = e.getAnnotation(Scope.class);
//        assertNotNull(scopeAnnotation);
//
//        assertEquals(WebApplicationContext.SCOPE_REQUEST, scopeAnnotation.value());
//        assertEquals(ScopedProxyMode.TARGET_CLASS, scopeAnnotation.proxyMode());
//    }
//
//    @Test
//    public void testCachedEncryptionEventListener() throws NoSuchMethodException{
//        Method e = MongoDBConfiguration.class.getDeclaredMethod("encryptionEventListener", CryptVault.class);
//        assertNotNull(e);
//
//        assertEquals(CachedEncryptionEventListener.class , e.getReturnType());
//
//        Bean beanAnnotation = e.getAnnotation(Bean.class);
//        assertNotNull(beanAnnotation);
//
//    }
//
////    @Test
////    public void testCryptVaultBeanIsSetToAES256() throws NoSuchMethodException {
////        mongoDBConfiguration.cryptVault();
////
////        byte[] secretKey = Base64.getDecoder().decode("hqHKBLV83LpCqzKpf8OvutbCs+O5wX5BPu3btWpEvXA=");
////
////        Mockito.verify(cryptVault, Mockito.times(1)).with256BitAesCbcPkcs5PaddingAnd128BitSaltKey(1, secretKey);
////
////    }
//}
