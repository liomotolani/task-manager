package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.Task;

import java.util.List;

public class TaskListResponseDTO extends StandardResponseDTO {
    private List<Task> tasks ;

    public TaskListResponseDTO() {
    }

    public TaskListResponseDTO(Status status) {
        this.status =status;
    }

    public TaskListResponseDTO(Status status, List<Task> tasks) {
        this.status = status;
        this.tasks = tasks;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
