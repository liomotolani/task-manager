package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.extras.Assignee;
import com.photizzo.quabbly.taskmanager.model.extras.Priority;
import com.photizzo.quabbly.taskmanager.validators.interfaces.ValidateEnum;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import java.util.List;


@GroupSequence({
        UpdateTaskInputDTO.First.class,
        UpdateTaskInputDTO.Second.class,
        UpdateTaskInputDTO.Third.class,
        UpdateTaskInputDTO.class
})

public class UpdateTaskInputDTO {
    private String summary;

    private String description;

    private String comment;

    private String attachments;

    private String subTask;

    private boolean watcher;

    private Assignee assignee = new Assignee();

    @ValidateEnum(enumClass = Priority.class,ignoreCase = true,message = "{priority.input.validate}",groups = UpdateTaskInputDTO.First.class )
    private String priority;

    @NotNull(message = "You need to provide the list of fields being updated")
    private List<String> updatedFields;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAttachments() {
        return attachments;
    }

    public void setAttachments(String attachments) {
        this.attachments = attachments;
    }

    public Assignee getAssignee() {
        return assignee;
    }

    public void setAssignee(Assignee assignee) {
        this.assignee = assignee;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getSubTask() {
        return subTask;
    }

    public void setSubTask(String subTask) {
        this.subTask = subTask;
    }

    public boolean isWatcher() {
        return watcher;
    }

    public void setWatcher(boolean watcher) {
        this.watcher = watcher;
    }

    public List<String> getUpdatedFields() {
        return updatedFields;
    }

    public void setUpdatedFields(List<String> updatedFields) {
        this.updatedFields = updatedFields;
    }

    interface First {
    }

    interface Second {
    }

    interface Third {
    }
    
}
