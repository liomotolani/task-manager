package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.Role;
import com.photizzo.quabbly.taskmanager.validators.interfaces.UniqueField;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@GroupSequence({
        UpdateRoleInputDTO.First.class,
        UpdateRoleInputDTO.Second.class,
        UpdateRoleInputDTO.Third.class,
        UpdateRoleInputDTO.class
})
public class UpdateRoleInputDTO {
    @NotNull(message = "{role.name.notNull}", groups = First.class)
    @Size(min = 2, max = 50, message = "{role.name.size}", groups = UpdateRoleInputDTO.Second.class)
    @UniqueField(columnName = "nameHash", message = "{role.name.unique}", className = Role.class, hashValue = true)
    private String name;

    @NotNull(message = "{role.capabilities.notNull}", groups = First.class)
    private List<String> capabilities;

    public UpdateRoleInputDTO() {
    }

    public List<String> getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(List<String> capabilities) {
        this.capabilities = capabilities;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    interface First {
    }

    interface Second {
    }

    interface Third {
    }

}