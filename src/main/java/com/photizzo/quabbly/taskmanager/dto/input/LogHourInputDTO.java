package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.extras.Type;
import com.photizzo.quabbly.taskmanager.validators.interfaces.ValidateEnum;

import javax.validation.GroupSequence;
import javax.validation.constraints.*;
import java.util.Date;

@GroupSequence({
        LogHourInputDTO.First.class,
        LogHourInputDTO.Second.class,
        LogHourInputDTO.Third.class,
        LogHourInputDTO.class
})
public class LogHourInputDTO {

    @NotNull(message = "{logHours.number.notNull}", groups = LogHourInputDTO.First.class)
    private int logNumber;

    @ValidateEnum(enumClass = Type.class,ignoreCase = true,message = "{logHours.type.validString}",groups = LogHourInputDTO.Second.class)
    @NotNull(message = "{logHours.type.notNull}", groups = LogHourInputDTO.First.class)
    private String logType;

    @PastOrPresent(message = "{logHours.logDate.dateError}",groups = LogHourInputDTO.Second.class)
    @NotNull(message = "{logHours.logDate.notNull}", groups = LogHourInputDTO.First.class)
    private Date logDate;


    public LogHourInputDTO(){

    }

    public int getLogNumber() {
        return logNumber;
    }

    public void setLogNumber(int logNumber) {
        this.logNumber = logNumber;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public Date getLogDate() {
        return logDate;
    }

    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

    public interface First {
    }

    public interface Second {
    }

    public interface Third {
    }
}
