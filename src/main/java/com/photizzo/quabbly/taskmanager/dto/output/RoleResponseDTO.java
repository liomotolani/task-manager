package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.Role;


public class RoleResponseDTO extends StandardResponseDTO {

    private Role role;

    public RoleResponseDTO() {

    }

    public RoleResponseDTO(Status status) {
        this.status = status;
    }

    public RoleResponseDTO(Status status, Role role) {
        super(status);
        this.role = role;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
