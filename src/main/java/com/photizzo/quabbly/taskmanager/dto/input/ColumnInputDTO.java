package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.Column;
import com.photizzo.quabbly.taskmanager.validators.interfaces.UniqueField;

import javax.validation.GroupSequence;
import javax.validation.constraints.*;
import java.util.List;


@GroupSequence({
        ColumnInputDTO.First.class,
        ColumnInputDTO.Second.class,
        ColumnInputDTO.Third.class,
        ColumnInputDTO.class
})
public class ColumnInputDTO {

    @NotNull(message = "{column.name.notNull}", groups = First.class)
    @Size(min = 2, message = "{column.name.sizeError}", groups = Second.class)
    @UniqueField(message = "{column.name.unique}", columnName = "name", className = Column.class, groups = Third.class)
    private String name;
    @NotNull(message = "{column.position.notNull}", groups = First.class)
    @Min(0)
    private int position;
    @NotNull(message = "{column.projectId.notNull}", groups = First.class)
    private String projectId;
    public String getName() {return name;}
    public void setName(String name) {this.name = name; }
    public int getPosition() { return position; }
    public void setPosition(int position) { this.position = position; }
    public String getProjectId() { return projectId; }
    public void setProjectId(String projectId) { this.projectId = projectId; }

    interface First {
    }

    interface Second {
    }

    interface Third {
    }

}
