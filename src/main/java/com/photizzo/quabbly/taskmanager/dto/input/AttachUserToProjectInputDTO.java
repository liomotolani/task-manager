package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.ProjectUser;
import com.photizzo.quabbly.taskmanager.model.Role;
import com.photizzo.quabbly.taskmanager.validators.interfaces.UniqueField;
import com.photizzo.quabbly.taskmanager.validators.interfaces.ValidObject;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;


@GroupSequence({
        AttachUserToProjectInputDTO.First.class,
        AttachUserToProjectInputDTO.Second.class,
        AttachUserToProjectInputDTO.class
})
public class AttachUserToProjectInputDTO {

    @NotNull(message = "{projectuser.userId.notNull}", groups = First.class)
    private String userId;

    @NotNull(message = "{projectuser.roleId.notNull}", groups = First.class)
    @ValidObject(className = Role.class, columnName = "id", message = "{projectuser.roleId.isValid}", groups = Second.class)
    private String roleId;

    @NotNull(message = "{projectuser.userFullname.notNull}", groups = First.class)
    private String userFullname;

    @NotNull(message = "{projectuser.userEmail.notNull}", groups = First.class)
    private String userEmail;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getUserFullname() {
        return userFullname;
    }

    public void setUserFullname(String userFullname) {
        this.userFullname = userFullname;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    interface First {
    }

    interface Second {
    }
}
