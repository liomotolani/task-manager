package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.User;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import java.util.List;

@GroupSequence({
        MarkColumnInputDTO.First.class,
        MarkColumnInputDTO.Second.class,
        MarkColumnInputDTO.Third.class,
        MarkColumnInputDTO.class
})
public class MarkColumnInputDTO {


    @NotNull(message = "{column.usersId.notNull}", groups = First.class)
    private List<String> userId;

    public List<String> getUserId() {return userId;}

    public void setUserId(List<String> userId) {this.userId = userId;}

    interface First {
    }

    interface Second {
    }

    interface Third {
    }
}
