package com.photizzo.quabbly.taskmanager.dto.remote.request;


import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.dto.output.StandardResponseDTO;
import com.photizzo.quabbly.taskmanager.model.User;

public class UserResponseDTO extends StandardResponseDTO {

    private User user;

    public UserResponseDTO() {
    }

    public UserResponseDTO(Status status) {
        this(status, null);
    }

    public UserResponseDTO(Status status, User user) {
        super(status);
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
