package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.model.User;


public class LoginResponseDTO {

    private String token;
    private User user;

    public LoginResponseDTO() {
    }

    public LoginResponseDTO(String token, User user) {
        this.token = token;
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
