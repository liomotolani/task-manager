package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.Goal;
import com.photizzo.quabbly.taskmanager.model.Secret.GoalSecret;
import com.photizzo.quabbly.taskmanager.model.extras.GoalTask;
import com.photizzo.quabbly.taskmanager.validators.interfaces.FutureDate;
import com.photizzo.quabbly.taskmanager.validators.interfaces.UniqueField;
import com.photizzo.quabbly.taskmanager.validators.interfaces.ValidateDateRange;

import javax.validation.GroupSequence;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@GroupSequence({
        CreateGoalInputDTO.First.class,
        CreateGoalInputDTO.Second.class,
        CreateGoalInputDTO.Third.class,
        CreateGoalInputDTO.class
})

@ValidateDateRange(dateOneFieldName="startDate", dateTwoFieldName="stopDate",message ="{goal.inValidDate}",
        groups = CreateGoalInputDTO.Third.class)

public class CreateGoalInputDTO {

    @NotNull(message = "{goal.name.notNull}", groups = First.class)
    @Size(min = 3, max = 50,message = "{goal.name.size}", groups = CreateGoalInputDTO.Second.class)
    @UniqueField(columnName = "nameHash", message= "{goal.name.unique}",className = Goal.class, hashValue = true, groups = CreateGoalInputDTO.Third.class)
    private String name;


    @FutureOrPresent(message = "{goal.startDate.dateError}",groups = CreateGoalInputDTO.Second.class)
    @NotNull(message = "{goal.startDate.notNull}", groups = CreateGoalInputDTO.First.class)
    private Date startDate;

    @FutureOrPresent(message = "{goal.stopDate.dateError}",groups = CreateGoalInputDTO.Second.class)
    @NotNull(message = "{goal.stopDate.notNull}", groups = CreateGoalInputDTO.First.class)
    private Date stopDate;

    private String description;

    private List<GoalTask> goalTask;

    public CreateGoalInputDTO(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStopDate() {
        return stopDate;
    }

    public void setStopDate(Date stopDate) {
        this.stopDate = stopDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<GoalTask> getGoalTask() {
        return goalTask;
    }

    public void setGoalTask(List<GoalTask> goalTask) {
        this.goalTask = goalTask;
    }

    interface First {
    }

    interface Second {
    }

    interface Third {
    }
}
