package com.photizzo.quabbly.taskmanager.dto.remote.request;

import java.util.List;

public class SendEmailRequestDTO {
    private List<String>  cc;

    private List<String> bcc;

    private String subject;

    private String htmlContent;

    private  List<String> recipient;

    public List<String> getCc() {
        return cc;
    }

    public void setCc(List<String> cc) {
        this.cc = cc;
    }

    public List<String> getBcc() {
        return bcc;
    }

    public void setBcc(List<String> bcc) {
        this.bcc = bcc;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getHtmlContent() {return htmlContent;}

    public void setHtmlContent(String htmlContent) {this.htmlContent = htmlContent;}

    public List<String> getRecipient() {
        return recipient;
    }

    public void setRecipient(List<String> recipient) {
        this.recipient = recipient;
    }

}
