package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.Column;
import com.photizzo.quabbly.taskmanager.validators.interfaces.UniqueField;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;




@GroupSequence({
        AddColumnInputDTO.First.class,
        AddColumnInputDTO.Second.class,
        AddColumnInputDTO.Third.class,
        AddColumnInputDTO.class
})

public class AddColumnInputDTO {
    @NotNull(message = "{column.name.notNull}", groups = AddColumnInputDTO.First.class)
    @Size(min = 2,max=50 , message = "{column.name.sizeError}", groups = AddColumnInputDTO.Second.class)
   // @UniqueField(columnName = "nameHash", className = Column.class, hashValue = true)
    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    interface First {
    }

    interface Second {
    }

    interface Third {
    }

}
