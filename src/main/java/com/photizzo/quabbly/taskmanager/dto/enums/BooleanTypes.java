package com.photizzo.quabbly.taskmanager.dto.enums;

public enum BooleanTypes {
    TRUE, FALSE
}
