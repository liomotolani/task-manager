package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.Task;
import com.photizzo.quabbly.taskmanager.validators.interfaces.ValidObject;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
@GroupSequence({
        AttachTaskToGoalInputDTO.First.class,
        AttachTaskToGoalInputDTO.Second.class,
        AttachTaskToGoalInputDTO.class
})
public class AttachTaskToGoalInputDTO {

    @ValidObject(className = Task.class,columnName = "id",groups = AttachTaskToGoalInputDTO.Second.class)
    @NotNull(message = "{Goal.taskId.notNull}", groups = AttachTaskToGoalInputDTO.First.class)
    private String taskId;


    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
    interface First {
    }

    interface Second {
    }

}
