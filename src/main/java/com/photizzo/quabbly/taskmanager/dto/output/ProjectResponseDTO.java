package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.Column;
import com.photizzo.quabbly.taskmanager.model.Goal;
import com.photizzo.quabbly.taskmanager.model.Project;
import com.photizzo.quabbly.taskmanager.model.Task;

import java.util.List;

public class ProjectResponseDTO extends StandardResponseDTO {

    private Project project;
    private List<Column> columnList;
    private List<Task>  taskList;
    private List<Goal>  goalList;

    public ProjectResponseDTO() {

    }

    public ProjectResponseDTO(Status status) {
        this.status = status;
    }

    public ProjectResponseDTO(Status status, Project project) {
        super(status);
        this.project = project;
    }

    public ProjectResponseDTO(Status status, Project project, List<Column> columnList) {
        super(status);
        this.project = project;
        this.columnList = columnList;
    }

    public ProjectResponseDTO(Status status, Project project, List<Column> columnList, List<Task> taskList) {
        super(status);
        this.project = project;
        this.columnList = columnList;
        this.taskList = taskList;
    }

    public ProjectResponseDTO(Status status, Project project, List<Column> columnList, List<Task> taskList,List<Goal> goalList) {
        super(status);
        this.project = project;
        this.columnList = columnList;
        this.taskList = taskList;
        this.goalList = goalList;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public List<Column> getColumnList() {
        return columnList;
    }

    public void setColumnList(List<Column> columnList) {
        this.columnList = columnList;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    public List<Goal> getGoalList() {
        return goalList;
    }

    public void setGoalList(List<Goal> goalList) {
        this.goalList = goalList;
    }
}
