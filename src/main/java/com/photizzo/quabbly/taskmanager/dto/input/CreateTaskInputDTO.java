package com.photizzo.quabbly.taskmanager.dto.input;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@GroupSequence({
        CreateTaskInputDTO.First.class,
        CreateTaskInputDTO.Second.class,
        CreateTaskInputDTO.Third.class,
        CreateTaskInputDTO.class
})

public class CreateTaskInputDTO {

    @NotNull(message = "{task.name.notNull}", groups = CreateTaskInputDTO.First.class)
    @Size(min = 2,max = 100 , message = "{task.name.sizeError}", groups = CreateTaskInputDTO.Second.class)
    private String summary;


    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    interface First {
    }

    interface Second {
    }

    interface Third {
    }
}
