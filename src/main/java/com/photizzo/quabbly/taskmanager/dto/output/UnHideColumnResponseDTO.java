package com.photizzo.quabbly.taskmanager.dto.output;
import com.photizzo.quabbly.taskmanager.dto.enums.Status;

public class UnHideColumnResponseDTO extends StandardResponseDTO{

    private Object data;

    public UnHideColumnResponseDTO() {

    }
    public UnHideColumnResponseDTO(Object data) {
        this.data = data;
    }

    public UnHideColumnResponseDTO(Status status) {
        super(status);
    }
    public UnHideColumnResponseDTO(Status status, Object data) {
        super(status);
        this.data = data;
    }
    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }




}
