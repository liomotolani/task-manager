package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.Goal;
import com.photizzo.quabbly.taskmanager.validators.interfaces.UniqueField;
import com.photizzo.quabbly.taskmanager.validators.interfaces.ValidateDateRange;

import javax.validation.GroupSequence;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

@GroupSequence({
        UpdateGoalInputDTO.First.class,
        UpdateGoalInputDTO.Second.class,
        UpdateGoalInputDTO.Third.class,
        UpdateGoalInputDTO.class
})
public class UpdateGoalInputDTO {

    private String name;

    @FutureOrPresent(message = "{goal.startDate.dateError}",groups = UpdateGoalInputDTO.Second.class)
    private Date startDate;

    @FutureOrPresent(message = "{goal.stopDate.dateError}",groups = UpdateGoalInputDTO.Second.class)
    private Date stopDate;

    private String description;

    @NotNull(message = "You need to provide the list of fields being updated")
    private List<String> updatedFields;

    public UpdateGoalInputDTO(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getStopDate() {
        return stopDate;
    }

    public void setStopDate(Date stopDate) {
        this.stopDate = stopDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getUpdatedFields() {
        return updatedFields;
    }

    public void setUpdatedFields(List<String> updatedFields) {
        this.updatedFields = updatedFields;
    }

    interface First {
    }

    interface Second {
    }

    interface Third {
    }
}
