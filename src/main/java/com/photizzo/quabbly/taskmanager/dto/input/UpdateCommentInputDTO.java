package com.photizzo.quabbly.taskmanager.dto.input;

public class UpdateCommentInputDTO {
    private String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
