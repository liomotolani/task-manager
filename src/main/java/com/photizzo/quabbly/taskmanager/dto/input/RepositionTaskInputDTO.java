package com.photizzo.quabbly.taskmanager.dto.input;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import java.util.List;

@GroupSequence({
        RepositionTaskInputDTO.First.class,
        RepositionTaskInputDTO.Second.class,
        RepositionTaskInputDTO.Third.class,
        RepositionTaskInputDTO.class
})

public class RepositionTaskInputDTO {
    @NotNull
    private String taskId;

    private String newPosition;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getNewPosition() {
        return newPosition;
    }

    public void setNewPosition(String newPosition) {
        this.newPosition = newPosition;
    }

    public int getPosition(){
        return Integer.parseInt(getNewPosition());
    }

    interface First {
    }

    interface Second {
    }

    interface Third {
    }
}
