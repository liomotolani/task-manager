package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.Role;

import java.util.List;

public class RoleListResponseDTO extends StandardResponseDTO {

    private List<Role> roleList;

    public RoleListResponseDTO() {
    }

    public RoleListResponseDTO(Status status) { super(status);}

    public RoleListResponseDTO(Status status, List<Role> roleList) {
        super(status);
        this.roleList = roleList;
    }

    public List<Role> getRoleList() { return roleList; }

    public void setRoleList(List<Role> roleList) { this.roleList = roleList;}
}
