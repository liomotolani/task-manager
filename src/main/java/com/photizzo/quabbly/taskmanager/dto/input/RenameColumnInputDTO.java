package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.Column;
import com.photizzo.quabbly.taskmanager.validators.interfaces.UniqueField;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@GroupSequence({
        RenameColumnInputDTO.First.class,
        RenameColumnInputDTO.Second.class,
        RenameColumnInputDTO.Third.class,
        RenameColumnInputDTO.class
})

public class RenameColumnInputDTO {
    @NotNull(message = "{column.name.notNull}", groups = RenameColumnInputDTO.First.class)
    @Size(min = 2,max=50 , message = "{column.name.sizeError}", groups = RenameColumnInputDTO.Second.class)
    private String name ;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    interface First {
    }

    interface Second {
    }

    interface Third {
    }

}
