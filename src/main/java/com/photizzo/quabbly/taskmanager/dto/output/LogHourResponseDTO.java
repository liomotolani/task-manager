package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.extras.LogHours;

public class LogHourResponseDTO extends StandardResponseDTO {

    private LogHours logHours;

    public  LogHourResponseDTO(){

    }

    public  LogHourResponseDTO( LogHours logHours) {
        this.logHours = logHours;
    }
    public  LogHourResponseDTO(Status status){
        super(status);
    }


    public  LogHourResponseDTO(Status status,LogHours logHours) {
        super(status);
        this.logHours = logHours;
    }

    public LogHours getLogHours() {return logHours;}

    public void setLogHours( LogHours logHours) {
        this.logHours = logHours;
    }
}
