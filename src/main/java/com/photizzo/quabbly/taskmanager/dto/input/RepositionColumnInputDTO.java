package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.Column;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@GroupSequence({
        RepositionColumnInputDTO.First.class,
        RepositionColumnInputDTO.Second.class,
        RepositionColumnInputDTO.Third.class,
        RepositionColumnInputDTO.class
})

public class RepositionColumnInputDTO {
    List<String> columnIdList;

    public List<String> getColumnIdList() {
        return columnIdList;
    }

    public void setColumnIdList(List<String> columnIdList) {
        this.columnIdList = columnIdList;
    }

    interface First {
    }

    interface Second {
    }

    interface Third {
    }
}
