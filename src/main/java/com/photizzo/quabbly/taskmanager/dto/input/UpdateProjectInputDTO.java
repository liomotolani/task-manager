package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.Project;
import com.photizzo.quabbly.taskmanager.validators.interfaces.UniqueField;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@GroupSequence({
        UpdateProjectInputDTO.First.class,
        UpdateProjectInputDTO.Second.class,
        UpdateProjectInputDTO.Third.class,
        UpdateProjectInputDTO.class
})


public class UpdateProjectInputDTO {
    private String name;


    @NotNull(message = "You need to provide the list of fields being updated")
    private List<String> updatedFields;

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description;}

    private String description;

    public UpdateProjectInputDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getUpdatedFields() { return updatedFields;}

    public void setUpdatedFields(List<String> updatedFields) {this.updatedFields = updatedFields;}


    interface First {
    }

    interface Second {
    }

    interface Third {
    }
}
