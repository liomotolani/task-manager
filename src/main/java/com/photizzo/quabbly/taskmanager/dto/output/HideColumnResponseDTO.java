package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;

import java.io.Serializable;

public class HideColumnResponseDTO extends StandardResponseDTO {
    private Object data;

    public HideColumnResponseDTO() {

    }
    public HideColumnResponseDTO(Object data) {
        this.data = data;
    }

    public HideColumnResponseDTO(Status status) {
        super(status);
    }
    public HideColumnResponseDTO(Status status, Object data) {
        super(status);
        this.data = data;
    }
    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }


}
