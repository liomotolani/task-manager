package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.extras.Capabilities;

import java.util.List;

public class CapabilitiesListResponseDTO extends StandardResponseDTO {

    private List<Capabilities> capabilities;

    public CapabilitiesListResponseDTO() {
    }

    public CapabilitiesListResponseDTO(Status status) {
        super(status);
    }

    public CapabilitiesListResponseDTO(Status status, List<Capabilities> capabilities) {
        super(status);
        this.capabilities = capabilities;
    }

    public List<Capabilities> getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(List<Capabilities> capabilities) {
        this.capabilities = capabilities;
    }
}
