package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.Role;
import com.photizzo.quabbly.taskmanager.model.extras.Capabilities;
import com.photizzo.quabbly.taskmanager.validators.interfaces.UniqueField;
import com.photizzo.quabbly.taskmanager.validators.interfaces.ValidateEnumList;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Objects;


@GroupSequence({
        CreateRoleInputDTO.First.class,
        CreateRoleInputDTO.Second.class,
        CreateRoleInputDTO.Third.class,
        CreateRoleInputDTO.class
})
public class CreateRoleInputDTO {

    @NotNull(message = "{role.name.notNull}", groups = First.class)
    @Size(min = 2, max = 50, message = "{role.name.size}", groups = CreateRoleInputDTO.Second.class)
    @UniqueField(columnName = "nameHash", message = "{role.name.unique}", className = Role.class, hashValue = true)
    private String name;

    @NotNull(message = "{role.capabilities.notNull}", groups = First.class)
    @ValidateEnumList(message = "{role.capabilities.inValid}",enumClass = Capabilities.class, groups = CreateRoleInputDTO.Second.class)
    private List<String> capabilities;

    public CreateRoleInputDTO() {
    }

    public List<String> getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(List<String> capabilities) {
        this.capabilities = capabilities;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    interface First {
    }

    interface Second {
    }

    interface Third {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateRoleInputDTO that = (CreateRoleInputDTO) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(capabilities, that.capabilities);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, capabilities);
    }
}
