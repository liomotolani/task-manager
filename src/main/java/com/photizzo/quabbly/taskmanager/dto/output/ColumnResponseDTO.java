package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.Column;

public class ColumnResponseDTO extends  StandardResponseDTO {

    private Column column;

    public ColumnResponseDTO(){

    }

    public ColumnResponseDTO(Column column) {
        this.column = column;
    }
    public ColumnResponseDTO(Status status){
        super(status);
    }


    public ColumnResponseDTO(Status status, Column column) {
        super(status);
        this.column = column;
    }

    public Column getColumn() {
        return column;
    }

    public void setColumn(Column column) {
        this.column = column;
    }
}
