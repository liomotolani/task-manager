package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.Project;

import java.util.List;

public class ProjectListResponseDTO extends StandardResponseDTO {

    private List<Project> projectList;

    public ProjectListResponseDTO(){

    }

    public ProjectListResponseDTO(Status status) {
        super(status);
    }

    public ProjectListResponseDTO(Status status, List<Project> projectList) {
        super(status);
        this.projectList = projectList;
    }

    public List<Project> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<Project> projectList) {
        this.projectList = projectList;
    }
}
