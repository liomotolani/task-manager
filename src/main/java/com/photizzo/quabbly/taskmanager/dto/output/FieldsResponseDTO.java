package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.Fields;

public class FieldsResponseDTO extends StandardResponseDTO {

    private Fields fields;

    public FieldsResponseDTO(){

    }

    public FieldsResponseDTO( Fields fields) {
        this.fields = fields;
    }
    public FieldsResponseDTO(Status status){
        super(status);
    }


    public FieldsResponseDTO(Status status, Fields fields) {
        super(status);
        this.fields = fields;
    }

    public Fields getFields() {return fields;}

    public void setFields( Fields fields) {
        this.fields = fields;
    }
}
