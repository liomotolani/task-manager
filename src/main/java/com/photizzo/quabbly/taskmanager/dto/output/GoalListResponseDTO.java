package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.Goal;
import com.photizzo.quabbly.taskmanager.model.extras.Comment;

import java.util.List;

public class GoalListResponseDTO extends StandardResponseDTO {
    List<Goal> goal;

    public GoalListResponseDTO() {
    }

    public GoalListResponseDTO(Status status) {
        super(status);
    }

    public GoalListResponseDTO(Status status, List<Goal> goal) {
        super(status);
        this.goal = goal;
    }

    public List<Goal> getGoal() {
        return goal;
    }

    public void setGoal(List<Goal> goal) {
        this. goal =  goal;
    }
}
