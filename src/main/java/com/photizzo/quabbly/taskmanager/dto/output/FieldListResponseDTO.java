package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.Fields;

import java.util.List;

public class FieldListResponseDTO extends StandardResponseDTO{

    private List<Fields> fieldsList;

    public FieldListResponseDTO() {
    }

    public FieldListResponseDTO(Status status) { super(status);}

    public FieldListResponseDTO(Status status, List<Fields> fieldsList) {
        super(status);
        this.fieldsList = fieldsList;
    }

    public List<Fields> getFieldsList() { return fieldsList; }

    public void setFieldsList(List<Fields> fieldsList) { this.fieldsList = fieldsList;}
}
