package com.photizzo.quabbly.taskmanager.dto.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.dto.input.ApiFieldError;

import java.io.Serializable;
import java.util.List;

public class ErrorResponseDTO implements Serializable {
    @JsonProperty
    private Status status;
    @JsonProperty
    private List<ApiFieldError> data;

    public ErrorResponseDTO() {
    }

    public ErrorResponseDTO(Status status, List<ApiFieldError> data) {
        this.status = status;
        this.data = data;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<ApiFieldError> getData() {
        return data;
    }

    public void setData(List<ApiFieldError> data) {
        this.data = data;
    }
}
