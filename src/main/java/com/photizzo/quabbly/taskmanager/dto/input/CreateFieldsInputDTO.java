package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.Fields;
import com.photizzo.quabbly.taskmanager.model.extras.FieldType;
import com.photizzo.quabbly.taskmanager.validators.interfaces.UniqueField;
import com.photizzo.quabbly.taskmanager.validators.interfaces.ValidateEnum;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@GroupSequence({
        CreateFieldsInputDTO.First.class,
        CreateFieldsInputDTO.Second.class,
        CreateFieldsInputDTO.Third.class,
        CreateFieldsInputDTO.class
})
public class CreateFieldsInputDTO {

    @UniqueField(columnName= "nameHash",message ="{fields.name.unique}",className = Fields.class,hashValue = true,groups=CreateFieldsInputDTO.Third.class)
    @Size(min = 3, max = 30,message = "{fields.name.size}", groups = CreateFieldsInputDTO.Second.class)
    @NotNull(message = "{fields.name.notNull}", groups = First.class)
    private String name;

    @Size(min = 3, max = 50,message = "{fields.description.size}", groups = CreateFieldsInputDTO.First.class)
    private String description;

    @ValidateEnum(enumClass = FieldType.class,ignoreCase = true,message = "{fields.type.validString}",groups = CreateFieldsInputDTO.Second.class)
    @NotNull(message = "{fields.type.notNull}", groups = CreateFieldsInputDTO.First.class)
    private String type;


    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public String getDescription() {return description;}

    public void setDescription(String description) {this.description = description;}

    public String getType() {return type;}

    public void setType(String type) {this.type = type;}

    public interface First {
    }

    public interface Second {
    }

    public interface Third {
    }
}
