package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.extras.Comment;

import java.util.List;

public class CommentListResponseDTO extends StandardResponseDTO {
    private List<Comment> comment;

    public CommentListResponseDTO() {
    }

    public CommentListResponseDTO(Status status) {
        super(status);
    }

    public CommentListResponseDTO(Status status, List<Comment> comment) {
        super(status);
        this.comment = comment;
    }

    public List<Comment> getComment() {
        return comment;
    }

    public void setComment(List<Comment> comment) {
        this.comment = comment;
    }
}
