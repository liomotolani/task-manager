package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.Project;
import com.photizzo.quabbly.taskmanager.validators.interfaces.UniqueField;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import java.util.Date;
import javax.validation.constraints.Size;


@GroupSequence({
        CreateProjectInputDTO.First.class,
        CreateProjectInputDTO.Second.class,
        CreateProjectInputDTO.Third.class,
        CreateProjectInputDTO.class
})
public class CreateProjectInputDTO {

    @NotNull(message = "{project.name.notNull}", groups = First.class)
    @Size(min = 3, max = 50, message = "{project.name.size.error}", groups = CreateProjectInputDTO.Second.class)
    @UniqueField(columnName = "nameHash", className = Project.class, hashValue = true)
    private String name;

    @Size(min=3, max = 50, message = "{project.description.size.error}",groups = CreateProjectInputDTO.class)
    private String description;


    public CreateProjectInputDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    interface First {
    }

    interface Second {
    }

    interface Third {
    }

}
