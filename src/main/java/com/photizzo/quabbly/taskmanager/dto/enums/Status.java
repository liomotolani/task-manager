package com.photizzo.quabbly.taskmanager.dto.enums;

public enum Status {
    SUCCESS, INTERNAL_ERROR, FAILED_VALIDATION, NOT_FOUND, INVALID_SERVICE_ERROR, CONFLICT, CREATED,BAD_REQUEST, NO_CONTENT;
}
