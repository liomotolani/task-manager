package com.photizzo.quabbly.taskmanager.dto.input;

public class UpdateSubTaskInputDTO {
    private String summary;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }
}
