package com.photizzo.quabbly.taskmanager.dto.input;

import javax.validation.constraints.NotNull;

public class MoveTaskInputDTO {
    @NotNull(message = "{task.columnId.notNull}", groups = RenameColumnInputDTO.First.class)
    private String columnId;

    public String getColumnId() {
        return columnId;
    }

    public void setColumnId(String columnId) {
        this.columnId = columnId;
    }
}
