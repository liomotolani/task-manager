package com.photizzo.quabbly.taskmanager.dto.output;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.photizzo.quabbly.taskmanager.dto.enums.Status;


public class BasicResponseDTO extends StandardResponseDTO {

    @JsonProperty
    private Object data;



    public BasicResponseDTO() {
    }

    public BasicResponseDTO(Status status) {
        super(status);
    }

    public BasicResponseDTO(Status status, Object data) {
        super(status);
        this.data = data;
    }


    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}

