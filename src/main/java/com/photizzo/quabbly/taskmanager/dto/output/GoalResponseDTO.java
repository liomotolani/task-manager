package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.Goal;
import com.photizzo.quabbly.taskmanager.model.Task;

import java.util.List;

public class GoalResponseDTO extends StandardResponseDTO {

    private Goal goal;

    private List<Task> taskList;

    public GoalResponseDTO(){

    }

    public GoalResponseDTO(Goal goal) {
        this.goal = goal;
    }
    public GoalResponseDTO(Status status){
        super(status);
    }


    public GoalResponseDTO(Status status, Goal goal) {
        super(status);
        this.goal = goal;
    }

    public GoalResponseDTO(Status status, Goal goal, List<Task> taskList) {
        super(status);
        this.goal = goal;
        this.taskList = taskList;
    }

    public Goal getGoal() {return goal;}

    public void setGoal(Goal goal) {
        this.goal = goal;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }
}
