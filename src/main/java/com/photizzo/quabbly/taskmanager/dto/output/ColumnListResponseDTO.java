package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.Column;

import java.util.List;

public class ColumnListResponseDTO extends StandardResponseDTO {
    List<Column> columnList;

    public ColumnListResponseDTO() {
    }

    public ColumnListResponseDTO(Status status) {
        super(status);
    }

    public ColumnListResponseDTO(Status status, List<Column> columnList) {
        super(status);
        this.columnList = columnList;
    }

    public List<Column> getColumnList() {
        return columnList;
    }

    public void setColumnList(List<Column> columnList) {
        this.columnList = columnList;
    }
}
