package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.extras.Attachments;

import java.util.List;

public class AttachmentListResponseDTO extends StandardResponseDTO {
    List<Attachments> attachmentsList;



    public AttachmentListResponseDTO(Status status) {
        super(status);
    }

    public AttachmentListResponseDTO(Status status, List<Attachments> attachmentsList) {
        super(status);
        this.attachmentsList = attachmentsList;
    }

    public List<Attachments> getAttachmentsList() {
        return attachmentsList;
    }

    public void setAttachmentsList(List<Attachments> attachmentsList) {
        this.attachmentsList = attachmentsList;
    }
}
