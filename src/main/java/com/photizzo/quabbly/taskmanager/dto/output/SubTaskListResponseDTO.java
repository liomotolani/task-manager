package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.extras.SubTask;

import java.util.List;

public class SubTaskListResponseDTO extends StandardResponseDTO {
    List<SubTask> subTaskList;

    public SubTaskListResponseDTO() {
    }

    public SubTaskListResponseDTO(Status status) {
        super(status);
    }


    public SubTaskListResponseDTO(Status status, List<SubTask> subTaskList) {
        super(status);
        this.subTaskList = subTaskList;
    }

    public List<SubTask> getSubTaskList() {
        return subTaskList;
    }

    public void setSubTaskList(List<SubTask> subTaskList) {
        this.subTaskList = subTaskList;
    }
}
