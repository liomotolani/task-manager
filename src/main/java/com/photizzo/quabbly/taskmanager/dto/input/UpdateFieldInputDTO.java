package com.photizzo.quabbly.taskmanager.dto.input;

import com.photizzo.quabbly.taskmanager.model.extras.FieldType;
import com.photizzo.quabbly.taskmanager.validators.interfaces.ValidateEnum;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import java.util.List;
@GroupSequence({
        UpdateFieldInputDTO.First.class,
        UpdateFieldInputDTO.Second.class,
        UpdateFieldInputDTO.Third.class,
        UpdateFieldInputDTO.class
})
public class UpdateFieldInputDTO {
    
    private String name;
    
    private String description;

    @ValidateEnum(enumClass = FieldType.class,ignoreCase = true,message = "{fields.type.validString}",groups = UpdateFieldInputDTO.First.class)
    private String type;
    
    private String value;

    @NotNull(message = "You need to provide the list of fields being updated")
    private List<String> updatedFields;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<String> getUpdatedFields() {
        return updatedFields;
    }

    public void setUpdatedFields(List<String> updatedFields) {
        this.updatedFields = updatedFields;
    }

    public interface First {
    }

    public interface Second {
    }

    public interface Third {
    }
}
