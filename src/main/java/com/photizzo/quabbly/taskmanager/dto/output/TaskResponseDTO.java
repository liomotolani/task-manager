package com.photizzo.quabbly.taskmanager.dto.output;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.model.Column;
import com.photizzo.quabbly.taskmanager.model.Task;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class TaskResponseDTO extends StandardResponseDTO {
    private Task task;


    private List<Column> columnList;

    private ResponseEntity responseEntity;

    public TaskResponseDTO() {
    }

    public TaskResponseDTO(Status status, ResponseEntity responseEntity) {
        super(status);
        this.responseEntity = responseEntity;
    }

    public TaskResponseDTO(Status status){
        this.status = status;
    }

    public TaskResponseDTO(Task task) {
        this.task = task;
    }

    public TaskResponseDTO(Status status, Task task) {
        super(status);
        this.task = task;
    }

    public TaskResponseDTO(Status status, Task task,List<Column> columnList) {
        super(status);
        this.task = task;
        this.columnList =columnList;
    }



    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public List<Column> getColumnList() {
        return columnList;
    }

    public void setColumnList(List<Column> columnList) {
        this.columnList = columnList;
    }


}
