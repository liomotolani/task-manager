/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.photizzo.quabbly.taskmanager.service;

import com.photizzo.quabbly.taskmanager.dto.input.AttachUserToProjectInputDTO;
import com.photizzo.quabbly.taskmanager.dto.input.CreateProjectInputDTO;
import com.photizzo.quabbly.taskmanager.dto.input.CreateRoleInputDTO;
import com.photizzo.quabbly.taskmanager.dto.input.UpdateRoleInputDTO;
import com.photizzo.quabbly.taskmanager.dto.output.*;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * @author osita
 */
@Service
public interface AdminService {

    RoleResponseDTO createRole(CreateRoleInputDTO role);

    BasicResponseDTO attachUserToProject(AttachUserToProjectInputDTO dto, String projectId);

    BasicResponseDTO fetchProjectUsers(String projectId);



    ProjectResponseDTO createProject(CreateProjectInputDTO project);

    ProjectListResponseDTO fetchProjects(Pageable pageable);

    CapabilitiesListResponseDTO fetchCapabilities();

    RoleListResponseDTO fetchRoles(Pageable pageable);

    RoleResponseDTO updateRole(UpdateRoleInputDTO dto, String roleId);

}
