package com.photizzo.quabbly.taskmanager.service;

import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.dto.input.AttachUserToProjectInputDTO;
import com.photizzo.quabbly.taskmanager.dto.input.CreateProjectInputDTO;
import com.photizzo.quabbly.taskmanager.dto.input.CreateRoleInputDTO;
import com.photizzo.quabbly.taskmanager.dto.input.UpdateRoleInputDTO;
import com.photizzo.quabbly.taskmanager.dto.output.*;
import com.photizzo.quabbly.taskmanager.model.*;
import com.photizzo.quabbly.taskmanager.model.Secret.ProjectSecret;
import com.photizzo.quabbly.taskmanager.model.Secret.RoleSecret;
import com.photizzo.quabbly.taskmanager.model.extras.Capabilities;
import com.photizzo.quabbly.taskmanager.model.projectLogData.CreateProjectData;
import com.photizzo.quabbly.taskmanager.repositories.ColumnRepository;
import com.photizzo.quabbly.taskmanager.repositories.GoalRepository;
import com.photizzo.quabbly.taskmanager.repositories.ProjectUserRepository;
import com.photizzo.quabbly.taskmanager.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.util.*;


@Service("adminService")
public class AdminServiceImpl extends AbstractService implements AdminService {

    private static final String SERVICE_NAME = "AdminService";

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ProjectUserRepository projectUserRepository;

    @Autowired
    private ColumnRepository columnRepository;

    @Autowired
    private GoalRepository goalRepository;


    @Autowired
    private Environment env;

    @Override
    public ProjectResponseDTO createProject(CreateProjectInputDTO project) {
        try {
            Project projectObj = saveProject(project);
            if (saveError(projectObj)) {
                return new ProjectResponseDTO(Status.INVALID_SERVICE_ERROR);
            }

            attachDefaultColumnsToProject(projectObj.getId());

            List<Column> columns = fetchCreatedColumns(projectObj.getId());

            return new ProjectResponseDTO(Status.CREATED, projectObj, columns);

        } catch (Exception e) {
            logError(SERVICE_NAME, "createProject", e);
            return new ProjectResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public ProjectListResponseDTO fetchProjects(Pageable pageable) {
        try {
            List<Project> projects = fetchProjectList(getTenantId(), pageable);
            return new ProjectListResponseDTO(Status.SUCCESS, projects);
        } catch (Exception e) {
            logError(SERVICE_NAME, "ProjectList", e);
            return new ProjectListResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public CapabilitiesListResponseDTO fetchCapabilities() {
        try {
            List<Capabilities> capabilities = new ArrayList<>(EnumSet.allOf(Capabilities.class));
            return new CapabilitiesListResponseDTO(Status.SUCCESS, capabilities);
        } catch (Exception e) {
            logError(SERVICE_NAME, "ProjectList", e);
            return new CapabilitiesListResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public RoleResponseDTO createRole(CreateRoleInputDTO role) {
        try {
            Role roleObj = saveRole(role);
            if (saveError(roleObj)) {
                return new RoleResponseDTO(Status.INTERNAL_ERROR);
            }

            return new RoleResponseDTO(Status.CREATED, roleObj);

        } catch (Exception e) {
            logError(SERVICE_NAME, "createRole", e);
            return new RoleResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public RoleListResponseDTO fetchRoles(Pageable pageable) {
        try {
            List<Role> roles = fetchRoleList(getTenantId(), pageable);
            return new RoleListResponseDTO(Status.SUCCESS, roles);
        } catch (Exception e) {
            logError(SERVICE_NAME, "RoleList", e);
            return new RoleListResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public RoleResponseDTO updateRole(UpdateRoleInputDTO dto, String roleId) {
        try {
            Role role = getRole(getTenantId(), roleId);
            if (role == null) {
                return new RoleResponseDTO(Status.NOT_FOUND);
            }
            updateRole(dto, role);

            return new RoleResponseDTO(Status.SUCCESS, role);
        } catch (Exception e) {
            logError(SERVICE_NAME, "updateGoal", e);
            return new RoleResponseDTO(Status.INTERNAL_ERROR);
        }
    }


    @Override
    public BasicResponseDTO attachUserToProject(AttachUserToProjectInputDTO dto, String projectId) {
        Project project = getProject(getTenantId(), projectId);
        if (projectNotFound(project)) {
            return new BasicResponseDTO(Status.NOT_FOUND);
        }

        ProjectUser existingProjectUser = getProjectUserIds(projectId, dto.getUserId());

        if(existingProjectUser != null){
            return new BasicResponseDTO(Status.FAILED_VALIDATION,getValidationError("userId","The user has been attached to this project",dto.getUserFullname()));
        }

        User user = getUserById(dto.getUserId());
        if(Objects.isNull(user)){
            return new BasicResponseDTO(Status.FAILED_VALIDATION,getValidationError("userId","The user does not exist",dto.getUserFullname()));
        }


        ProjectUser projectUser = saveProjectUser(dto, projectId);

        if (saveError(projectUser)) {
            return new BasicResponseDTO(Status.INTERNAL_ERROR);
        }

        return new BasicResponseDTO(Status.SUCCESS,projectUser);
    }



    @Override
    public BasicResponseDTO fetchProjectUsers(String projectId) {
        try {
            Project project = getProject(getTenantId(), projectId);
            if (projectNotFound(project)) {
                return new BasicResponseDTO(Status.NOT_FOUND);
            }

            List<ProjectUser> projectUserList = getProjectUsers(getTenantId(), projectId);
            return new BasicResponseDTO(Status.SUCCESS, projectUserList);

        } catch (Exception e) {
            logError(SERVICE_NAME, "listProjectUsers", e);
            return new BasicResponseDTO(Status.INTERNAL_ERROR);
        }
    }


    private List<ProjectUser> getProjectUsers(String tenantId, String projectId) {
        return projectUserRepository.findByTenantIdAndProjectId(tenantId, projectId);

    }
        private List<ProjectUser> getProjectUsers (String tenantId, Pageable pageable) {
            return projectUserRepository.findByTenantId(tenantId, pageable);
        }

        private Role getRole (String tenantId, String roleId){
            return roleRepository.findByTenantIdAndId(tenantId, roleId);
        }

        private Goal getGoal (String tenantId, String goalId){
            return goalRepository.findByTenantIdAndId(tenantId, goalId);
        }
        private List<Project> fetchProjectList (String tenantId, Pageable pageable){
            return projectRepository.findByTenantId(tenantId, pageable);
        }

        private List<Role> fetchRoleList (String tenantId, Pageable pageable){
            return roleRepository.findByTenantId(tenantId, pageable);
        }

        private List<Goal> fetchGoalList (String tenantId, Pageable pageable){
            return goalRepository.findByTenantId(tenantId, pageable);
        }
        private void uniqueProjectKey(Project projectObj, CreateProjectInputDTO dto, Pageable pageable){
            String name = dto.getName();
            String uniqueKey = splitProjectNameForUniqueKey(name);
            if (uniqueKey != "") {
                List<Project> project = projectRepository.findByTenantIdAndUniqueKeyStartingWith(getTenantId(), uniqueKey, pageable);
                if (project != null && project.size() > 0) {
                    int count = project.size();
                    uniqueKey = uniqueKey + count;
                }
            }
            projectObj.setUniqueKey(uniqueKey);
            projectRepository.save(projectObj);
        }


        private String splitProjectNameForUniqueKey(String name){
            String[] newName = name.split(" ");
            String uniqueKey = "";
            for (int i = 0; i < newName.length; i++) {
                if (newName.length > 1) {
                    String s = newName[i];
                    uniqueKey += Character.toUpperCase(s.charAt(0));
                } else if (name.length() > 3) {
                    uniqueKey += name.substring(0, 3).toUpperCase();
                }
            }
            return uniqueKey;
        }


    private Project saveProject(CreateProjectInputDTO dto) throws NoSuchAlgorithmException {
        Project projectObj = Project.createProjectWithDefaults(dto.getName(),dto.getDescription(), hash(dto.getName()), getUserId(), getTenantId(), getUserFullName());
        uniqueProjectKey(projectObj,dto,Pageable.unpaged());
        String action = "createProject";
        CreateProjectData createProjectData = new CreateProjectData(getUserFullName(),new Date(),dto.getName(),action);
        ProjectSecret projectSecret = projectObj.getProjectSecret();
        List<Object> projectLog = getExisting(projectSecret.getProjectLog());
        projectLog.add(createProjectData);
        projectSecret.setProjectLog(projectLog);
        return projectRepository.save(projectObj);
    }
    private <T> List<T> getExisting(List<T> t) {
        return t == null ? new ArrayList() : t;
    }


        private void attachDefaultColumnsToProject (String projectId) throws NoSuchAlgorithmException {
            String[] columnDefaults = env.getProperty("default.project.columns", String[].class);
            int count = 0;
            for (String columnName : columnDefaults) {
                saveColumn(columnName, count++, projectId);
            }
        }

        private boolean projectUserNotFound (ProjectUser projectUser){
            return projectUser == null;
        }


        private void saveColumn (String name,int count, String projectId) throws NoSuchAlgorithmException {
            Column columnObj = Column.createColumnWithDefaults(name, hash(name), count, projectId, getUserId(), getTenantId());
            columnRepository.save(columnObj);
        }

        private List<Column> fetchCreatedColumns (String projectId){
            return columnRepository.findAllByProjectIdOrderByPositionAsc(projectId);
        }

    private ProjectUser getProjectUserIds(String projectId, String userId){
        return projectUserRepository.findByTenantIdAndProjectIdAndUserId(getTenantId(), projectId, userId);
    }

    private ProjectUser saveProjectUser(AttachUserToProjectInputDTO dto, String projectId) {
        Role role = roleRepository.findByTenantIdAndId(getTenantId(),dto.getRoleId());
        ProjectUser projectUser = ProjectUser.createProjectUserWithDefaults(dto.getUserFullname(), dto.getUserEmail(), projectId, dto.getUserId(), dto.getRoleId(), getUserId(), getTenantId(),role);
        projectUserRepository.save(projectUser);
        return projectUser;
    }

        private Role saveRole (CreateRoleInputDTO dto) throws NoSuchAlgorithmException {
            Role roleObj = Role.createRoleWithDefaults(dto.getName(), dto.getCapabilities(), hash(dto.getName()), getUserId(), getTenantId());
            return roleRepository.save(roleObj);
        }

        private Role updateRole (UpdateRoleInputDTO dto, Role role){
            RoleSecret roleSecret = role.getRoleSecret();
            roleSecret.updateFieldsByDefault(dto.getName(), dto.getCapabilities());
            role.setLastUpdatedTime(new Date());

            return roleRepository.save(role);
        }

    }

