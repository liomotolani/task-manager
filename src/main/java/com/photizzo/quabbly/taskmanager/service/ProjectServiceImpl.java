package com.photizzo.quabbly.taskmanager.service;

import com.auth0.jwt.interfaces.Claim;
import com.photizzo.quabbly.taskmanager.config.TokenProvider;
import com.photizzo.quabbly.taskmanager.dto.enums.Status;
import com.photizzo.quabbly.taskmanager.dto.input.*;
import com.photizzo.quabbly.taskmanager.dto.output.*;
import com.photizzo.quabbly.taskmanager.model.*;
import com.photizzo.quabbly.taskmanager.model.Secret.*;
import com.photizzo.quabbly.taskmanager.model.extras.*;
import com.photizzo.quabbly.taskmanager.model.projectLogData.*;
import com.photizzo.quabbly.taskmanager.repositories.*;
import com.photizzo.quabbly.taskmanager.util.UpdateType;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Service("projectService")
public class ProjectServiceImpl extends AbstractService implements ProjectService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private SubTaskRepository subTaskRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FieldsRepository fieldsRepository;

    @Autowired
    private LogHourRepository logHourRepository;

    @Autowired
    private GoalRepository goalRepository;

    @Autowired
    private ProjectUserRepository projectUserRepository;



    @Autowired
    private AttachmentRepository attachmentRepository;

    private static final String SERVICE_NAME = "ProjectService";

    @Autowired
    private Environment env;

    private RestTemplate restTemplate = new RestTemplate();

    private HttpHeaders headers = new HttpHeaders();

    @Autowired
    private ColumnRepository columnRepository;

    @Autowired
    private TokenProvider tokenProvider;




    @Override
    public ProjectResponseDTO updateProject(String projectId, UpdateProjectInputDTO dto) {
        try{
            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new ProjectResponseDTO(Status.NOT_FOUND);
            }
            updateProjectParameters(project,dto);

            projectRepository.save(project);
            return new ProjectResponseDTO(Status.SUCCESS,project);
        } catch (Exception e){
            logError(SERVICE_NAME, "updateProject", e);
            return new ProjectResponseDTO(Status.INTERNAL_ERROR);
        }

    }

    private void updateProjectParameters(Project project,UpdateProjectInputDTO dto) {
        ProjectSecret projectSecret = project.getProjectSecret();
        String projectName = projectSecret.getName();
        if (isUpdating("name", dto.getName(), dto)) {
            projectSecret.setName(dto.getName());
            String action = "updateProjectName";
            UpdateProjectData updateProjectData = new UpdateProjectData(getUserFullName(), new Date(), projectName, dto.getName(), action);
            updateProjectLog(project, updateProjectData);
        }
        if (isUpdating("description", dto.getDescription(), dto)) {
            projectSecret.setDescription(dto.getDescription());
            String action = "updateProjectDescription";
            UpdateProjectData updateProjectData = new UpdateProjectData(getUserFullName(), new Date(), projectName, dto.getDescription(), action);
            updateProjectLog(project, updateProjectData);
        }
    }



    private void updateProjectLog(Project project, Object logMessage) {
        ProjectSecret projectSecret = project.getProjectSecret();
        List<Object> projectLog = getExisting(projectSecret.getProjectLog());
        projectLog.add(logMessage);
        projectSecret.setProjectLog(projectLog);
        project.setProjectSecret(projectSecret);
        projectRepository.save(project);
    }

    private void taskUniqueFieldKey(Task task,String projectId){
      String projectUniqueKey = getProjectUniqueKey(projectId);
      int taskNumber = getTaskNextNumber(projectId);
      String newTaskUniqueKey = projectUniqueKey+"-"+taskNumber;
      task.setTaskUniqueKey(newTaskUniqueKey);
      taskRepository.save(task);

    }

    private String getProjectUniqueKey(String projectId){
        Project project = getProject(getTenantId(),projectId);
        String projectUniqueKey = project.getUniqueKey();
        return projectUniqueKey;
    }

    private int getTaskNextNumber(String projectId){
        List<Task> taskList = countCreatedTasks(projectId);
        int taskNumber = 0;
        for(int n =1; n <= taskList.size();n++){
            taskNumber = n;
        }
        return taskNumber;
    }

    private  void updateTaskLog(Task task,Object logMessage){
        TaskSecret taskSecret = task.getTaskSecret();
        List<Object> taskLog = getExisting(taskSecret.getTaskLog());
        taskLog.add(logMessage);
        taskSecret.setTaskLog(taskLog);
        task.setTaskSecret(taskSecret);
        taskRepository.save(task);
    }


    @Override
    public ColumnResponseDTO addColumn(AddColumnInputDTO dto, String projectId) {
        try{
            Project project = getProject(getTenantId(),projectId);
            ProjectSecret projectSecret = project.getProjectSecret();
            String projectName = projectSecret.getName();
            if (projectNotFound(project)) {
                return new ColumnResponseDTO(Status.NOT_FOUND);
            }
            int nextColumnPosition = getNextColumnPosition(projectId);
            Column column = Column.createColumnWithDefaults(dto.getName() ,hash(dto.getName()), nextColumnPosition, projectId, getUserId(), getTenantId());

            List<Column> columnList = fetchColumnList(getTenantId(),projectId);
            for (Column col :columnList){
                String existingNames = col.getColumnSecret().getName().toLowerCase();
                String newName = dto.getName().toLowerCase();
                if (existingNames.equals(newName)){
                    return new ColumnResponseDTO(Status.FAILED_VALIDATION);
                }
            }
            String action = "addColumnToProject";
            AddColumnToProjectData addColumnToProjectData = new AddColumnToProjectData(getUserFullName(),new Date(),action,dto.getName(),projectName);
            updateProjectLog(project,addColumnToProjectData);
            columnRepository.save(column);
            projectRepository.save(project);
            String ColumnId = column.getId();
            if(columnIdNotFound(ColumnId)){
                return new ColumnResponseDTO(Status.INTERNAL_ERROR);
            }
            return new ColumnResponseDTO(Status.CREATED, column);
        } catch (Exception e){
            logError(SERVICE_NAME, "addColumnToProject", e);
            return new ColumnResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public ProjectResponseDTO fetchProject(String projectId) {
        try{
            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new ProjectResponseDTO(Status.NOT_FOUND);
            }
            List<Column> columns= fetchCreatedColumns(project.getId());
            List<Task> tasks = fetchCreatedTasks(project.getId());
            List<Goal> goals = fetchCreatedGoals(project.getId());
            return new ProjectResponseDTO(Status.SUCCESS,project,columns,tasks,goals);
        }catch (Exception e){
            logError(SERVICE_NAME, "fetchProject", e);
            return new ProjectResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public ColumnResponseDTO renameColumn(RenameColumnInputDTO dto, String columnId,String projectId) {
        try {
            Column column =  getColumn(getTenantId(),columnId);
            if (columnNotFound(column)){
                return new ColumnResponseDTO(Status.NOT_FOUND);
            }

            List<Column> columnList = fetchColumnList(getTenantId(),projectId);
            for (Column col : columnList){
                String existingNames = col.getColumnSecret().getName().toLowerCase();
                String newName = dto.getName().toLowerCase();
                if (existingNames.equals(newName) ){
                    return new ColumnResponseDTO(Status.FAILED_VALIDATION);
                }
            }
            renameAndSaveColumn(column, dto, projectId);


            return new ColumnResponseDTO(Status.SUCCESS ,column);
        }
        catch (Exception e) {
            logError(SERVICE_NAME, "renameProject", e);
            return new ColumnResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public BasicResponseDTO attachTaskToGoal(AttachTaskToGoalInputDTO dto,String goalId){
        try{
            Goal goalObj = getGoal(getTenantId(),goalId);
            if(goalNotFound(goalObj)){
                return new BasicResponseDTO(Status.NOT_FOUND);
            }
            List<Goal> taskIdExist = goalRepository.findByTenantIdAndProjectIdAndTaskId(getTenantId(),goalObj.getProjectId(),dto.getTaskId());
            if(taskIdExist.size()>0){
                return new BasicResponseDTO(Status.FAILED_VALIDATION,getValidationError("taskId","The task has been attached to this goal",dto.getTaskId()));
            }


            Task task = getTask(getTenantId(), dto.getTaskId());
            task.setGoalId(new ObjectId(goalId));

            taskRepository.save(task);
            return new BasicResponseDTO(Status.SUCCESS);

        }catch (Exception e){
            logError(SERVICE_NAME, "attachTaskToGoal", e);
            return new BasicResponseDTO(Status.INTERNAL_ERROR);
        }
    }


    @Override
    public HideColumnResponseDTO markColumnAsHidden(MarkColumnInputDTO dto,String columnId,String projectId) {
        try {
            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new HideColumnResponseDTO(Status.NOT_FOUND);
            }
            Column columnObj = getColumn(getTenantId(), columnId);
            if (columnNotFound(columnObj)) {
                return new HideColumnResponseDTO(Status.NOT_FOUND);
            }
            if (!columnObj.isVisibility()) {
                return new HideColumnResponseDTO(Status.FAILED_VALIDATION);
            }
            List<String> userList = dto.getUserId();
            if( userList.size()== 0){
                return new HideColumnResponseDTO(Status.FAILED_VALIDATION);
            }else if(userList.size()!= 0) {
                for (String userId : userList) {
                    List<String> usercolumnhide = columnObj.getColumnSecret().getUsersId();
                    if (usercolumnhide == null) {
                        columnObj.getColumnSecret().setUsersId(userList);
                    } else {
                        usercolumnhide.add(userId);
                    }
                }
            }
            columnObj.setLastUpdatedTime(new Date());
            columnObj.setVisibility(false);

            columnRepository.save(columnObj);

            return new HideColumnResponseDTO(Status.SUCCESS,columnObj);
        } catch (Exception e) {
            logError(SERVICE_NAME, "markColumnAsHidden", e);
            return new HideColumnResponseDTO(Status.INTERNAL_ERROR);
        }

    }


    @Override
    public UnHideColumnResponseDTO markColumnAsVisible(MarkColumnInputDTO dto,String columnId,String projectId) {
        try {
            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new UnHideColumnResponseDTO(Status.NOT_FOUND);
            }
            Column columnObj = getColumn(getTenantId(), columnId);
            if (columnNotFound(columnObj)) {
                return new UnHideColumnResponseDTO(Status.NOT_FOUND);
            }
            if (columnObj.isVisibility()) {
                return new UnHideColumnResponseDTO(Status.FAILED_VALIDATION);
            }
            List<String> userList = dto.getUserId();
            if(userList.size()== 0){
                return new UnHideColumnResponseDTO(Status.FAILED_VALIDATION);
            }else if(userList.size() != 0) {
                for (String userId : userList) {
                    List<String> usercolumnhide = columnObj.getColumnSecret().getUsersId();
                    if (usercolumnhide == null) {
                        columnObj.getColumnSecret().setUsersId(userList);
                    } else {
                        usercolumnhide.add(userId);
                    }
                }
            }
            columnObj.setCreatedTime(new Date());
            columnObj.setVisibility(true);

            columnRepository.save(columnObj);

            return new UnHideColumnResponseDTO(Status.SUCCESS,columnObj);
        } catch (Exception e) {
            logError(SERVICE_NAME, "markColumnAsVisible", e);
            return new UnHideColumnResponseDTO(Status.INTERNAL_ERROR);
        }

    }

    @Override
    public BasicResponseDTO fetchProjects(String userId) {
        try {

            List<ProjectUser> projectUserList = projectUserRepository.findByTenantIdAndUserId(getTenantId(),userId);

            List<Project> allUserProject = new ArrayList<Project>();
            if(projectUserList.size() != 0){
                for(ProjectUser projects: projectUserList){
                    Project eachProject = projectRepository.findByTenantIdAndId(getTenantId(), projects.getProjectId());
                    allUserProject.add(eachProject);
                }
            }
            return new BasicResponseDTO(Status.SUCCESS,allUserProject);
        } catch (Exception e) {
            logError(SERVICE_NAME, "ProjectList", e);
            return new BasicResponseDTO(Status.INTERNAL_ERROR);
        }
    }


    @Override
    public BasicResponseDTO createLogHour(LogHourInputDTO dto,String taskId,String projectId){
        try{

            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new BasicResponseDTO(Status.NOT_FOUND);
            }
            Task task = getTask(getTenantId(),taskId);
            if (taskNotFound(task)){
                return new BasicResponseDTO(Status.NOT_FOUND);
            }
            LogHours logHours = new LogHours();
            LogHourSecret logHourSecret = new LogHourSecret();
            setAndSaveLogHourParameters(logHours,logHourSecret,dto,projectId,taskId);
            Object loggedHours = totalLoggedHours(taskId);

            return new BasicResponseDTO(Status.CREATED,loggedHours);
        }catch(Exception e){
            logError(SERVICE_NAME, "createLogHour", e);
            return new BasicResponseDTO(Status.INTERNAL_ERROR);
        }

    }


    private void renameAndSaveColumn(Column column, RenameColumnInputDTO dto, String projectId) {
        ColumnSecret columnSecret = column.getColumnSecret();
        String oldColumnName = columnSecret.getName();
        columnSecret.setName(dto.getName());
        String action = "renameColumn";
        Project project = getProject(getTenantId(), projectId);
        ProjectSecret projectSecret = project.getProjectSecret();
        String projectName = projectSecret.getName();
        RenameColumnData renameColumnData = new RenameColumnData(getUserFullName(), oldColumnName, dto.getName(), projectName, new Date(), action);
        List<Object> projectLog = getExisting(projectSecret.getProjectLog());
        projectLog.add(renameColumnData);
        projectSecret.setProjectLog(projectLog);
        project.setProjectSecret(projectSecret);
        columnRepository.save(column);
        projectRepository.save(project);

    }


    @Override
    public ColumnListResponseDTO repositionColumns(String projectId,RepositionColumnInputDTO dto) {
        try{
            Project project = getProject(getTenantId(),projectId);
            ProjectSecret projectSecret = project.getProjectSecret();
            String projectName = projectSecret.getName();
            String action = "repositionColumn";
            int counter = 0;
            for (String columnId: dto.getColumnIdList()){
                Column eachColumn = columnRepository.findByTenantIdAndProjectIdAndId(getTenantId(),projectId,columnId);
                if (columnNotFound(eachColumn)){
                    return new ColumnListResponseDTO(Status.NOT_FOUND);
                }
                eachColumn.setPosition(counter);
                counter++;
                columnRepository.save(eachColumn);
            }

            RepositionColumnData repositionColumnData = new RepositionColumnData(getUserFullName(),new Date(),projectName,action);
            updateProjectLog(project,repositionColumnData);
            projectRepository.save(project);
            List<Column> columnList = fetchColumnList(getTenantId(),projectId);

            return new ColumnListResponseDTO(Status.SUCCESS,columnList);

        }catch (Exception e){
            logError(SERVICE_NAME, "renameProject", e);
            return new ColumnListResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public TaskResponseDTO createTask(CreateTaskInputDTO dto ,String projectId) {
        try{
            Task task = new Task();
            TaskSecret taskSecret = new TaskSecret();
            Task newTask = setAndSaveTaskParameters(task,taskSecret,dto,projectId);
            taskUniqueFieldKey(newTask,projectId);
            List<Column> columns = fetchCreatedColumns(projectId);

            return new TaskResponseDTO(Status.CREATED, newTask,columns);
        }catch(Exception e){
            logError(SERVICE_NAME, "createTask", e);
            return new TaskResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    @Transactional
    public TaskResponseDTO updateTask(UpdateTaskInputDTO dto, String taskId,String projectId) {
        try{

            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new TaskResponseDTO(Status.NOT_FOUND);
            }
            Task task = getTask(getTenantId(),taskId);

            if (taskNotFound(task)){
                return new TaskResponseDTO(Status.NOT_FOUND);
            }

            TaskSecret taskSecret = task.getTaskSecret();
            String taskName = taskSecret.getSummary();
            String newtaskName = dto.getSummary();
            String subTaskSummary = dto.getSubTask();
            ProjectSecret projectSecret = project.getProjectSecret();
            String projectName = projectSecret.getName();

            updateWatchers(dto, task, taskSecret, taskName, project, projectName);


            updateDescription(dto, task, taskSecret, taskName, project, projectName);

            updateSummary(dto, task, taskSecret, taskName, newtaskName, project, projectName);

            updateSubTask(dto, task, taskSecret, taskName, subTaskSummary, project, projectName);

            updateComment(dto, task, taskSecret, taskName, project, projectName);

            updateAttachments(dto, task, taskSecret, taskName, project, projectName);

            updateAssignee(dto, task, taskSecret, taskName, project, projectName);

            updatePriority(dto,taskSecret);

            task.setTaskSecret(taskSecret);
            task.setLastUpdatedTime(new Date());
            taskRepository.save(task);

            return new TaskResponseDTO(Status.SUCCESS ,task);
        } catch(Exception e){
            logError(SERVICE_NAME,"updateTask",e);
            return new TaskResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    private void updateWatchers(UpdateTaskInputDTO dto, Task task, TaskSecret taskSecret, String taskName, Project project, String projectName) {
        if (isUpdating("watcher",dto.isWatcher(),dto) && isWatcher(dto) && isNotAnExistingWatcher(taskSecret)) {
            List<Watchers> existingWatchers = getExisting(taskSecret.getWatchers());
            existingWatchers.add(new Watchers(getUserId(), getUserFullName(),getUsername()));
            taskSecret.setWatchers(existingWatchers);
            WatchTaskData watchTaskData = new WatchTaskData(getUserFullName(),new Date(),"startedWatchingTask",taskName,projectName);
            updateProjectLog(project,watchTaskData);
            updateTaskLog(task,watchTaskData);
        }else if (isUpdating("watcher",dto.isWatcher(),dto) && !isWatcher(dto) && isAnExistingWatcher(taskSecret)){
            removeWatcher(taskSecret);
            WatchTaskData watchTaskData = new WatchTaskData(getUserFullName(),new Date(),"stoppedWatchingTask",taskName,projectName);
            updateTaskLog(task,watchTaskData);
            updateProjectLog(project,watchTaskData);
        }
    }

    private void updateAssignee(UpdateTaskInputDTO dto, Task task, TaskSecret taskSecret, String taskName, Project project, String projectName) {
        if (isUpdating("assignee",dto.getAssignee(),dto)){
            Assignee assignee = dto.getAssignee();
            taskSecret.setAssignee(assignee);
            AssigneeData assigneeData = new AssigneeData(dto.getAssignee().getUserName(),new Date(),getUserFullName(),"assignTask",taskName,projectName);
            sendEmail(task, dto, UpdateType.ASSIGNEE);
            String message = assigneeData.toString();
            updateProjectLog(project,assigneeData);
            updateTaskLog(task,assigneeData);

        }
    }

    private void updateAttachments(UpdateTaskInputDTO dto, Task task, TaskSecret taskSecret, String taskName, Project project, String projectName) throws InvalidKeySpecException, NoSuchAlgorithmException {
        if (isUpdating("attachments", dto.getAttachments(), dto) && attachmentTokenIsValid(dto.getAttachments())) {
            List<Attachments> existingAttachments = getExisting(taskSecret.getAttachments());
            existingAttachments.addAll(getAttachmentsFromToken(dto.getAttachments()));
            UploadAttachmentData uploadAttachmentData = new  UploadAttachmentData(getUserFullName(), "uploadAttachment",new Date(),taskName,projectName);
            sendEmail(task, dto, UpdateType.ATTACHMENTS);
            updateTaskLog(task,uploadAttachmentData);
            updateProjectLog(project,uploadAttachmentData);
            taskSecret.setAttachments(existingAttachments);
        }
    }

    private void updateComment(UpdateTaskInputDTO dto, Task task, TaskSecret taskSecret, String taskName, Project project, String projectName) {
        if (isUpdating("comment", dto.getComment(), dto)) {
            List<Comment> existingComments = getExisting(taskSecret.getComments());
            existingComments.add(new Comment(dto.getComment(), getUserId(), getUserFullName(),getUsername()));
            AddCommentData addCommentData = new AddCommentData(getUserFullName(),new Date(),taskName,projectName,"addCommentToTask");
            sendEmail(task, dto, UpdateType.COMMENTS);
             updateTaskLog(task,addCommentData);
             updateProjectLog(project,addCommentData);
            taskSecret.setComments(existingComments);
        }
    }

    private void updateSubTask(UpdateTaskInputDTO dto, Task task, TaskSecret taskSecret, String taskName, String subTaskSummary, Project project, String projectName) {
        if (isUpdating("subTask", dto.getSubTask(), dto)) {
            List<SubTask> existingSubtasks = getExisting(taskSecret.getSubTasks());
            existingSubtasks.add(new SubTask(dto.getSubTask(), getUserId(), getUserFullName()));
            AddSubTaskData addSubTaskData = new AddSubTaskData(getUserFullName(), "addSubTask",subTaskSummary, taskName, projectName, new Date());
            updateTaskLog(task,addSubTaskData);
            updateProjectLog(project,addSubTaskData);
            taskSecret.setSubTasks(existingSubtasks);

        }
    }

    private void updateSummary(UpdateTaskInputDTO dto, Task task, TaskSecret taskSecret, String taskName, String newtaskName, Project project, String projectName) {
        if (isUpdating("summary", dto.getSummary(), dto)) {
            UpdateTaskSummaryData updateSummaryTaskData = new UpdateTaskSummaryData(getUserFullName(), "updateTaskSummary",new Date(),taskName,newtaskName,projectName);
            sendEmail(task, dto, UpdateType.SUMMARY);
            updateTaskLog(task,updateSummaryTaskData);
            updateProjectLog(project,updateSummaryTaskData);
            taskSecret.setSummary(dto.getSummary());
        }
    }

    private void updateDescription(UpdateTaskInputDTO dto, Task task, TaskSecret taskSecret, String taskName, Project project, String projectName) {
        if (isUpdating("description", dto.getDescription(), dto)) {
            UpdateDescriptionData updateDescriptionData = new UpdateDescriptionData(getUserFullName(), "updateTaskDescription",new Date(),taskName, projectName);
            sendEmail(task, dto, UpdateType.DESCRIPTION);
            String logMessage = updateDescriptionData .toString();
            updateTaskLog(task,updateDescriptionData);
            updateProjectLog(project,updateDescriptionData);
            taskSecret.setDescription(dto.getDescription());
        }
    }

    private void updatePriority(UpdateTaskInputDTO dto,TaskSecret taskSecret){
        if(isUpdating("priority",dto.getPriority(),dto)) {
                taskSecret.setPriority(dto.getPriority().toString());
            }

    }






    @Override
    public TaskResponseDTO fetchTask(String taskId,String projectId) {
        try{
            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new TaskResponseDTO(Status.NOT_FOUND);
            }
            Task task = getTask(getTenantId(),taskId);
            if (taskNotFound(task)){
                return new TaskResponseDTO(Status.NOT_FOUND);
            }
            List<Column> columns = fetchCreatedColumns(task.getProjectId());
            return new TaskResponseDTO(Status.SUCCESS, task,columns);
        } catch (Exception e){
            logError(SERVICE_NAME, "getTask", e);
            return new TaskResponseDTO(Status.INTERNAL_ERROR);
        }

    }

    @Override
    public BasicResponseDTO fetchLogHour(String taskId) {
        try{

            Task task = getTask(getTenantId(),taskId);
            if (taskNotFound(task)){
                return new BasicResponseDTO(Status.NOT_FOUND);
            }
            Object logHours = totalLoggedHours(taskId);
            return new BasicResponseDTO(Status.SUCCESS,logHours);
        } catch (Exception e){
            logError(SERVICE_NAME, "getLogHour", e);
            return new BasicResponseDTO(Status.INTERNAL_ERROR);
        }

    }

    @Override
    public TaskListResponseDTO fetchTasks(String projectId) {
        try {
            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new TaskListResponseDTO(Status.NOT_FOUND);
            }
            List<Task> taskList = getTaskList(getTenantId(),projectId);
            return new TaskListResponseDTO(Status.SUCCESS, taskList );

        } catch (Exception e) {
            logError(SERVICE_NAME, "listTasks", e);
            return new TaskListResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public GoalResponseDTO createGoal(CreateGoalInputDTO goal, String projectId) {
        try {
            Goal goalObj = saveGoal(goal,projectId);
            if (saveError(goalObj)) {
                return new GoalResponseDTO(Status.INVALID_SERVICE_ERROR);
            }

            return new GoalResponseDTO(Status.CREATED, goalObj );

        } catch (Exception e) {
            logError(SERVICE_NAME, "createGoal", e);
            return new GoalResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public FieldsResponseDTO createFields(CreateFieldsInputDTO field ,String projectId) {
        try{

            Fields fields = new Fields();
            FieldsSecret fieldsSecret = new FieldsSecret();
            Fields fieldsObj = saveFields(fields,field,projectId,fieldsSecret);
            if (saveError(fieldsObj)) {
                return new FieldsResponseDTO(Status.INTERNAL_ERROR);
            }


            return new FieldsResponseDTO(Status.CREATED,fieldsObj);
        }catch(Exception e){
            logError(SERVICE_NAME, "createField", e);
            return new FieldsResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public FieldsResponseDTO updateFields(UpdateFieldInputDTO field ,String fieldId) {
        try{

            Fields fields = getField(getTenantId(),fieldId);
            FieldsSecret fieldsSecret = fields.getFieldsSecret();
            if (fieldNotFound(fields)){
                return new FieldsResponseDTO(Status.NOT_FOUND);
            }

            updateField(fields,field);
            fields.setFieldsSecret(fieldsSecret);
            fields.setLastModifiedTime(new Date());
            fieldsRepository.save(fields);

            return new FieldsResponseDTO(Status.SUCCESS,fields);
        }catch(Exception e){
            logError(SERVICE_NAME, "updateField", e);
            return new FieldsResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    private void updateField(Fields fields,UpdateFieldInputDTO dto) {
        FieldsSecret fieldsSecret = fields.getFieldsSecret();

        if (isUpdating("name", dto.getName(), dto)) {
            fieldsSecret.setName(dto.getName());
        }
        if (isUpdating("description", dto.getDescription(), dto)) {
            fieldsSecret.setDescription(dto.getDescription());
        }
        if (isUpdating("type", dto.getType(), dto)) {
            FieldType fieldType = FieldType.valueOf(dto.getType().toUpperCase());
            String fieldTypeObj = null;
            switch (fieldType){
                case NUMBER:
                    fieldTypeObj = "NUMBER";
                    break;
                case MONEY:
                    fieldTypeObj = "MONEY";
                    break;
                case DATE:
                    fieldTypeObj = "DATE";
                    break;
                case TEXT:
                    fieldTypeObj = "TEXT";
                    break;
                default:
            }
            fieldsSecret.setType(fieldTypeObj);

        }
        if (isUpdating("value", dto.getValue(), dto)) {
            fieldsSecret.setValue(dto.getValue());
        }
        fields.setFieldsSecret(fieldsSecret);

    }

    @Override
    public GoalResponseDTO updateGoal(UpdateGoalInputDTO dto, String goalId,String projectId) {
        try {
            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new GoalResponseDTO(Status.NOT_FOUND);
            }
            Goal goal = goalRepository.findByTenantIdAndId(getTenantId(), goalId);
            if (goal == null) {
                return new GoalResponseDTO(Status.NOT_FOUND);
            }

            updateGoalParameters(goal,dto);
            goal.setLastModifiedTime(new Date());

            goalRepository.save(goal);

            return new GoalResponseDTO(Status.SUCCESS, goal);
        } catch (Exception e) {
            logError(SERVICE_NAME, "updateGoal", e);
            return new GoalResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    private void updateGoalParameters(Goal goal,UpdateGoalInputDTO dto) {
        GoalSecret goalSecret = goal.getGoalSecret();
        if (isUpdating("name", dto.getName(), dto)) {
            goalSecret.setName(dto.getName());
        }
        if (isUpdating("description", dto.getDescription(), dto)) {
            goalSecret.setDescription(dto.getDescription());
        }
        if (isUpdating("startDate", dto.getStartDate(), dto)) {
            goalSecret.setStartDate(dto.getStartDate());
        }
        if (isUpdating("stopDate", dto.getStopDate(), dto)) {
            goalSecret.setStopDate(dto.getStopDate());
        }
        goal.setGoalSecret(goalSecret);

    }


    @Override
    public FieldListResponseDTO fetchFields(String projectId) {
        try {
            List<Fields> fieldsList = getFieldList(getTenantId(),projectId);
            return new FieldListResponseDTO(Status.SUCCESS,fieldsList);

        } catch (Exception e) {
            logError(SERVICE_NAME, "listFields", e);
            return new FieldListResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public ColumnListResponseDTO fetchColumns(String projectId) {
        try {
            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new ColumnListResponseDTO(Status.NOT_FOUND);
            }
            List<Column> columnList = getColumnList(getTenantId(),projectId);
            return new ColumnListResponseDTO(Status.SUCCESS,columnList);

        } catch (Exception e) {
            logError(SERVICE_NAME, "listFields", e);
            return new ColumnListResponseDTO(Status.INTERNAL_ERROR);
        }
    }
    @Override
    public GoalListResponseDTO fetchGoals(String projectId){
        try {
            List<Goal> goalList = getGoalList(getTenantId(), projectId);
            return new GoalListResponseDTO(Status.SUCCESS, goalList);
        } catch (Exception e) {
            logError(SERVICE_NAME, "GoalList", e);
            return new GoalListResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public GoalResponseDTO fetchGoal(String goalId) {
        try{

            Goal goal = getGoal(getTenantId(),goalId);
            if (goalNotFound(goal)){
                return new GoalResponseDTO(Status.NOT_FOUND);
            }

            List<Task> tasks = taskRepository.findByTenantIdAndGoalId(getTenantId(), new ObjectId(goalId));
            return new GoalResponseDTO(Status.SUCCESS,goal,tasks);
        } catch (Exception e){
            logError(SERVICE_NAME, "getTask", e);
            return new GoalResponseDTO(Status.INTERNAL_ERROR);
        }

    }

    @Override
    public CommentListResponseDTO updateComment(UpdateCommentInputDTO dto, String commentUuid,String taskId,String projectId) {
        try{
            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new CommentListResponseDTO(Status.NOT_FOUND);
            }
            List<Comment> commentsList = fetchCommentList(getTenantId(), taskId);
            getCommentFromCommentListAndUpdateComment(commentsList,dto,commentUuid);
            Task task = getTask(getTenantId(),taskId);
            TaskSecret taskSecret = task.getTaskSecret();
            taskSecret.setComments(commentsList);
            taskRepository.save(task);
            return new CommentListResponseDTO(Status.SUCCESS,commentsList);
        } catch(Exception e){
            logError(SERVICE_NAME, "updateComment", e);
            return new CommentListResponseDTO(Status.INTERNAL_ERROR);
        }
    }




    @Override
    public SubTaskListResponseDTO updateSubTask(UpdateSubTaskInputDTO dto, String subTaskUuid, String taskId,String projectId) {
        try{
            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new SubTaskListResponseDTO(Status.NOT_FOUND);
            }
            List<SubTask> subTaskList = fetchSubTaskList(getTenantId(),taskId);
            getSubTaskFromSubTaskListAndUpdateSubTask(subTaskList,dto,subTaskUuid);
             Task task = getTask(getTenantId(),taskId);
             TaskSecret taskSecret = task.getTaskSecret();
             taskSecret.setSubTasks(subTaskList);

             updateSubTaskLog(dto,projectId,taskId);
             taskRepository.save(task);
            return new SubTaskListResponseDTO(Status.SUCCESS,subTaskList);
        } catch(Exception e){
            logError(SERVICE_NAME, "updateSubTask", e);
            return new SubTaskListResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    private void updateSubTaskLog(UpdateSubTaskInputDTO dto, String projectId,String taskId){
        Task task = getTask(getTenantId(),taskId);
        TaskSecret taskSecret = task.getTaskSecret();
        String taskName = taskSecret.getSummary();
        String subTaskSummary = dto.getSummary();
        String action = "updatedSubTask";
        Project project = getProject(getTenantId(), projectId);
        ProjectSecret projectSecret = project.getProjectSecret();
        String projectName = projectSecret.getName();
        UpdateSubTaskData updateSubTaskData = new   UpdateSubTaskData(getUserFullName(),action , subTaskSummary , taskName,projectName, new Date());
        updateProjectLog(project,updateSubTaskData);
        updateTaskLog(task,updateSubTaskData);
    }

    @Override
    public TaskResponseDTO moveTask(MoveTaskInputDTO dto, String taskId,String projectId) {
        try{
            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new TaskResponseDTO(Status.NOT_FOUND);
            }
            Task task = getTask(getTenantId(),taskId);
            if (taskNotFound(task)){
                return new TaskResponseDTO(Status.NOT_FOUND);
            }
            String taskName = task.getTaskSecret().getSummary();
            String oldColumnName = getColumn(getTenantId(),task.getColumnId()).getColumnSecret().getName();
            String newColumnName = getColumn(getTenantId(),dto.getColumnId()).getColumnSecret().getName();
            String projectName = project.getProjectSecret().getName();
            String action = "moveTask";
            task.setColumnId(dto.getColumnId());
            taskRepository.save(task);
            MoveTaskData moveTaskData = new MoveTaskData(getUserFullName(),oldColumnName,newColumnName,new Date(),taskName,projectName,action);
            updateTaskLog(task,moveTaskData);
            updateProjectLog(project,moveTaskData);
            return new TaskResponseDTO(Status.SUCCESS,task);

        }catch (Exception e){
            logError(SERVICE_NAME, "moveTask", e);
            return new TaskResponseDTO(Status.INTERNAL_ERROR);

        }

    }

    @Override
    public BasicResponseDTO deleteAttachment(String taskId, String attachmentUuid,String projectId) {
        try{
            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new BasicResponseDTO(Status.NOT_FOUND);
            }
            Task task = getTask(getTenantId(),taskId);
            if(taskNotFound(task)){
                return new BasicResponseDTO(Status.NOT_FOUND);
            }

            removeAttachment(task,attachmentUuid);
            taskRepository.save(task);
            return new BasicResponseDTO(Status.SUCCESS);
        } catch(Exception e){
            logError(SERVICE_NAME, "deleteAttachment", e);
            return new BasicResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    @Override
    public BasicResponseDTO deleteComment(String taskId, String commentUuid,String projectId) {
        try{
            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new BasicResponseDTO(Status.NOT_FOUND);
            }
            Task task = getTask(getTenantId(),taskId);
            if (taskNotFound(task)){
                return new BasicResponseDTO(Status.NOT_FOUND);
            }
            ProjectSecret projectSecret = project.getProjectSecret();
            String projectName = projectSecret.getName();
            String taskName = task.getTaskSecret().getSummary();
            DeleteCommentData deleteCommentData = new DeleteCommentData(getUserFullName(),"deleteComment",taskName,projectName,new Date());
            updateTaskLog(task,deleteCommentData);
            updateProjectLog(project,deleteCommentData);
            removeComment(task,commentUuid);
            taskRepository.save(task);

            return new BasicResponseDTO(Status.SUCCESS);

        }catch (Exception e){
            logError(SERVICE_NAME, "deleteComments", e);
            return new BasicResponseDTO(Status.INTERNAL_ERROR);

        }
    }

    @Override
    public BasicResponseDTO deleteSubTask(String taskId, String subTaskUuid,String projectId) {
        try{
            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new BasicResponseDTO(Status.NOT_FOUND);
            }
            Task task = getTask(getTenantId(),taskId);
            if (taskNotFound(task)){
                return new BasicResponseDTO(Status.NOT_FOUND);
            }
            String taskName = task.getTaskSecret().getSummary();
            ProjectSecret projectSecret = project.getProjectSecret();
            String projectName = projectSecret.getName();
            String action = " deleteSubTask ";
            removeSubTask(task,subTaskUuid);
            taskRepository.save(task);
            DeleteSubtaskData deleteSubtaskData = new DeleteSubtaskData(getUserFullName(),new Date(),taskName,projectName,action);
            updateProjectLog(project,deleteSubtaskData);
            updateTaskLog(task,deleteSubtaskData);
            return new BasicResponseDTO(Status.SUCCESS);

        }catch (Exception e){
            logError(SERVICE_NAME, "deleteComments", e);
            return new BasicResponseDTO(Status.INTERNAL_ERROR);

        }
    }


    @Override
    public BasicResponseDTO deleteTask(String taskId,String projectId) {

        try{
            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new BasicResponseDTO(Status.NOT_FOUND);
            }
            Task task = getTask(getTenantId(),taskId);
            if (taskNotFound(task)){
                return new BasicResponseDTO(Status.NOT_FOUND);
            }
            String taskSummary = task.getTaskSecret().getSummary();

            ProjectSecret projectSecret = project.getProjectSecret();
            String projectName = projectSecret.getName();
            String action = " deleteTask ";
            DeleteTaskData deleteTaskData = new DeleteTaskData(getUserFullName(),new Date(),taskSummary,projectName,action);
            taskRepository.delete(task);
            updateProjectLog(project,deleteTaskData);
            return new BasicResponseDTO(Status.SUCCESS);
        }catch (Exception e){
            logError(SERVICE_NAME, "deleteTask", e);
            return new BasicResponseDTO(Status.INTERNAL_ERROR);
        }



    }



    @Override
    public BasicResponseDTO deleteColumn(String columnId,String projectId) {
        try{
            Project project = getProject(getTenantId(),projectId);
            if (projectNotFound(project)){
                return new BasicResponseDTO(Status.NOT_FOUND);
            }
            Column column = getColumn(getTenantId(),columnId);
            if(columnNotFound(column)){
                return new BasicResponseDTO(Status.NOT_FOUND);
            }

           deleteAColumn(columnId,projectId);
            columnRepository.delete(column);
            return new BasicResponseDTO(Status.SUCCESS);
        }catch (Exception e){
            logError(SERVICE_NAME, "deleteColumn", e);
            return new BasicResponseDTO(Status.INTERNAL_ERROR);
        }

    }
    private void deleteAColumn(String columnId,String projectId) {
        Column column = getColumn(getTenantId(),columnId);
        ColumnSecret columnSecret = column.getColumnSecret();
        String columnName = columnSecret.getName();
        String action = "deleteColumn";
        Project project = getProject(getTenantId(), projectId);
        ProjectSecret projectSecret = project.getProjectSecret();
        String projectName = projectSecret.getName();
        DeleteColumnData deleteColumnData = new DeleteColumnData(getUserFullName(), action, columnName,projectName, new Date());
        List<Object> projectLog = getExisting(projectSecret.getProjectLog());
        projectLog.add(deleteColumnData);
        projectSecret.setProjectLog(projectLog);
        project.setProjectSecret(projectSecret);
        columnRepository.delete(column);
        projectRepository.save(project);

    }

    @Override
    public BasicResponseDTO deleteProject(String projectId) {
        try{
            Project project = getProject(getTenantId(),projectId);
            ProjectSecret projectSecret = project.getProjectSecret();
            String projectName = projectSecret.getName();
            if(projectNotFound(project)){
                return new BasicResponseDTO(Status.NOT_FOUND);
            }
            String action = "deleteProject";
            DeleteProjectData deleteProjectData = new  DeleteProjectData(getUserFullName(),  new Date(),projectName,action);
            List<Object> projectLog = getExisting(projectSecret.getProjectLog());
            projectLog.add(deleteProjectData);
            projectSecret.setProjectLog(projectLog);
            project.setProjectSecret(projectSecret);
            projectRepository.delete(project);
            return new BasicResponseDTO(Status.SUCCESS,project);
        }catch (Exception e){
            logError(SERVICE_NAME, "deleteProject", e);
            return new BasicResponseDTO(Status.INTERNAL_ERROR);
        }
    }

    private void deleteAttachmentLog( String projectId,String taskId,Attachments attachments){
        Task task = getTask(getTenantId(),taskId);
        TaskSecret taskSecret = task.getTaskSecret();
        String taskName = taskSecret.getSummary();
        String filename = attachments.getFilename();
        String action = "deleteAttachment";
        Project project = getProject(getTenantId(), projectId);
        ProjectSecret projectSecret = project.getProjectSecret();
        String projectName = projectSecret.getName();
        DeleteAttachmentData deleteAttachmentData = new DeleteAttachmentData(getUserFullName(),action,filename, taskName,projectName,  new Date());
        updateProjectLog(project,deleteAttachmentData);
        updateTaskLog(task,deleteAttachmentData);

    }

    private List<Attachments> fetchAttachmentList(String tenantId, String taskId) {
        return taskRepository.findByTenantIdAndId(tenantId,taskId).getTaskSecret().getAttachments();
    }

    private List<Column> fetchColumnList(String tenantId,String projectId) {
        return columnRepository.findByTenantIdAndProjectId(tenantId,projectId);

    }
    private List<Task> fetchTaskList(String tenantId,String projectId) {
        return taskRepository.findByTenantIdAndProjectId(tenantId,projectId);

    }

    private Task getTask(String tenantId, String taskId) {
        return taskRepository.fetchTaskByTenantIdAndId(tenantId,taskId);
    }

    private Fields getField(String tenantId, String fieldId) {
        return fieldsRepository.findByTenantIdAndId(tenantId,fieldId);
    }


    private Column getTaskColumn(String tenantId,String columnId,String projectId) {
        return columnRepository.findByTenantIdAndProjectIdAndId(tenantId,columnId,projectId);
    }

    private Goal getGoal(String tenantId, String goalId) {
        return goalRepository.findByTenantIdAndId(tenantId,goalId);
    }



    private void getCommentFromCommentListAndUpdateComment(List<Comment> commentsList, UpdateCommentInputDTO dto, String commentUuid) {
        for (Comment c : commentsList) {
            if (c.getUuid().equals(commentUuid)){
                c.setComment(dto.getComment());
                c.setLastUpdatedTime(new Date());
                commentRepository.save(c);
            }
        }
    }

    private void getSubTaskFromSubTaskListAndUpdateSubTask(List<SubTask> subTaskList, UpdateSubTaskInputDTO dto, String subTaskUuid) {
        for (SubTask subTask : subTaskList) {
            if (subTask.getUuid().equals(subTaskUuid)){
                subTask.setSummary(dto.getSummary());
                subTask.setLastUpdatedTime(new Date());

                subTaskRepository.save(subTask);
            }
        }
    }




    private List<Comment> fetchCommentList(String tenantId, String taskId) {
        return  taskRepository.findByTenantIdAndId(tenantId,taskId).getTaskSecret().getComments();
    }


    private Column renameAndSaveColumn(Column column,RenameColumnInputDTO dto) {
        column.getColumnSecret().setName(dto.getName());
        column.setNameHash(dto.getName());
        column.setLastUpdatedTime(new Date());
        columnRepository.save(column);
        return column;
    }

    private Column getColumn(String tenantId, String columnId) {
        return columnRepository.findByTenantIdAndId(tenantId, columnId);
    }

    private boolean columnIdNotFound(String id) {
        return id == null;
    }

    private Task setAndSaveTaskParameters(Task task, TaskSecret taskSecret, CreateTaskInputDTO dto, String projectId) {
        taskSecret.setSummary(dto.getSummary());
        task.setTaskSecret(taskSecret);
        task.setCreatedTime(new Date());
        task.setTenantId(getTenantId());
        task.setProjectId(projectId);
        task.setUserName(getUsername());
        task.setUserId(getUserId());
        String action = "createTask";
        Project project = getProject(getTenantId(),projectId);
        ProjectSecret projectSecret = project.getProjectSecret();
        String projectName = projectSecret.getName();
        CreateTaskData createTaskData = new  CreateTaskData(getUserFullName(),dto.getSummary(),projectName,new Date(),action);
        List<Object> taskLog = getExisting(taskSecret.getTaskLog());
        List<Object> projectLog = getExisting(projectSecret.getProjectLog());
        taskLog.add(createTaskData);
        projectLog.add(createTaskData);
        projectSecret.setProjectLog(projectLog);
        taskSecret.setTaskLog(taskLog);
        List<Column> columnList = fetchColumnList(getTenantId(),projectId);
        for (Column column : columnList){
            if (column.getPosition() == getFirstColumnPosition(projectId)){
                String firstColumnId = column.getId();
                task.setColumnId(firstColumnId);
            }
        }
        projectRepository.save(project);
        taskRepository.save(task);
        return task;
    }

    private boolean columnNotFound(Column column) {
        return   column == null;
    }

    private boolean newColumnPositionIsGreaterThanLastColumnPosition(int newColumnPosition, int lastColumnPosition) {
        return   newColumnPosition >  lastColumnPosition;
    }

    private int lastColumnPosition(List<Column> allColumns) {
        int maxPosition = 1;
        for(Column column : allColumns){
            if(column.getPosition() > maxPosition){
                maxPosition = column.getPosition();
            }
        }

        return maxPosition;
    }

    private boolean commentNotFound(String commentId) {
        return commentId == null;
    }

    private boolean isWatcher(UpdateTaskInputDTO dto){
        return dto.isWatcher();
    }

    private boolean taskNotFound(Task task) {
        return task == null;
    }
    private boolean fieldNotFound(Fields fields) {
        return fields == null;
    }

    private boolean columnListNotFound(List<Column> columnList) {
        return columnList == null;
    }

    private List<Task> fetchCreatedTasks(String projectId) {
        return taskRepository.findByTenantIdAndProjectId(getTenantId(), projectId);
    }

    private List<Goal> fetchCreatedGoals(String projectId) {
        return goalRepository.findByTenantIdAndProjectId(getTenantId(), projectId);
    }
    private List<Task> countCreatedTasks(String projectId) {
        return taskRepository.countByTenantIdAndProjectId(getTenantId(), projectId);
    }

    private Goal saveGoal(CreateGoalInputDTO dto,String projectId) throws NoSuchAlgorithmException {
        Goal goalObj = Goal.createGoalWithDefaults(dto.getName(), dto.getStartDate(),dto.getStopDate(),hash(dto.getName()),dto.getDescription(),projectId,getTenantId(),getUserId());
        goalRepository.save(goalObj);
        return goalObj ;

    }

    private List<Task> getTaskList(String tenantId, String projectId) {
        return taskRepository.findByTenantIdAndProjectId(tenantId,projectId);
    }

    private List<Project> fetchProjectList(String tenantId, Pageable pageable){
        return projectRepository.findByTenantId(tenantId, pageable);
    }


    private List<Column> fetchCreatedColumns(String projectId) {
        return columnRepository.findAllByProjectIdOrderByPositionAsc(projectId);
    }

    private int getNextColumnPosition(String projectId) {
        Column lastColumn = columnRepository.findFirstByProjectIdOrderByPositionDesc(projectId);
        return lastColumn != null ? lastColumn.getPosition() + 1 : 0;
    }

    private int getFirstColumnPosition(String projectId){
        Column firstColumn = columnRepository.findFirstByProjectIdOrderByPositionAsc(projectId);
        return firstColumn != null ? firstColumn.getPosition() : 0;
    }

    private void removeWatcher(TaskSecret taskSecret) {
        List<Watchers> existingWatchers = getExisting(taskSecret.getWatchers());
        int index = 0;
        for (Watchers watcher : existingWatchers){
            if (watcher.getUserId().equals(getUserId())){
                index = existingWatchers.indexOf(watcher);
                break;
            }
        }
        existingWatchers.remove(index);
    }

    private boolean isNotAnExistingWatcher(TaskSecret taskSecret) {
        return taskSecret.getWatchers() == null || taskSecret.getWatchers().stream().noneMatch(p -> p.getUserId().equals(getUserId()));
    }

    private boolean isAnExistingWatcher(TaskSecret taskSecret) {
        return taskSecret.getWatchers() == null || taskSecret.getWatchers().stream().anyMatch(p -> p.getUserId().equals(getUserId()));
    }

    private boolean isUpdating(String field, Object data, UpdateTaskInputDTO dto) {
        return dto.getUpdatedFields().contains(field) && data != null;
    }

    private boolean isUpdating(String field, Object data, UpdateGoalInputDTO dto) {
        return dto.getUpdatedFields().contains(field) && data != null;
    }

    private boolean isUpdating(String field, Object data, UpdateProjectInputDTO dto) {
        return dto.getUpdatedFields().contains(field) && data != null;
    }

    private boolean isUpdating(String field, Object data, UpdateFieldInputDTO dto) {
        return dto.getUpdatedFields().contains(field) && data != null;
    }

    private <T> List<T> getExisting(List<T> t) {
        return t == null ? new ArrayList() : t;
    }

    private boolean attachmentTokenIsValid(String token) throws InvalidKeySpecException, NoSuchAlgorithmException {
        return tokenProvider.validateToken(token);
    }

    private List<SubTask> fetchSubTaskList(String tenantId, String taskId) {
        return getTask(tenantId,taskId).getTaskSecret().getSubTasks();
    }

    private List<Attachments> getAttachmentsFromToken(String token) {
        Map<String, Claim> claim = tokenProvider.getClaims(token);
        Claim attachmentInfo = claim.get("data");
        return attachmentInfo.asList(Attachments.class);
    }

    private void removeAttachment(Task task, String url) {
        List<Attachments> attachmentsList = task.getTaskSecret().getAttachments();
        int index = 0;
        for (Attachments attachment : attachmentsList){
            if (attachment.getUrl().equals(url)){
                index = attachmentsList.indexOf(attachment);
                String projectId = task.getProjectId();
                String taskId = task.getId();
               deleteAttachmentLog(projectId,taskId,attachment);
                break;
            }
        }
        attachmentsList.remove(index);
    }

    private void removeComment(Task task, String commentUuid) {
        List<Comment> commentList = task.getTaskSecret().getComments();
        int index = 0;
        for (Comment comment : commentList){
            if (comment.getUuid().equals(commentUuid)){
                index = commentList.indexOf(comment);
                break;
            }
        }
        commentList.remove(index);
    }

    private void removeSubTask(Task task, String subTaskUuid) {
        List<SubTask> subTaskList = task.getTaskSecret().getSubTasks();

        int index = 0;
        for(SubTask subTask :subTaskList){
            if (subTask.getUuid().equals(subTaskUuid)){
                index = subTaskList.indexOf(subTask);
                break;
            }
        }
        subTaskList.remove(index);
    }

    private void removeWatchers(Task task, String watcherId) {
        List<Watchers> watchersList = task.getTaskSecret().getWatchers();
        int index = 0;
        for (Watchers watchers : watchersList){
            String existingWatchers = watchers.getUserId().toLowerCase();
            String newWatchers = getUserId().toLowerCase();
            if (existingWatchers.equals(newWatchers)){
                index = watchersList.indexOf(watchers);
                break;
            }
        }
        watchersList.remove(index);
    }

    private List<Fields> getFieldList(String tenantId,String projectId) {
        return fieldsRepository.findByTenantIdAndProjectId(tenantId,projectId);
    }

    private List<Column> getColumnList(String tenantId,String projectId) {
        return columnRepository.findByTenantIdAndProjectId(tenantId,projectId);
    }
    private List<Goal> getGoalList(String tenantId,String projectId) {
        return goalRepository.findByTenantIdAndProjectId(tenantId,projectId);
    }
    private Fields saveFields(Fields fields, CreateFieldsInputDTO dto, String projectId, FieldsSecret fieldsSecret) {

       fieldsSecret.setName(dto.getName());
       fieldsSecret.setDescription(dto.getDescription());
       FieldType fieldType = FieldType.valueOf(dto.getType().toUpperCase());
        String fieldTypeObj = null;
        switch (fieldType){
            case NUMBER:
                fieldTypeObj = "NUMBER";
                break;
            case MONEY:
                fieldTypeObj = "MONEY";
                break;
            case DATE:
                fieldTypeObj = "DATE";
                break;
            case TEXT:
                fieldTypeObj = "TEXT";
                break;
                default:
        }
        fieldsSecret.setType(fieldTypeObj);
        fields.setFieldsSecret(fieldsSecret);
        fields.setCreatedTime(new Date());
        fields.setTenantId(getTenantId());
        fields.setProjectId(projectId);
        fields.setUserId(getUserId());
        fieldsRepository.save(fields);
        return fields;
    }

    private LogHours setAndSaveLogHourParameters(LogHours logHours, LogHourSecret logHourSecret, LogHourInputDTO dto, String projectId,String taskId) {
        logHourSecret.setLogNumber(dto.getLogNumber());
        logHourSecret.setLogType(dto.getLogType().toString());
        logHourSecret.setCreatedBy(getUserFullName());
        logHourSecret.setLogDate(dto.getLogDate());
        logHours.setLogHourSecret(logHourSecret);
        logHours.setCreatedDate(new Date());
        logHours.setTenantId(getTenantId());
        logHours.setTaskId(taskId);
        logHours.setProjectId(projectId);
        logHours.setUserId(getUserId());


        logHourRepository.save(logHours);
        return logHours;
    }

    private String totalLoggedHours(String taskId){
       List<LogHours> logs = logHourRepository.findByTenantIdAndTaskId(getTenantId(),taskId);
        double hours = 0;
        int calcHours = 0;
        double days = 0;
        int calcDays = 0;
        double weeks = 0;
        int calcWeeks = 0;
        String loggedHours = "";
        for(LogHours log : logs){
            switch (log.getLogHourSecret().getLogType()){
                case "h":
                    hours = hours + log.getLogHourSecret().getLogNumber();
                    break;
                case "d":
                    days = days + log.getLogHourSecret().getLogNumber();
                    break;
                case "w":
                    weeks = weeks + log.getLogHourSecret().getLogNumber();
                    break;
                default:
            }
        }

        double modhours = Math.floor(hours / 24);
        Double h = new Double(hours % 24);
        calcHours = h.intValue();
        if(modhours > 0){
            Double d = new Double(Math.floor(days + modhours));
            days = d.intValue();
        }
        double moddays = Math.floor(days / 7);
        Double d = new Double(days % 7);
        calcDays= d.intValue();
        if(moddays >= 0){
            Double w = new Double(Math.floor(weeks + moddays));
            calcWeeks = w.intValue();

        }
        if(calcWeeks > 0){
            loggedHours = calcWeeks + "w";
        }
        if(calcDays > 0){
            loggedHours = loggedHours + calcDays + "d";
        }
        if(calcHours > 0){
            loggedHours = loggedHours + calcHours + "h";
        }

        return loggedHours;

    }

}
