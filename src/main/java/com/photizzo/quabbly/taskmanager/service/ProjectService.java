/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.photizzo.quabbly.taskmanager.service;

import com.photizzo.quabbly.taskmanager.dto.input.*;
import com.photizzo.quabbly.taskmanager.dto.output.*;
import org.springframework.stereotype.Service;

/**
 *
 * @author osita
 */
@Service
public interface ProjectService {


    ProjectResponseDTO fetchProject(String projectId);



    ColumnResponseDTO addColumn(AddColumnInputDTO column , String projectId);

    ColumnResponseDTO  renameColumn(RenameColumnInputDTO column , String columnId,String projectId);

    ColumnListResponseDTO repositionColumns(String columnId,RepositionColumnInputDTO dto);


    TaskResponseDTO createTask(CreateTaskInputDTO task ,String projectId);

    TaskResponseDTO updateTask(UpdateTaskInputDTO task,String taskId,String projectId );

    TaskResponseDTO fetchTask(String taskId,String projectId);

    TaskListResponseDTO fetchTasks(String projectId);


    CommentListResponseDTO updateComment(UpdateCommentInputDTO dto , String commentUuid,String taskId,String projectId);

    TaskResponseDTO moveTask(MoveTaskInputDTO dto, String taskId,String projectId);

    BasicResponseDTO deleteAttachment(String taskId, String url,String projectId);

    BasicResponseDTO deleteComment(String taskId,String commentUuid,String projectId);

    BasicResponseDTO deleteColumn(String columnId,String projectId);

    ProjectResponseDTO updateProject(String projectId, UpdateProjectInputDTO dto);

    BasicResponseDTO deleteSubTask(String taskId, String subTaskUuid,String projectId);

    BasicResponseDTO deleteTask(String taskId,String projectId);

    BasicResponseDTO fetchProjects(String userId);
    SubTaskListResponseDTO updateSubTask(UpdateSubTaskInputDTO dto, String subTaskUuid, String taskId,String projectId);

    BasicResponseDTO deleteProject(String projectId);

    HideColumnResponseDTO markColumnAsHidden(MarkColumnInputDTO dto,String columnId,String projectId);

    UnHideColumnResponseDTO markColumnAsVisible(MarkColumnInputDTO dto,String columnId,String projectId);

    FieldsResponseDTO createFields(CreateFieldsInputDTO dto, String projectId);

    BasicResponseDTO createLogHour(LogHourInputDTO dto,String taskId,String projectId);

    FieldsResponseDTO updateFields(UpdateFieldInputDTO dto, String fieldId);



    BasicResponseDTO fetchLogHour(String taskId);
    GoalResponseDTO fetchGoal(String goalId);

    FieldListResponseDTO fetchFields(String projectId);

    GoalListResponseDTO fetchGoals(String projectId);

    GoalResponseDTO updateGoal(UpdateGoalInputDTO dto, String goalId,String projectId);
    ColumnListResponseDTO fetchColumns(String projectId);


    GoalResponseDTO createGoal(CreateGoalInputDTO dto, String projectId);
    BasicResponseDTO attachTaskToGoal(AttachTaskToGoalInputDTO dto,String goalId);

}
