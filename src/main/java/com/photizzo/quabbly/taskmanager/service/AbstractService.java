package com.photizzo.quabbly.taskmanager.service;

import com.google.common.collect.Lists;
import com.photizzo.quabbly.taskmanager.config.TokenProvider;
import com.photizzo.quabbly.taskmanager.dto.input.ApiFieldError;
import com.photizzo.quabbly.taskmanager.dto.input.UpdateTaskInputDTO;
import com.photizzo.quabbly.taskmanager.dto.remote.request.UserResponseDTO;
import com.photizzo.quabbly.taskmanager.model.Goal;
import com.photizzo.quabbly.taskmanager.model.Project;
import com.photizzo.quabbly.taskmanager.model.Task;
import com.photizzo.quabbly.taskmanager.model.User;
import com.photizzo.quabbly.taskmanager.model.extras.Assignee;
import com.photizzo.quabbly.taskmanager.model.extras.Model;
import com.photizzo.quabbly.taskmanager.model.extras.Watchers;
import com.photizzo.quabbly.taskmanager.repositories.ProjectRepository;
import com.photizzo.quabbly.taskmanager.util.TaskManagerUtil;
import com.photizzo.quabbly.taskmanager.util.UpdateType;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

abstract class AbstractService {

    private static final String GLOBAL_SERVICE_NAME = "TaskmanagerService";

    @Autowired
    private TokenProvider tokenProvider;


    @Autowired
    private Environment env;

    @Autowired
    protected ProjectRepository projectRepository;

    @Value("${notification.url}")
    private String notificationUrl;

    @Value("${users.url}")
    private String usersUrl;

    @Value("${update.description.body}")
    private String taskDescriptionBody;

    @Value("${update.comments.body}")
    private String taskCommentBody;

    @Value("${update.assignee.body}")
    private String taskAssigneeBody;

    @Value("${update.attachments.body}")
    private String taskAttachmentBody;

    @Value("${update.summary.body}")
    private String taskSummaryBody;

    void logError(String serviceName, String methodName, Exception ex) {
        ex.printStackTrace(); //fluentd, logstash
    }

    String getUserId() {
        return tokenProvider.getUserId();
    }

    String getTenantId() {
        return tokenProvider.getTenantId();
    }

    String getUsername() {
        return tokenProvider.getUsername();
    }

    String getCompanyName() {
        return tokenProvider.getTenantName();
    }

    String getUserFullName() {
        return tokenProvider.getUserFullName();
    }

    String hash(String value) throws NoSuchAlgorithmException {
        return TaskManagerUtil.md5Hash(value);
    }

    Project getProject(String tenantId, String projectId) {
        return projectRepository.findByTenantIdAndId(tenantId, projectId);
    }

    boolean projectNotFound(Project project) {
        return project == null;
    }

    boolean goalNotFound(Goal goal) {
        return goal == null;
    }

    boolean saveError(Model m) {
        return m.getId() == null;
    }

    protected void sendEmail(Task task, UpdateTaskInputDTO dto, UpdateType type) {

        try{
            List<Watchers> watchers = task.getTaskSecret().getWatchers();
            Assignee assignee = task.getTaskSecret().getAssignee();

            if (noEmailRecipientsAvailable(watchers, assignee)) {
                return;
            }

            String body;
            String subject;

            switch (type) {
                case DESCRIPTION:
                    subject = "Description updated for task - " + task.getTaskSecret().getSummary();
                    body = getDescriptionBody(task, dto);
                    sendUpdateEmail(body, subject, watchers, assignee);
                    break;
                case COMMENTS:
                    subject = "New comment for task - " + task.getTaskSecret().getSummary();
                    body = getCommentBody(task, dto);
                    sendUpdateEmail(body, subject, watchers, assignee);
                    break;
                case ASSIGNEE:
                    subject = "New assignee for task - " + task.getTaskSecret().getSummary();
                    body = getAssigneeBody(task, dto, assignee);
                    sendUpdateEmail(body, subject, watchers, assignee);
                    break;
                case ATTACHMENTS:
                    subject = "Attachment has been uploaded for task - " + task.getTaskSecret().getSummary();
                    body = getAttachmentBody(task, dto);
                    sendUpdateEmail(body, subject, watchers, assignee);
                    break;
                case SUMMARY:
                    subject = "Summary updated for task - " + task.getTaskSecret().getSummary();
                    body = getSummaryBody(task, dto);
                    sendUpdateEmail(body, subject, watchers, assignee);
                    break;
            }
        }catch (Exception ex){
            Logger.getLogger("email").info(ex.getMessage());
        }

    }

    private boolean noEmailRecipientsAvailable(List<Watchers> watchers, Assignee assignee) {
        return watchers == null && assignee == null;
    }

    private void sendUpdateEmail(String body, String subject, List<Watchers> watchers, Assignee assignee) {
        List<String> recipients = new ArrayList<>();


        if (assignee != null) {
            recipients.add(assignee.getUserEmail());
        } else {
            if (watchers != null && !watchers.isEmpty()) {
                watchers.stream().forEach(e -> {
                    recipients.add(e.getUserName());
                });  // there's a bug here, it should be .. getEmail but watchers doesnt contain email
            }
        }


        List<String> cc = new ArrayList<>();
        if (assignee != null && watchers != null && !watchers.isEmpty()) {
            watchers.stream().forEach(e -> {
                cc.add(e.getUserName());
            });  // there's a bug here, it should be .. getEmail but watchers doesnt contain email
        }

        sendEmail(recipients, cc, subject, body);
    }

    private String getDescriptionBody(Task task, UpdateTaskInputDTO dto) {
        return MessageFormat.format(taskDescriptionBody, getUserFullName(), task.getTaskSecret().getSummary(), task.getTaskSecret().getDescription(), dto.getDescription());
    }


    private String getCommentBody(Task task, UpdateTaskInputDTO dto) {
        return MessageFormat.format(taskCommentBody, getUserFullName(), dto.getComment());
    }

    private String getAssigneeBody(Task task, UpdateTaskInputDTO dto, Assignee assignee) {
        return MessageFormat.format(taskAssigneeBody, getUserFullName(), task.getTaskSecret().getSummary());
    }

    private String getAttachmentBody(Task task, UpdateTaskInputDTO dto) {
        return MessageFormat.format(taskAttachmentBody, getUserFullName(), task.getTaskSecret().getSummary());
    }

    private String getSummaryBody(Task task, UpdateTaskInputDTO dto) {
        return MessageFormat.format(taskSummaryBody, getUserFullName(), task.getTaskSecret().getSummary(), dto.getSummary());
    }

    private void sendEmail(List<String> recipients, List<String> cc, String subject, String body) {

        RestTemplate RestTemplate = getRestTemplate();
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<String, String>();
        requestBody.add("htmlContent", body);
        requestBody.add("subject", subject);
        requestBody.put("recipient",recipients);
        requestBody.put("cc", cc);

        HttpEntity emailEntity = new HttpEntity<>(requestBody, getHttpHeaders());

        ResponseEntity<String> emailResponseEntity =
                RestTemplate.postForEntity(notificationUrl, emailEntity, String.class);

        Logger.getLogger("email").info("Send notification endpoint returned: "+ emailResponseEntity.getStatusCodeValue());

    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + tokenProvider.getUserAuthorizationToken());
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        return headers;
    }

    private RestTemplate getRestTemplate() {
        CloseableHttpClient httpClient
                = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
        HttpComponentsClientHttpRequestFactory requestFactory
                = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);

        return new RestTemplate(requestFactory);
    }


    List<ApiFieldError> getValidationError(String field, String message, String rejectedValue) {
        return Lists.newArrayList(new ApiFieldError(field, message, rejectedValue));
    }

    protected User getUserById(String userId) {

        RestTemplate restTemplate = getRestTemplate();

        HttpEntity entity = new HttpEntity<>(getHttpHeaders());

        String url = usersUrl + "/users/id/"+userId;

        ResponseEntity<UserResponseDTO> response;

        try {
            response = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    entity,
                    UserResponseDTO.class);
        }catch (HttpClientErrorException e) {
            return null;
        }

        if(!response.getStatusCode().is2xxSuccessful()){
            return null;
        }

        return response.getBody().getUser();
    }


}
