package com.photizzo.quabbly.taskmanager.config;

import com.auth0.jwt.interfaces.Claim;
import com.photizzo.quabbly.taskmanager.model.Project;
import com.photizzo.quabbly.taskmanager.model.ProjectUser;
import com.photizzo.quabbly.taskmanager.model.User;
import com.photizzo.quabbly.taskmanager.repositories.ProjectRepository;
import com.photizzo.quabbly.taskmanager.repositories.ProjectUserRepository;
import com.photizzo.quabbly.taskmanager.util.TaskManagerUtil;
import org.springframework.core.env.Environment;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService{

    private String usersUrl;

    private ProjectRepository projectRepository;

    private ProjectUserRepository projectUserRepository;

    private TokenProvider tokenProvider;

    private HttpServletRequest request;

    UserDetailsService(Environment environment, ProjectRepository projectRepository, ProjectUserRepository projectUserRepository, TokenProvider tokenProvider, HttpServletRequest request) {
        this.usersUrl = environment.getProperty("users.url");
        this.projectRepository = projectRepository;
        this.projectUserRepository = projectUserRepository;
        this.tokenProvider = tokenProvider;
        this.request = request;
    }

    public UserDetails loadUserByUsername(String authToken) throws UsernameNotFoundException {
        Map<String, Claim> claims = tokenProvider.getClaims(authToken);
        String userId = claims.get("userId").asString();
        String tenantId = claims.get("organisationId").asString();

        User user = TaskManagerUtil.getUserByUuid(usersUrl, userId, authToken);
        if(user == null){
            return getAuthorityWithoutAccess(user);
        }

        if(user.getRoles().contains("ADMIN")) {
            return getAuthorityWithValidAccess(user);
        }

        String projectId = getProjectIdFromRequest(request);

        if(projectId == null){
            return getAuthorityWithoutAccess(user);
        }

        Project project = projectRepository.findByTenantIdAndId(tenantId, projectId);
        if(project == null){
            return getAuthorityWithoutAccess(user);
        }

        ProjectUser projectUser = projectUserRepository.findByTenantIdAndProjectIdAndUserId(tenantId, projectId, user.getId());
        if(projectUser == null){
            return getAuthorityWithoutAccess(user);
        }

        return getAuthorityWithValidAccess(user);
    }

    private String getProjectIdFromRequest(HttpServletRequest request) {
        String[] spl = request.getRequestURI().split("project/");
        if(spl.length <= 1){
            return null;
        }
        return spl[1].split("/")[0];
    }

    private UserDetails getAuthorityWithoutAccess(User user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        return new org.springframework.security.core.userdetails.User(user.getEmail(), "", authorities);
    }

    private UserDetails getAuthorityWithValidAccess(User user) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority("PROJECT"));
        authorities.add(new SimpleGrantedAuthority("ADMIN"));
        authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));

        return new org.springframework.security.core.userdetails.User(user.getEmail(), "", authorities);
    }





}
