package com.photizzo.quabbly.taskmanager.config;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.io.Serializable;
import java.util.Iterator;

public class CustomPermissionEvaluator implements PermissionEvaluator {

    private static String PROJECT_ROLE = "PROJECT";


    @Override
    public boolean hasPermission(Authentication auth, Object targetType, Object projectId) {
        if ((auth == null)){
            return false;
        }
        return hasPrivilege(auth);
    }

    @Override
    public boolean hasPermission(
            Authentication auth, Serializable targetId, String targetType, Object permission) {
        if ((auth == null)) {
            return false;
        }
        return hasPrivilege(auth);
    }

    private boolean hasPrivilege(Authentication auth) {
        User user = (User) auth.getPrincipal();
        Iterator<GrantedAuthority> authIterator = user.getAuthorities().iterator();
        while(authIterator.hasNext()){
            SimpleGrantedAuthority simpleGrantedAuthority = (SimpleGrantedAuthority) authIterator.next();
            if(simpleGrantedAuthority.toString().equals(PROJECT_ROLE)){
                return true;
            }

        }
        return false;
    }


}

