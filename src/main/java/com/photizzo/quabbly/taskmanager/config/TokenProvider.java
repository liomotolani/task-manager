package com.photizzo.quabbly.taskmanager.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;
import java.util.stream.Collectors;

import static com.photizzo.quabbly.taskmanager.dto.input.Constants.HEADER_STRING;
import static com.photizzo.quabbly.taskmanager.dto.input.Constants.TOKEN_PREFIX;


@Component
public class TokenProvider implements Serializable {

    @Value("${jwt.private.key}")
    private String jwtPrivateKeyPem;
    @Value("${jwt.public.key}")
    private String jwtPublicKey;
    @Value("${jwt.key.issuer}")
    private String jwtKeyIssuer;

    private String userId;
    private String organisationId;
    private String username;
    private String companyName;
    private String firstName;
    private String lastName;


    @Autowired
    private HttpServletRequest request;

    void setDetails(String userId, String organisationId, String username,String companyName,String firstName,String lastName) {
        this.userId = userId;
        this.organisationId = organisationId;
        this.username = username;
        this.companyName = companyName;
        this.firstName = firstName;
        this.lastName = lastName;

    }

    public String getUserId() {
        return this.userId;
    }

    public String getTenantId() {
        return this.organisationId;
    }
    public String getTenantName(){
        return this.companyName;
    }

    public String getUserFullName(){
        return this.firstName + " " + this.lastName ;
    }

    public String getUsername(){
        return this.username;
    }

    String getUsernameFromToken(String token) {
        return JWT.decode(token).getSubject();
    }

    private Date getExpirationDateFromToken(String token) {
        return JWT.decode(token).getExpiresAt();
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }


    private Algorithm getAlgorithm() throws NoSuchAlgorithmException, InvalidKeySpecException {
        jwtPrivateKeyPem = jwtPrivateKeyPem.replaceAll("\\n", "").replace("-----BEGIN PRIVATE KEY-----", "").replace("-----END PRIVATE KEY-----", "");
        jwtPublicKey = jwtPublicKey.replaceAll("\\n", "").replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");

        KeyFactory kf = KeyFactory.getInstance("RSA");

        PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(jwtPrivateKeyPem));
        RSAPrivateKey privateKey = (RSAPrivateKey) kf.generatePrivate(keySpecPKCS8);

        X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(jwtPublicKey));
        RSAPublicKey publicKey = (RSAPublicKey) kf.generatePublic(keySpecX509);

        return Algorithm.RSA256(publicKey, privateKey);
    }

    public Boolean validateToken(String token) throws InvalidKeySpecException, NoSuchAlgorithmException {
        JWTVerifier verifier = JWT.require(getAlgorithm())
                .withIssuer(jwtKeyIssuer)
                .build();
        DecodedJWT jwt = verifier.verify(token);

        return (jwt != null && !isTokenExpired(token));
    }

    UsernamePasswordAuthenticationToken getAuthentication(final String token, final UserDetails userDetails) {

        Claim claims = JWT.decode(token).getClaim("subject");

        final Collection<? extends GrantedAuthority> authorities =
                Arrays.stream(claims.toString().split(","))
                        .map(SimpleGrantedAuthority::new)
                        .collect(Collectors.toList());

        return new UsernamePasswordAuthenticationToken(userDetails, "", authorities);
    }

    public Map<String, Claim> getClaims(String authToken) {
        return JWT.decode(authToken).getClaims();
    }

    public String getUserAuthorizationToken() {
        String header = request.getHeader(HEADER_STRING);
         String authToken = header.replace(TOKEN_PREFIX, "");
        return authToken;
    }
}
