package com.photizzo.quabbly.taskmanager.config;

import com.auth0.jwt.interfaces.Claim;
import com.photizzo.quabbly.taskmanager.repositories.ProjectRepository;
import com.photizzo.quabbly.taskmanager.repositories.ProjectUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.photizzo.quabbly.taskmanager.dto.input.Constants.HEADER_STRING;
import static com.photizzo.quabbly.taskmanager.dto.input.Constants.TOKEN_PREFIX;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private TokenProvider tokenProvider;

    @Autowired
    private ProjectUserRepository projectUserRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private Environment environment;

    private Set<SimpleGrantedAuthority> getAuthority(Map<String, Claim> claims, String token) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();

        authorities.add(new SimpleGrantedAuthority("USERID@" + claims.get("userId").asString()));
        authorities.add(new SimpleGrantedAuthority("TENANTID@" + claims.get("organisationId").asString()));
        authorities.add(new SimpleGrantedAuthority("TOKEN@" + token));
        Claim roles = claims.get("roles");
        for (Object role : roles.asList(String.class)) {
            authorities.add(new SimpleGrantedAuthority("ROLE@" + role));
        }
        return authorities;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        String header = req.getHeader(HEADER_STRING);
        String username = null;
        String authToken = null;
        if (header != null && header.startsWith(TOKEN_PREFIX)) {
            authToken = header.replace(TOKEN_PREFIX, "");
            username = tokenProvider.getUsernameFromToken(authToken);
        }

        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            Map<String, Claim> claims = tokenProvider.getClaims(authToken);
            try {
                if (tokenProvider.validateToken(authToken)) {
                    UserDetailsService service = new UserDetailsService(environment, projectRepository, projectUserRepository, tokenProvider, req);
                    UserDetails userDetails = service.loadUserByUsername(authToken);

                    UsernamePasswordAuthenticationToken authentication = tokenProvider.getAuthentication(authToken, userDetails);
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));


                    String userId = claims.get("userId").asString();
                    Claim organisations = claims.get("organisationId");
                    Claim organisationName = claims.get("iss");
                    Claim firstName = claims.get("firstname");
                    Claim lastName = claims.get("lastname");

                    tokenProvider.setDetails(userId, organisations.asString(), username,organisationName.asString(),firstName.asString(),lastName.asString());



                    SecurityContextHolder.getContext().setAuthentication(authentication);

                }
            } catch (InvalidKeySpecException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }

        chain.doFilter(req, res);
    }

}