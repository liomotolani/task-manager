package com.photizzo.quabbly.taskmanager.validators.interfaces;

import com.photizzo.quabbly.taskmanager.validators.DatetimeValidator;
import com.photizzo.quabbly.taskmanager.validators.UniqueFieldValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = {DatetimeValidator.class})
@Retention(RetentionPolicy.RUNTIME)

@Target({
        ElementType.ANNOTATION_TYPE,
        ElementType.FIELD
})

public @interface Datetime {
    String message() default "{datetime}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    boolean isUpdate() default false;

    String columnName();

    Class<?> className();

}
