package com.photizzo.quabbly.taskmanager.validators.interfaces;

import com.photizzo.quabbly.taskmanager.validators.ValidObjectValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = {ValidObjectValidator.class})
@Retention(RetentionPolicy.RUNTIME)

@Target({
        ElementType.ANNOTATION_TYPE,
        ElementType.FIELD
})

public @interface ValidObject {
    String message() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String columnName();

    Class<?> className();

}
