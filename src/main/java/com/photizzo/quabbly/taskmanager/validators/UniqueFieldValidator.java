package com.photizzo.quabbly.taskmanager.validators;

import com.photizzo.quabbly.taskmanager.config.TokenProvider;
import com.photizzo.quabbly.taskmanager.util.TaskManagerUtil;
import com.photizzo.quabbly.taskmanager.validators.interfaces.UniqueField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.security.NoSuchAlgorithmException;

@Component
public class UniqueFieldValidator implements ConstraintValidator<UniqueField, Object> {
    private Class className;
    private String columnName;
    private boolean isUpdate;
    private boolean hashValue;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private TokenProvider tokenProvider;

    @Override
    public void initialize(final UniqueField annotation) {
        className = annotation.className();
        columnName = annotation.columnName();
        isUpdate = annotation.isUpdate();
        hashValue = annotation.hashValue();
    }

    @Override
    public boolean isValid(Object value, final ConstraintValidatorContext context) {

        if (value == null) {
            return true;
        }

        if (hashValue) {
            try {
                value = TaskManagerUtil.md5Hash(value.toString());
            } catch (NoSuchAlgorithmException e) {
                return false;
            }
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(columnName).is(value).and("tenantId").is(tokenProvider.getTenantId()));

        return mongoTemplate.findOne(query, className) == null;

    }

}