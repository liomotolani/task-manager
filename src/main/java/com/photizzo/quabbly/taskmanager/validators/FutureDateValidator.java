package com.photizzo.quabbly.taskmanager.validators;


import com.google.common.base.Strings;
import com.photizzo.quabbly.taskmanager.validators.interfaces.FutureDate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FutureDateValidator implements ConstraintValidator<FutureDate, String> {

    private boolean isOptional;

    private static boolean isFutureDate(String format, String value) {
        try {
            Date d = new SimpleDateFormat(format).parse(value);

            return d.after(new Date());
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public void initialize(FutureDate futureDate) {
        this.isOptional = futureDate.optional();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
        if (isOptional && Strings.isNullOrEmpty(value)) {
            return true;
        }
        return isFutureDate("yyyy-MM-dd HH:mm:ss", value);
    }
}