package com.photizzo.quabbly.taskmanager.validators;

import com.photizzo.quabbly.taskmanager.validators.interfaces.ValidateDateRange;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Date;

    @Component
    public class DateRangeValidator implements ConstraintValidator<ValidateDateRange, Object> {
        private String dateOneFieldName;
        private String dateTwoFieldName;

        @Override
        public void initialize(final ValidateDateRange annotation) {
            dateOneFieldName = annotation.dateOneFieldName();
            dateTwoFieldName = annotation.dateTwoFieldName();

        }

        @Override
        public boolean isValid(final Object value, final ConstraintValidatorContext context) {

            BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(value);

            Date valueOfThis = (Date) wrapper.getPropertyValue(dateOneFieldName);
            Date valueOfThat = (Date) wrapper.getPropertyValue(dateTwoFieldName);

            boolean isAfter = valueOfThat.after(valueOfThis);


            if (!isAfter) {


                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
                        .addPropertyNode(dateTwoFieldName)
                        .addConstraintViolation();

                return false;

            }

            return true;

        }

}
