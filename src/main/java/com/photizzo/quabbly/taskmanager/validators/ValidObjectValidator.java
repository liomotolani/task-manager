package com.photizzo.quabbly.taskmanager.validators;

import com.photizzo.quabbly.taskmanager.validators.interfaces.ValidObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class ValidObjectValidator implements ConstraintValidator<ValidObject, Object> {
    private Class className;
    private String columnName;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void initialize(final ValidObject annotation) {
        className = annotation.className();
        columnName = annotation.columnName();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {

        if (value == null) {
            return false;
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(columnName).is(value));

        Object o = mongoTemplate.findOne(query, className);

        return o != null;
    }

}