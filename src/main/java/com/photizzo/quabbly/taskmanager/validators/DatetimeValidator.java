package com.photizzo.quabbly.taskmanager.validators;

import com.photizzo.quabbly.taskmanager.validators.interfaces.Datetime;
import com.photizzo.quabbly.taskmanager.validators.interfaces.UniqueField;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class DatetimeValidator implements ConstraintValidator<Datetime, String> {
  
    @Override
    public void initialize(Datetime datetime) { //final Datetime annotation
      
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {

        if (value == null) {
            return false;
        }
           
        try {
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(value);
            return true;
        } catch (ParseException ex) {
            return false;
        }
        
    }

}