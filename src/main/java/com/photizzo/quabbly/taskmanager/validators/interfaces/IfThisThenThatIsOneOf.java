package com.photizzo.quabbly.taskmanager.validators.interfaces;

import com.photizzo.quabbly.taskmanager.validators.DependentFieldOneOfValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = {DependentFieldOneOfValidator.class})
@Retention(RUNTIME)
@Target({TYPE, ANNOTATION_TYPE})
public @interface IfThisThenThatIsOneOf {
    String message() default "please choose a different value. this one already exists";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    boolean isUpdate() default false;

    String thisName();

    String operator();

    String thisState();

    String thatName();

    String[] thatList();

    @Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
    @interface List {
        IfThisThenThatIsOneOf[] value();
    }


}
