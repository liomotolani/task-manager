package com.photizzo.quabbly.taskmanager.validators;

import com.photizzo.quabbly.taskmanager.validators.interfaces.IfThisThenThatIsOneOf;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

@Component
public class DependentFieldOneOfValidator implements ConstraintValidator<IfThisThenThatIsOneOf, Object> {
    private String thisName;
    private String operator;
    private String thisState;
    private String thatName;
    private String[] thatList;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void initialize(final IfThisThenThatIsOneOf annotation) {
        thisName = annotation.thisName();
        thisState = annotation.thisState();
        thatName = annotation.thatName();
        thatList = annotation.thatList();
        operator = annotation.operator();
    }

    @Override
    public boolean isValid(final Object value, final ConstraintValidatorContext context) {

        BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(value);

        Object valueOfThis = wrapper.getPropertyValue(thisName);
        Object valueOfThat = wrapper.getPropertyValue(thatName);

        boolean thisTrue = thisIsTrue(valueOfThis, operator, thisState);
        boolean thatOneOf = thatIsOneOf(valueOfThat, thatList);

        if (thisTrue) {

            if (!thatOneOf) {
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
                        .addPropertyNode(thatName)
                        .addConstraintViolation();

                return false;
            }
        }

        return true;

    }

    private boolean thatIsOneOf(Object valueOfThat, String[] thatList) {
        return Arrays.asList(thatList).contains(String.valueOf(valueOfThat));
    }

    //easily support more operators in the future like > , < etc
    //we use string "null" because annotation cant accept null.
    private boolean thisIsTrue(Object value, String operator, String thisState) {
        if (operator.equals("=")) {
            if (thisState.equals("null")) {
                return value == null;
            }
            return String.valueOf(value).equals(thisState);
        } else {
            if (thisState.equals("null")) {
                return value != null;
            }
            return !String.valueOf(value).equals(thisState);
        }
    }

}