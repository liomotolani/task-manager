package com.photizzo.quabbly.taskmanager.validators;

import com.photizzo.quabbly.taskmanager.validators.interfaces.ValidateEnumList;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class EnumListValidator implements ConstraintValidator<ValidateEnumList, List<String>> {

    private ValidateEnumList annotation;

    @Override
    public void initialize(ValidateEnumList  annotation)
    {
        this.annotation = annotation;
    }

    @Override
    public boolean isValid(List<String> valueListForValidation, ConstraintValidatorContext constraintValidatorContext)
    {
        boolean result = false;

        Object[] enumValues = this.annotation.enumClass().getEnumConstants();

        if(enumValues != null)
        {
            for(Object enumValue:enumValues)
            {

                for(String valueForValidation : valueListForValidation){
                    if(valueForValidation.equals(enumValue.toString()) || (this.annotation.ignoreCase()) && valueForValidation.equalsIgnoreCase(enumValue.toString())){
                        result = true;
                        break;
                    }
                }

            }
        }

        return result;
    }
}
