package com.photizzo.quabbly.taskmanager.util;

import com.photizzo.quabbly.taskmanager.dto.remote.request.UserResponseDTO;
import com.photizzo.quabbly.taskmanager.model.User;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class TaskManagerUtil {

    public static String md5Hash(String value) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(value.getBytes());
        byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest).toUpperCase();
    }

    public static User getUserByUuid(String usersUrl, String userId, String token) {

        RestTemplate restTemplate = getRestTemplate();

        HttpEntity entity = new HttpEntity<>(getHttpHeaders(token));

        String url = usersUrl + "/users/"+userId;

        ResponseEntity<UserResponseDTO> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                entity,
                UserResponseDTO.class);

        if(!response.getStatusCode().is2xxSuccessful()){
            return null;
        }

        return response.getBody().getUser();
    }

    private static HttpHeaders getHttpHeaders(String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + token);
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        return headers;
    }

    private static RestTemplate getRestTemplate() {
        CloseableHttpClient httpClient
                = HttpClients.custom().setSSLHostnameVerifier(new NoopHostnameVerifier()).build();
        HttpComponentsClientHttpRequestFactory requestFactory
                = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);

        return new RestTemplate(requestFactory);
    }
}
