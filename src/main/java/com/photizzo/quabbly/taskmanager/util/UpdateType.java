package com.photizzo.quabbly.taskmanager.util;

public enum UpdateType {
    DESCRIPTION,
    SUMMARY,
    COMMENTS,
    ATTACHMENTS,
    WATCHERS,
    ASSIGNEE
}
