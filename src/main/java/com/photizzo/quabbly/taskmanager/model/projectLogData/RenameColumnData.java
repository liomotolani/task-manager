package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class RenameColumnData {
    private String createdBy;

    private String oldColumnName;

    private String columnName;

    private Date createdTime;

    private String projectName;

    private String action;

    public RenameColumnData(String createdBy, String oldColumnName, String columnName, String projectName, Date createdTime, String action) {
        this.createdBy = createdBy;
        this.oldColumnName = oldColumnName;
        this.columnName = columnName;
        this.projectName = projectName;
        this.createdTime = createdTime;
        this.action = action;
    }

    @Override
    public String toString(){return createdBy + " " + action + " " + oldColumnName + " " + " to " + columnName + " " + " in "+ projectName +" on " + createdTime;}
}
