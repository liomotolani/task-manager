package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class AddColumnToProjectData {
    private String addedBy;

    private Date addedDate;

    private String action;

    private String columnName;

    private String projectName;

    public AddColumnToProjectData(String addedBy, Date addedDate, String action, String columnName, String projectName) {
        this.addedBy = addedBy;
        this.addedDate = addedDate;
        this.action = action;
        this.columnName = columnName;
        this.projectName = projectName;
    }

    @Override
    public String toString() {
        return addedBy + " " + action + " column " + columnName + " to project " + projectName + " on " + addedDate;
    }
}
