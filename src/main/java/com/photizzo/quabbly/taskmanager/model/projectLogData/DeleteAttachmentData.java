package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class DeleteAttachmentData {

    private String deletedBy;

    private String action;

    private String filename;

    private String taskName;

    private String projectName;

    private Date deletedDate;

    public DeleteAttachmentData(String deletedBy, String action, String filename, String taskName, String projectName, Date deletedDate) {
        this.deletedBy = deletedBy;
        this.action = action;
        this.filename = filename;
        this.taskName = taskName;
        this.projectName = projectName;
        this.deletedDate = deletedDate;
    }

    @Override
    public String toString(){return deletedBy + " " + action + " " + filename + " " + taskName + " " + projectName + " " +deletedDate;}
}
