package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class RepositionColumnData {
    private String repositionedBy;

    private Date repositionTime;

    private String projectName;

    private String action;

    public RepositionColumnData(String repositionedBy, Date repositionTime, String projectName, String action) {
        this.repositionedBy = repositionedBy;
        this.repositionTime = repositionTime;
        this.projectName = projectName;
        this.action = action;
    }

    @Override
    public String toString() {
        return repositionedBy + " " + action + " columns in " + projectName + " on " + repositionTime ;
    }
}
