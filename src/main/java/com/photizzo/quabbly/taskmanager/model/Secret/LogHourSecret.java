package com.photizzo.quabbly.taskmanager.model.Secret;

import java.util.Date;

public class LogHourSecret {
    private int logNumber;

    private String logType;

    private Date logDate;

    private String createdBy;

    public LogHourSecret(){}

    public LogHourSecret(int logNumber,String logType,Date logDate,String createdBy){
        this.logNumber = logNumber;
        this.logType = logType;
        this.logDate = logDate;
        this.createdBy = createdBy;
    }

    public int getLogNumber() { return logNumber; }

    public void setLogNumber(int logNumber) {this.logNumber = logNumber;}

    public String getLogType() {return logType;}

    public void setLogType(String logType) {this.logType = logType;}

    public Date getLogDate() {return logDate;}

    public void setLogDate(Date logDate) {this.logDate = logDate;}


    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
