package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class CreateProjectData {
    private String createdBy;

    private Date createdTime;

    private String projectName;

    private String action;

    public CreateProjectData(String createdBy, Date createdTime, String projectName, String action) {
        this.createdBy = createdBy;
        this.createdTime = createdTime;
        this.projectName = projectName;
        this.action = action;
    }

    @Override
    public String toString() {
        return createdBy + " " + action + " " + projectName + " project on " + createdTime;
    }
}
