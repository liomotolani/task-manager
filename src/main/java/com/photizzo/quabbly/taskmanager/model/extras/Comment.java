package com.photizzo.quabbly.taskmanager.model.extras;

import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class Comment {

    @Id
    private String id;
    private String uuid;
    private Date createdAt;
    private Date lastUpdatedTime;
    private String comment;
    private String userId;
    private String userEmail;
    private String userName;



    public Comment() {
    }

    public Comment(String comment, String commentedByUserId, String commentedByName,String commentedByEmail) {
        this.comment = comment;
        this.userId = commentedByUserId;
        this.userEmail = commentedByEmail;
        this.userName = commentedByName;
        this.uuid = UUID.randomUUID().toString();
        this.createdAt = new Date();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(Date lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return uuid.equals(comment.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
