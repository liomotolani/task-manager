package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class DeleteCommentData {

    private String deletedBy;

    private String action;


    private String taskName;

    private String projectName;

    private Date deletedDate;

    public DeleteCommentData(String deletedBy, String action, String taskName, String projectName, Date deletedDate) {
        this.deletedBy = deletedBy;
        this.action = action;
        this.taskName = taskName;
        this.projectName = projectName;
        this.deletedDate = deletedDate;
    }

    @Override
    public String toString(){ return deletedBy + " " +  action  + " on " + taskName + " "+ " in " + projectName + " " + deletedDate;}
}
