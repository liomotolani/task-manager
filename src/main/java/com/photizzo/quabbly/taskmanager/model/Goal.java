package com.photizzo.quabbly.taskmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.photizzo.quabbly.taskmanager.model.Secret.GoalSecret;
import com.photizzo.quabbly.taskmanager.model.extras.Model;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class Goal extends Model {

    @NotNull
    @Indexed
    private GoalSecret goalSecret;

    @JsonIgnore
    private String taskId;

    @JsonIgnore
    private String nameHash;

    @JsonIgnore
    @Indexed
    private String tenantId;

    @JsonIgnore
    @Indexed
    private String userId;

    @Indexed
    private String projectId;

    @Indexed
    private Date createdTime;

    @Indexed
    private Date lastModifiedTime;

    public Goal(){

    }


    public static Goal createGoalWithDefaults(String goalName,Date startDate,Date stopDate, String nameHash,String description,String projectId,String tenantId, String userId){
        Goal goalObj = new Goal();
        goalObj.setGoalSecret(new GoalSecret(goalName,startDate,stopDate,description));
        goalObj.setNameHash(nameHash);
        goalObj.setTenantId(tenantId);
        goalObj.setProjectId(projectId);
        goalObj.setUserId(userId);
        goalObj.setCreatedTime(new Date());
        return goalObj;
    }



    public GoalSecret getGoalSecret() { return goalSecret; }

    public void setGoalSecret(GoalSecret goalSecret) {this.goalSecret = goalSecret;}

    public String getNameHash() {
        return nameHash;
    }

    public void setNameHash(String nameHash) {
        this.nameHash = nameHash;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getTenantId() {return tenantId; }

    public void setTenantId(String tenantId) {this.tenantId = tenantId;}

    public String getUserId() {return userId;}

    public void setUserId(String userId) {this.userId = userId;}

    public Date getCreatedTime() {return createdTime;}

    public void setCreatedTime(Date createdTime) {this.createdTime = createdTime;}

    public Date getLastModifiedTime() {return lastModifiedTime;}

    public void setLastModifiedTime(Date lastModifiedTime) {this.lastModifiedTime = lastModifiedTime;}

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
}
