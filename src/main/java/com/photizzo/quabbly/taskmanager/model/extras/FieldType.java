package com.photizzo.quabbly.taskmanager.model.extras;

public enum FieldType {

    NUMBER,
    MONEY,
    DATE,
    TEXT
}
