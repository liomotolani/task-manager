package com.photizzo.quabbly.taskmanager.model.extras;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class SubTask {

    private String summary;

    private Date createdTime;

    private String uuid;

    private String userId;
    private String userName;

    private Date lastUpdatedTime;


    public SubTask(String summary, String userId, String userName) {
        this.summary = summary;
        this.createdTime = new Date();
        this.uuid = UUID.randomUUID().toString();
        this.userId = userId;
        this.userName = userName;
    }

    public SubTask() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Date getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(Date lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubTask subTask = (SubTask) o;
        return uuid.equals(subTask.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
