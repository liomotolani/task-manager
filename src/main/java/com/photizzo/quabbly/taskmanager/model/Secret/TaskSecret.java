package com.photizzo.quabbly.taskmanager.model.Secret;

import com.photizzo.quabbly.taskmanager.model.extras.*;

import java.util.List;

public class TaskSecret {
    private String summary;


    private String description;

    private List<Comment> comments;

    private List<Attachments> attachments;

    private List<SubTask> subTasks;

    private List<Watchers> watchers;

    private Assignee assignee;

    private String priority;

    private List<String> updatedFields;

    private List<Object> taskLog;

    private String goalId;




    public  TaskSecret(){

    }

    public TaskSecret(String summary, String description, List<Comment> comments, List<Attachments> attachments, List<SubTask> subTasks, List<Watchers> watchers) {
        this.summary = summary;
        this.description = description;
        this.comments = comments;
        this.attachments = attachments;
        this.subTasks = subTasks;
        this.watchers = watchers;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Attachments> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachments> attachments) {
        this.attachments = attachments;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public List<SubTask> getSubTasks() {
        return subTasks;
    }

    public void setSubTasks(List<SubTask> subTasks) {
        this.subTasks = subTasks;
    }

    public List<Watchers> getWatchers() {
        return watchers;
    }

    public void setWatchers(List<Watchers> watchers) {
        this.watchers = watchers;
    }

    public Assignee getAssignee() {
        return assignee;
    }

    public void setAssignee(Assignee assignee) {
        this.assignee = assignee;
    }

    public String getGoalId() {
        return goalId;
    }

    public void setGoalId(String goalId) {
        this.goalId = goalId;
    }

    public List<String> getUpdatedFields() {
        return updatedFields;
    }

    public void setUpdatedFields(List<String> updatedFields) {
        this.updatedFields = updatedFields;
    }

    public List<Object> getTaskLog() {
        return taskLog;
    }

    public void setTaskLog(List<Object> taskLog) {
        this.taskLog = taskLog;
    }
}