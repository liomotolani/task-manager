package com.photizzo.quabbly.taskmanager.model.extras;

public class RoleType {

    public static final String ADMIN = "ADMIN";
    public static final String CREATE_PROJECT = "CREATE_PROJECT";
}
