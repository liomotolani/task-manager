package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)

public class AddSubTaskData {

    private String addedBy;

    private String action;

    private Date addedDate;

    private String taskName;

    private String projectName;

    private String subtaskSummary;

    public AddSubTaskData(String addedBy, String action, String subtaskSummary, String taskName, String projectName,Date addedDate) {
        this.addedBy = addedBy;
        this.action = action;
        this.addedDate = addedDate;
        this.taskName = taskName;
        this.projectName = projectName;
        this.subtaskSummary = subtaskSummary;
    }
    @Override
    public String toString(){return addedBy + " " + action + " " + subtaskSummary + " " +"in"+ taskName + " " + " for "+ projectName +" "+ " on " +addedDate;}
}
