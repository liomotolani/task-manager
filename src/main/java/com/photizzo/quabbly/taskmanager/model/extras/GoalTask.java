package com.photizzo.quabbly.taskmanager.model.extras;

public class GoalTask {

    private String taskSummary;

    private String description;

    public String getTaskSummary() {return taskSummary;}

    public void setTaskSummary(String taskSummary) {this.taskSummary = taskSummary;}

    public String getDescription() {return description;}

    public void setDescription(String description) { this.description = description;}
}
