package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.io.Serializable;
import java.util.Date;
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class UpdateProjectData implements Serializable {
    private String createdBy;

    private Date createdTime;

    private String oldProjectName;

    private String newProjectName;

    private String action;

    public UpdateProjectData(String createdBy, Date createdTime, String oldProjectName, String newProjectName, String action) {
        this.createdBy = createdBy;
        this.createdTime = createdTime;
        this.oldProjectName = oldProjectName;
        this.newProjectName = newProjectName;
        this.action = action;
    }

    @Override
    public String toString() {
        return createdBy + " " + action + " project," + oldProjectName +" to " +newProjectName+ " on " + createdTime;
    }
}
