package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class UploadAttachmentData {
    private String uploadedBy;

    private String action;

    private Date uploadedDate;

    private String taskName;

    private String projectName;

    public UploadAttachmentData(String uploadedBy, String action,Date uploadedDate, String taskName, String projectName) {
        this.uploadedBy = uploadedBy;
        this.action = action;
        this.uploadedDate = uploadedDate;
        this.taskName = taskName;
        this.projectName = projectName;
    }

    @Override
    public String toString(){return uploadedBy+ " " + action + " " +  "a file in " +  taskName + " " +  projectName  + " " +" on " + uploadedDate;}
}
