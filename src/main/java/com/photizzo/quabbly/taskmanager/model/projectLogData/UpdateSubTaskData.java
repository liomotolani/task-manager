package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class UpdateSubTaskData {

    private String updatedBy;

    private String action;

    private String subTaskSummary;

    private String taskName;

    private String projectName;

    private Date updatedDate;


    public UpdateSubTaskData(String updatedBy,  String action, String subTaskSummary, String taskName, String projectName,Date updatedDate) {
        this.updatedBy = updatedBy;
        this.updatedDate = updatedDate;
        this.subTaskSummary = subTaskSummary;
        this.taskName = taskName;
        this.projectName = projectName;
        this.action = action;
    }

    @Override
    public String toString(){return updatedBy + " " + action + subTaskSummary + " to " + " on  "
            + taskName + " in " + projectName + " " + updatedDate;}
}
