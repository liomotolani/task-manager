package com.photizzo.quabbly.taskmanager.model.Secret;

import com.photizzo.quabbly.taskmanager.model.Task;
import org.springframework.data.mongodb.core.index.Indexed;

import java.util.Date;
import java.util.List;

public class GoalSecret {

    @Indexed
    private String name;
    private Date startDate;
    private Date stopDate;
    private String description;
    private List<Task> taskList;


    public GoalSecret() {
    }

    public GoalSecret(String name, Date startDate, Date stopDate, String description) {
        this.name = name;
        this.startDate = startDate;
        this.stopDate = stopDate;
        this.description = description;

    }


    public void updateFieldsByDefault(String name, Date startDate, Date stopDate, String description) {
        this.name = name;
        this.startDate = startDate;
        this.stopDate = stopDate;
        this.description = description;
    }


    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public Date getStartDate() {return startDate;}

    public void setStartDate(Date startDate) {this.startDate = startDate;}

    public Date getStopDate() {return stopDate;}

    public void setStopDate(Date stopDate) {this.stopDate = stopDate;}

    public String getDescription() {return description;}

    public void setDescription(String description) {this.description = description;}

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }
}
