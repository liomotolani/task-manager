package com.photizzo.quabbly.taskmanager.model.Secret;



import java.util.List;

public class ColumnSecret {

    private String name;

    private List <String> usersId;

    public ColumnSecret() {
    }

    public ColumnSecret(String name) {
        this.name = name;
    }

    public String getName(){ return name; }

    public void setName(String name) {this.name = name;}

    public List <String> getUsersId() {
        return usersId;
    }

    public void setUsersId(List <String> usersId) {
        this.usersId = usersId;
    }

}
