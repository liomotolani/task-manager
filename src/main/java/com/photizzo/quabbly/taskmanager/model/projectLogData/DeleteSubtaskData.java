package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class DeleteSubtaskData {
    private String createdBy;

    private Date createdTime;

    private String taskName;

    private String projectName;

    private String action;

    public DeleteSubtaskData(String createdBy, Date createdTime, String taskName, String projectName, String action) {
        this.createdBy = createdBy;
        this.createdTime = createdTime;
        this.taskName = taskName;
        this.projectName = projectName;
        this.action = action;
    }

    @Override
    public String toString() {
        return  createdBy + " " + action + " on task " + taskName + " in project" + projectName + " on " + createdTime;
    }


}
