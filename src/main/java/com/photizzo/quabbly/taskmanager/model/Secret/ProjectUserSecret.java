package com.photizzo.quabbly.taskmanager.model.Secret;

import com.photizzo.quabbly.taskmanager.model.Role;

public class ProjectUserSecret {

    private String fullname;
    private String email;

    private Role role;

    public ProjectUserSecret() {
    }

    public ProjectUserSecret(String fullname, String email,Role role) {
        this.fullname = fullname;
        this.email = email;
        this.role = role;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
