package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class DeleteTaskData {
    private String deletedBy;

    private Date deleteTime;

    private String taskName;

    private String projectName;

    private String action;

    public DeleteTaskData(String deletedBy, Date deleteTime, String taskName, String projectName, String action) {
        this.deletedBy = deletedBy;
        this.deleteTime = deleteTime;
        this.taskName = taskName;
        this.projectName = projectName;
        this.action = action;
    }

    @Override
    public String toString() {
        return deletedBy + " " + action + "  task " + taskName + " from project " + projectName + " on " + deleteTime;
    }
}
