/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.photizzo.quabbly.taskmanager.model;

import com.bol.secure.Encrypted;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.photizzo.quabbly.taskmanager.model.Secret.ColumnSecret;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;
import java.util.List;

@Document
public class Column {
    @Id
    private String id;

    @Field
    @Encrypted
    private ColumnSecret columnSecret;


    @Indexed
    @JsonIgnore
    private String nameHash;

    private int position;


    @Indexed
    private String projectId;

    private Date createdTime;

    private Date lastUpdatedTime;

    @JsonIgnore
    private String uuid;

    @Indexed
    @JsonIgnore
    private String tenantId;

    private boolean visibility;

    public static Column createColumnWithDefaults(String name, String nameHash, int position, String projectId, String userId, String tenantId) {
        Column columnObj = new Column();
        columnObj.setNameHash(nameHash);
        columnObj.setPosition(position);
        columnObj.setProjectId(projectId);
        columnObj.setCreatedTime(new Date());
        columnObj.setUuid(userId);
        columnObj.setTenantId(tenantId);
        columnObj.setVisibility(true);
        columnObj.setColumnSecret(new ColumnSecret(name));
        return columnObj;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setColumnSecret(ColumnSecret columnSecret) {
        this.columnSecret = columnSecret;
    }

    public ColumnSecret getColumnSecret() { return columnSecret; }


    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getNameHash() {
        return nameHash;
    }

    public void setNameHash(String nameHash) {
        this.nameHash = nameHash;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(Date lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public boolean isVisibility() {return visibility;}

    public void setVisibility(boolean visibility) {this.visibility = visibility;}

    @Override
    public String toString() {
        return "Column{" + "id=" + id + ", nameHash=" + nameHash + ", position=" + position + ", projectId=" + projectId + ", createdTime=" + createdTime + ", lastUpdatedTime=" + lastUpdatedTime + '}';
    }

}
