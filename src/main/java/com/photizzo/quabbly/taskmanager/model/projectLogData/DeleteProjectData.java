package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class DeleteProjectData {

    private String deletedBy;

    private Date deletedDate;

    private String projectName;

    private String action;

    public DeleteProjectData(String deletedBy, Date deletedDate, String projectName, String action) {
        this.deletedBy = deletedBy;
        this.deletedDate = deletedDate;
        this.projectName = projectName;
        this.action = action;
    }

    @Override
    public String toString(){ return deletedBy + " " + action + projectName + " " + deletedDate;}
}
