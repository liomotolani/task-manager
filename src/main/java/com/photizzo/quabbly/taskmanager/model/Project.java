/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.photizzo.quabbly.taskmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.photizzo.quabbly.taskmanager.model.Secret.ProjectSecret;
import com.photizzo.quabbly.taskmanager.model.extras.Model;
import com.photizzo.quabbly.taskmanager.validators.interfaces.UniqueField;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;


@Document
public class Project extends Model {
     
    @NotNull
    private ProjectSecret projectSecret;

    @Indexed
    @JsonIgnore
    private String nameHash;

    private String createdBy;

    private String uniqueKey;

    private Date createdTime;
    private Date lastUpdatedTime;


    @JsonIgnore
    private String uuid;

    @Indexed
    @JsonIgnore
    private String tenantId;

    public static Project createProjectWithDefaults(String name,String description, String nameHash, String uuid, String tenantId, String createdBy) {
        Project projectObj = new Project();
        projectObj.setProjectSecret(new ProjectSecret(name,description));
        projectObj.setUuid(uuid);
        projectObj.setTenantId(tenantId);
        projectObj.setCreatedTime(new Date());
        projectObj.setNameHash(nameHash);
        projectObj.setCreatedBy(createdBy);
        return projectObj;
    }

    public Date getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(Date lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public String getNameHash() {
        return nameHash;
    }

    private void setNameHash(String nameHash) {
        this.nameHash = nameHash;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    private void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getUuid() {
        return uuid;
    }

    private void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTenantId() {
        return tenantId;
    }

    private void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public ProjectSecret getProjectSecret() {
        return projectSecret;
    }

    public void setProjectSecret(ProjectSecret projectSecret) {
        this.projectSecret = projectSecret;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUniqueKey() {return uniqueKey;}

    public void setUniqueKey(String uniqueKey) {this.uniqueKey = uniqueKey;}

    @Override
    public String toString() {
        return "Project{" + "id=" + getId() + ", projectSecret=" + projectSecret + ", createdTime=" + createdTime + ", uuid=" + uuid + ", tenantId=" + tenantId + '}';
    }

}
