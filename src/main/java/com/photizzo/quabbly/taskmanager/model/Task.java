package com.photizzo.quabbly.taskmanager.model;

import com.bol.secure.Encrypted;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.photizzo.quabbly.taskmanager.model.Secret.TaskSecret;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

public class Task {
    @Id
    private String id;

    @Field
    @Encrypted
    private TaskSecret taskSecret;

    @JsonIgnore
    @Field
    private ObjectId goalId;

    @Indexed
    private String projectId;

    private String taskUniqueKey;

    @Indexed
    private String columnId;

    private Date createdTime;

    private Date lastUpdatedTime;

    @JsonIgnore
    private String userId;

    private String userName;


    private  int position;

    @JsonIgnore
    private String uuid;

    @Indexed
    @Field
    @JsonIgnore
    private String tenantId;

    private Goal goal;

    public ObjectId getGoalId() {
        return goalId;
    }

    public void setGoalId(ObjectId goalId) {
        this.goalId = goalId;
    }

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName( ) {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TaskSecret getTaskSecret() {
        return taskSecret;
    }

    public void setTaskSecret(TaskSecret taskSecret) {
        this.taskSecret = taskSecret;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getTaskUniqueKey() {return taskUniqueKey;}

    public void setTaskUniqueKey(String taskUniqueKey) { this.taskUniqueKey = taskUniqueKey;}

    public String getColumnId() {
        return columnId;
    }

    public void setColumnId(String columnId) {
        this.columnId = columnId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(Date lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

}
