package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class UpdateDescriptionData {

    private String editedBy;

    private String action;

    private Date editedDate;

    private String taskName;

    private String projectName;

    public UpdateDescriptionData(String editedBy, String action, Date editedDate, String taskName, String projectName) {
        this.editedBy = editedBy;
        this.action = action;
        this.editedDate = editedDate;
        this.taskName = taskName;
        this.projectName = projectName;
    }

    @Override
    public String toString(){return editedBy+ " " + action + " " + taskName + " task description in " + projectName + " on " + editedDate;}
}
