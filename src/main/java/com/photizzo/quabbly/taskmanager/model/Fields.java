package com.photizzo.quabbly.taskmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.photizzo.quabbly.taskmanager.model.Secret.FieldsSecret;
import com.photizzo.quabbly.taskmanager.model.extras.Model;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class Fields extends Model {

    @NotNull
    @Indexed
    private FieldsSecret fieldsSecret;

    @JsonIgnore
    private String nameHash;

    private String projectId;

    @JsonIgnore
    @Indexed
    private String tenantId;

    @JsonIgnore
    @Indexed
    private String userId;

    @Indexed
    private Date createdTime;

    @Indexed
    private Date lastModifiedTime;

    public Fields() {
    }


    public static Fields createField(String name, String nameHash,String description, String type, String projectId, String userId, String tenantId) {
        Fields fieldObj = new Fields();
        fieldObj.setFieldsSecret(new FieldsSecret(name, description, type));
        fieldObj.setNameHash(nameHash);
        fieldObj.setTenantId(tenantId);
        fieldObj.setCreatedTime(new Date());
        fieldObj.setUserId(userId);
        fieldObj.setProjectId(projectId);

        return fieldObj;
    }
    public FieldsSecret getFieldsSecret() {return fieldsSecret;}

    public void setFieldsSecret(FieldsSecret fieldsSecret) {this.fieldsSecret = fieldsSecret;}

    public String getNameHash() {return nameHash;}

    public void setNameHash(String nameHash) {this.nameHash = nameHash;}

    public String getTenantId() {return tenantId;}

    public void setTenantId(String tenantId) {this.tenantId = tenantId;}

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getUserId() {return userId;}

    public void setUserId(String userId) {this.userId = userId;}

    public Date getCreatedTime() {return createdTime;}

    public void setCreatedTime(Date createdTime) { this.createdTime = createdTime;}

    public Date getLastModifiedTime() {return lastModifiedTime;}

    public void setLastModifiedTime(Date lastModifiedTime) {this.lastModifiedTime = lastModifiedTime;}
}
