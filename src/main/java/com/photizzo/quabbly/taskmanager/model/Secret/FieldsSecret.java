package com.photizzo.quabbly.taskmanager.model.Secret;

public class FieldsSecret {
    private String name;

    private String description;

    private String type;

    private String value;

    public FieldsSecret() {
    }

    public FieldsSecret(String name, String description, String type) {
        this.name = name;
        this.description = description;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {return type;
    }

    public void setType(String type) {this.type = type;}

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
