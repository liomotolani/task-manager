package com.photizzo.quabbly.taskmanager.model.extras;

import java.util.Date;
import java.util.Objects;

public class Watchers {
    private String userId;
    private String userName;
    private String userEmail;
    private Date createdTime;
    private Date lastUpdatedTime;


    public Watchers() {
    }

    public Watchers(String userId, String userName,String userEmail) {
        this.userId = userId;
        this.userName = userName;
        this.userEmail = userEmail;
        this.createdTime = new Date();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(Date lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Watchers watchers = (Watchers) o;
        return userId.equals(watchers.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId);
    }
}
