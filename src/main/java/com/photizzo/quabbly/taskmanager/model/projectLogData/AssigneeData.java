package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class AssigneeData {
    private String assignee;

    private Date time;

    private String assignedBy;

    private String action;

    private String taskName;

    private String projectName;

    public AssigneeData(String assignee, Date time, String assignedBy, String action, String taskName, String projectName) {
        this.assignee = assignee;
        this.time = time;
        this.assignedBy = assignedBy;
        this.action = action;
        this.taskName = taskName;
        this.projectName = projectName;
    }

    @Override
    public String toString() {
        return assignedBy + " " + action + " task " + taskName + " to " + assignee + " in project " + projectName + " on " + time;
    }
}
