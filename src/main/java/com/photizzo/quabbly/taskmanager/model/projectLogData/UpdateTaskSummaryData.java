package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class UpdateTaskSummaryData {
    private String editedBy;

    private String action;

    private Date editedDate;

    private String newtaskName;

    private String taskName;

    private String projectName;

    public UpdateTaskSummaryData(String editedBy, String action, Date editedDate, String taskName,String newtaskName, String projectName) {
        this.editedBy = editedBy;
        this.action = action;
        this.editedDate = editedDate;
        this.newtaskName = newtaskName;
        this.taskName = taskName;
        this.projectName = projectName;
    }

    @Override
    public String toString(){return editedBy+ " " + action + " " + taskName + " task summary to "+ newtaskName + " in " + projectName + " on " + editedDate;}
}
