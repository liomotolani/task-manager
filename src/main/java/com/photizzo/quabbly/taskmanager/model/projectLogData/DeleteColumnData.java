package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class DeleteColumnData {
    private String deletedBy;

    private Date deletedDate;

    private String action;

    private String columnName;

    private String projectName;

    public DeleteColumnData(String deletedBy,String action,String columnName, String projectName,Date deletedDate) {
        this.deletedBy = deletedBy;
        this.deletedDate = deletedDate;
        this.action = action;
        this.columnName = columnName;
        this.projectName = projectName;
    }

    @Override
    public String toString(){return deletedBy +" "+ action +" "+ columnName +" "+ "in" + projectName +" "+ "on" + deletedDate; }
}
