package com.photizzo.quabbly.taskmanager.model.extras;

public enum Capabilities {

    GENERAL,
    REARRANGE_COLUMNS,
    RENAME_COLUMN,
    REARRANGE_TASKS,
    UPDATE_TASK,
    DELETE_COLUMN,
    DELETE_ATTACHMENT,
    DELETE_COMMENT
}
