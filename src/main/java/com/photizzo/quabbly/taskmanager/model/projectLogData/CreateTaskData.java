package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class CreateTaskData {
    private String createdBy;

    private String taskName;

    private String projectName;

    private Date createdTime;

    private String action;

    public CreateTaskData(String createdBy, String taskName, String projectName, Date createdTime, String action) {
        this.createdBy = createdBy;
        this.taskName = taskName;
        this.projectName = projectName;
        this.createdTime = createdTime;
        this.action = action;
    }

    @Override
    public String toString() {
        return createdBy + " " + action + " " + taskName +  " task in " +  projectName + " on " + createdTime ;
    }
}
