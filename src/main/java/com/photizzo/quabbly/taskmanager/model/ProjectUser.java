package com.photizzo.quabbly.taskmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.photizzo.quabbly.taskmanager.model.Secret.ProjectUserSecret;
import com.photizzo.quabbly.taskmanager.model.extras.Model;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
public class ProjectUser extends Model {

    @JsonIgnore
    private String userId;

    @JsonIgnore
    private String projectId;

    @JsonIgnore
    private String roleId;

    private Date createdTime;

    private Date lastUpdatedTime;

    private ProjectUserSecret projectUserSecret;


    @JsonIgnore
    private String uuid;

    @Indexed
    @JsonIgnore
    private String tenantId;

    public static ProjectUser createProjectUserWithDefaults(String userFullname, String userEmail, String projectId, String userId, String roleId, String uuid, String tenantId,Role role) {
        ProjectUser projectUser = new ProjectUser();
        projectUser.setProjectUserSecret(new ProjectUserSecret(userFullname, userEmail,role));
        projectUser.setUuid(uuid);
        projectUser.setTenantId(tenantId);
        projectUser.setCreatedTime(new Date());
        projectUser.setUserId(userId);
        projectUser.setRoleId(roleId);
        projectUser.setProjectId(projectId);

        return projectUser;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Date getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(Date lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public ProjectUserSecret getProjectUserSecret() {
        return projectUserSecret;
    }

    public void setProjectUserSecret(ProjectUserSecret projectUserSecret) {
        this.projectUserSecret = projectUserSecret;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }
}
