package com.photizzo.quabbly.taskmanager.model.projectLogData;

import java.util.Date;

public class AddProjectToUserData {

    private String projectUser;

    private Date addedDate;

    private String addedBy;

    private String action;

    private String projectName;

    public AddProjectToUserData(String projectUser, Date addedDate, String addedBy, String action, String projectName) {
        this.projectUser = projectUser;
        this.addedDate = addedDate;
        this.addedBy = addedBy;
        this.action = action;
        this.projectName = projectName;
    }

    @Override
    public String toString() {
        return addedBy + " " + action + projectUser + " to " + projectName + " on " + addedDate;
    }
}
