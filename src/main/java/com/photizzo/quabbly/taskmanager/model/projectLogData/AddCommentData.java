package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class AddCommentData {
    private String commentBy;

    private Date commentDate;

    private String taskName;

    private String projectName;

    private String action;

    public AddCommentData(String commentBy, Date commentDate, String taskName, String projectName, String action) {
        this.commentBy = commentBy;
        this.commentDate = commentDate;
        this.taskName = taskName;
        this.projectName = projectName;
        this.action = action;
    }

    @Override
    public String toString() {
        return  commentBy + " " + action + " on task " + taskName + " in project" + projectName + " on " + commentDate;
    }

}
