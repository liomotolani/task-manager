package com.photizzo.quabbly.taskmanager.model.Secret;

import java.util.List;

public class RoleSecret {

    private String name;
    private List<String> capabilities;

    public RoleSecret() {
    }

    public RoleSecret(String name, List<String> capabilities) {
        this.name = name;
        this.capabilities = capabilities;
    }

    public void updateFieldsByDefault(String name, List<String> capabilities) {
        this.name = name;
        this.capabilities= capabilities;
    }


    public List<String> getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(List<String> capabilities) {
        this.capabilities = capabilities;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
