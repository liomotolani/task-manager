/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.photizzo.quabbly.taskmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.photizzo.quabbly.taskmanager.model.Secret.RoleSecret;
import com.photizzo.quabbly.taskmanager.model.extras.Model;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
import java.util.Objects;


@Document
public class Role extends Model {


    @NotNull
    private RoleSecret roleSecret;

    @Indexed
    @JsonIgnore
    private String nameHash;


    private Date createdTime;

    private Date lastUpdatedTime;


    @JsonIgnore
    private String uuid;

    @Indexed
    @JsonIgnore
    private String tenantId;

    public static Role createRoleWithDefaults(String name, List<String> capabilities, String nameHash, String uuid, String tenantId) {
        Role roleObj = new Role();
        roleObj.setRoleSecret(new RoleSecret(name, capabilities));
        roleObj.setUuid(uuid);
        roleObj.setTenantId(tenantId);
        roleObj.setCreatedTime(new Date());
        roleObj.setNameHash(nameHash);

        return roleObj;
    }

    public Date getLastUpdatedTime() {
        return lastUpdatedTime;
    }

    public void setLastUpdatedTime(Date lastUpdatedTime) {
        this.lastUpdatedTime = lastUpdatedTime;
    }

    public String getNameHash() {
        return nameHash;
    }

    private void setNameHash(String nameHash) {
        this.nameHash = nameHash;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    private void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public String getUuid() {
        return uuid;
    }

    private void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getTenantId() {
        return tenantId;
    }

    private void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public RoleSecret getRoleSecret() {
        return roleSecret;
    }

    private void setRoleSecret(RoleSecret roleSecret) {
        this.roleSecret = roleSecret;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return Objects.equals(roleSecret, role.roleSecret) &&
                Objects.equals(nameHash, role.nameHash) &&
                Objects.equals(createdTime, role.createdTime) &&
                Objects.equals(lastUpdatedTime, role.lastUpdatedTime) &&
                Objects.equals(uuid, role.uuid) &&
                Objects.equals(tenantId, role.tenantId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleSecret, nameHash, createdTime, lastUpdatedTime, uuid, tenantId);
    }
}
