package com.photizzo.quabbly.taskmanager.model.extras;

public enum Type {
    h,
    d,
    w
}
