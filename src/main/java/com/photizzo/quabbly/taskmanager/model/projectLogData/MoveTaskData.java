package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class MoveTaskData {
    private String movedBy;

    private String oldColumnName;

    private String newColumnName;

    private Date date;

    private String taskName;

    private  String projectName;

    private String action;

    public MoveTaskData(String movedBy, String oldColumnName, String newColumnName, Date date, String taskName, String projectName, String action) {
        this.movedBy = movedBy;
        this.oldColumnName = oldColumnName;
        this.newColumnName = newColumnName;
        this.date = date;
        this.taskName = taskName;
        this.projectName = projectName;
        this.action = action;
    }

    @Override
    public String toString() {
        return  movedBy + " " + action + " task " +  taskName + " from column " + oldColumnName + " to column " + newColumnName + " in project " + projectName + " on "+ date ;

    }
}
