package com.photizzo.quabbly.taskmanager.model.projectLogData;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class WatchTaskData {
    private String watcher;

    private Date time;

    private String action;

    private String taskName;

    private String projectName;

    public WatchTaskData(String watcher, Date time, String action, String taskName, String projectName) {
        this.watcher = watcher;
        this.time = time;
        this.action = action;
        this.taskName = taskName;
        this.projectName = projectName;
    }


    @Override
    public String toString() {
        return watcher + " " + action + " task " + taskName + " in project " + projectName + " on " + time;
    }
}
