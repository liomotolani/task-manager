/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.photizzo.quabbly.taskmanager.model.Secret;

import com.bol.secure.Encrypted;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 *
 * @author CROWN INTERACTIVE
 */
public class ProjectSecret {
    
    @NotNull
    @Encrypted
    private String name;

    private String description;

    private List<Object> projectLog;


    public ProjectSecret() {
    }

    public ProjectSecret(String name,String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {return description;}

    public void setDescription(String description) {this.description = description;}

    public List<Object> getProjectLog() {
        return projectLog;
    }

    public void setProjectLog(List<Object> projectLog) {
        this.projectLog = projectLog;
    }
}
