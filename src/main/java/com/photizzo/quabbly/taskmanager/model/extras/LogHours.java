package com.photizzo.quabbly.taskmanager.model.extras;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.photizzo.quabbly.taskmanager.model.Secret.GoalSecret;
import com.photizzo.quabbly.taskmanager.model.Secret.LogHourSecret;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class LogHours {


    @NotNull
    @Indexed
    private LogHourSecret logHourSecret;


    @JsonIgnore
    @Indexed
    private String tenantId;

    @JsonIgnore
    @Indexed
    private String userId;

    @Indexed
    private String projectId;

    @Indexed
    private String taskId;

    @Indexed
    private Date createdDate;

    @Indexed
    private Date lastModifiedDate;


    public LogHours() {
    }

    public static LogHours createLogHour(int number,String type,Date logDate,String createdBy,String projectId,String taskId,String userId,String tenantId){
        LogHours logHoursObj = new LogHours();
        logHoursObj.setLogHourSecret(new LogHourSecret(number,type,logDate,createdBy));
        logHoursObj.setProjectId(projectId);
        logHoursObj.setTaskId(taskId);
        logHoursObj.setTenantId(tenantId);
        logHoursObj.setUserId(userId);
        logHoursObj.setCreatedDate(new Date());
        return logHoursObj;
    }

    public LogHourSecret getLogHourSecret() {
        return logHourSecret;
    }

    public void setLogHourSecret(LogHourSecret logHourSecret) {
        this.logHourSecret = logHourSecret;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Date lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Date getCreatedDate() {return createdDate; }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

}
