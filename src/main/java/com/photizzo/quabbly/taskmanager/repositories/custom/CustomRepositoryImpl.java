package com.photizzo.quabbly.taskmanager.repositories.custom;

import com.photizzo.quabbly.taskmanager.model.Task;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.stereotype.Repository;

import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.query.Criteria.where;

@Repository
public class CustomRepositoryImpl implements CustomRepository{


    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<Task> fetchTasksByTenantId(String tenantId) {


        LookupOperation columnLookup = LookupOperation.newLookup()

                .from("column")
                .localField("columnId")
                .foreignField("_id")
                .as("columns");


        MatchOperation matchOperation = match(where("tenantId").is(tenantId));

        Aggregation aggregation = Aggregation.newAggregation(columnLookup, matchOperation);
        AggregationResults<Task> res = mongoTemplate.aggregate(aggregation, "task", Task.class);
        return res.getMappedResults();

    }


    @Override
    public Task fetchTaskByTenantIdAndId(String tenantId, String id) {

        LookupOperation columnLookup = LookupOperation.newLookup()

                .from("column")
                .localField("columnId")
                .foreignField("_id")
                .as("columns");

        LookupOperation goalLookup = LookupOperation.newLookup()

                .from("goal")
                .localField("goalId")
                .foreignField("_id")
                .as("goal");

        MatchOperation matchOperation = match(where("_id").is(new ObjectId(id)).and("tenantId").is(tenantId));

        Aggregation aggregation = Aggregation.newAggregation(columnLookup, goalLookup, matchOperation);
        AggregationResults<Task> res = mongoTemplate.aggregate(aggregation, "task", Task.class);

        return res.getUniqueMappedResult();
    }



}

