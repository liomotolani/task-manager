package com.photizzo.quabbly.taskmanager.repositories;

import com.photizzo.quabbly.taskmanager.model.Task;
import com.photizzo.quabbly.taskmanager.repositories.custom.CustomRepository;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface TaskRepository extends CrudRepository<Task,String> , CustomRepository {
    List<Task> findByTenantId(String tenantId, Pageable pageable);

    Task findFirstByProjectIdOrderByCreatedTimeDesc(String projectId);

    Task findByTenantIdAndId(String tenantId ,String id);

    List<Task> findByTenantIdAndProjectIdAndId(String tenantId,String projectId,String taskId);

    List <Task> countByTenantIdAndProjectId(String tenantId,String id);

    List<Task> findByTenantIdAndProjectId(String tenantId, String projectId);

    List<Task> findByTenantIdAndProjectId(String tenantId, String projectId, Pageable pageable);

    List<Task> findByTenantIdAndColumnId(String tenantId, String columnId);

    Long countByTenantIdAndColumnId(String tenantId, String columnId);

    Task findFirstByTenantIdAndColumnIdOrderByPositionDesc(String tenantId , ObjectId id);

    List<Task> findByTenantIdAndGoalId(String tenantId, ObjectId goalId);

}
