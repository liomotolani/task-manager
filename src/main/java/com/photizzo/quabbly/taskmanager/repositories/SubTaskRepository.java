package com.photizzo.quabbly.taskmanager.repositories;

import com.photizzo.quabbly.taskmanager.model.extras.SubTask;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SubTaskRepository extends CrudRepository<SubTask,String> {
    void delete(SubTask subtask);
}
