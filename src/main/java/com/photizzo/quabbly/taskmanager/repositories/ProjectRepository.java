package com.photizzo.quabbly.taskmanager.repositories;

import com.photizzo.quabbly.taskmanager.model.Project;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends CrudRepository<Project, String>{

    Project findByTenantIdAndId(String tenantId, String Id);

    List<Project> findByTenantId(String tenantId, Pageable pageable);

    List<Project> findByTenantIdAndUniqueKeyStartingWith(String tenantId,String uniqueKey, Pageable pageable);


}