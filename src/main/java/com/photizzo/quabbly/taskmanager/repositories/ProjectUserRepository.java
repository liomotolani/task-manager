package com.photizzo.quabbly.taskmanager.repositories;


import com.photizzo.quabbly.taskmanager.model.ProjectUser;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectUserRepository extends CrudRepository<ProjectUser, String> {

    List<ProjectUser> findByTenantIdAndUserId(String tenantId, String userId);



    List<ProjectUser> findByTenantId(String tenantId, Pageable pageable);

    List<ProjectUser> findByTenantIdAndProjectId(String tenantId, String projectId);

    ProjectUser findByTenantIdAndProjectIdAndUserId(String tenantId,String projectId,String userId);

    List<ProjectUser> findByTenantIdAndIdAndUserId(String tenantId,String projectUserId,String userId);

    List<ProjectUser> findByTenantIdAndId(String tenantId,String projectUserId);

}