package com.photizzo.quabbly.taskmanager.repositories;

import com.photizzo.quabbly.taskmanager.model.User;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User,String> {
    User findByEmail(String email);

    @Query(value="{ 'organisationId' : ?0 }",fields="{ 'firstname' : 1, 'lastname' : 1}")
    User findByOrganisationIdAndId(String id);
}
