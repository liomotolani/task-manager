package com.photizzo.quabbly.taskmanager.repositories;

import com.photizzo.quabbly.taskmanager.model.Fields;
import com.photizzo.quabbly.taskmanager.model.extras.FieldType;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FieldsRepository extends CrudRepository<Fields,String> {
    List<Fields> findByTenantIdAndProjectId(String tenantId, String projectId);

    Fields findByTenantIdAndId(String tenantId ,String fieldId);

}
