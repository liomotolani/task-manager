package com.photizzo.quabbly.taskmanager.repositories;

import com.photizzo.quabbly.taskmanager.model.extras.LogHours;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LogHourRepository extends CrudRepository<LogHours,String> {
 List<LogHours> findByTenantIdAndTaskId(String tenantId,String taskId);

 LogHours findByTaskId(String taskId);

}
