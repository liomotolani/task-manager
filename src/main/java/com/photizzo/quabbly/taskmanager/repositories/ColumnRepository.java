package com.photizzo.quabbly.taskmanager.repositories;

import com.photizzo.quabbly.taskmanager.model.Column;
import java.util.List;
import org.springframework.data.repository.CrudRepository;


public interface ColumnRepository extends CrudRepository<Column, String>{
    
    Column findByTenantIdAndId(String tenantId ,String columnId);
    List<Column> findByProjectId(String projectId);
    Column findFirstByProjectIdOrderByPositionDesc(String projectId);

    Column findFirstByProjectIdOrderByPositionAsc(String projectId);

    List<Column> findAllByProjectIdOrderByPositionAsc(String projectId);

    List<Column> findByTenantIdAndProjectId(String tenantId, String projectId);




    Column findByTenantIdAndProjectIdAndId(String tenantId, String projectId, String columnId);
}