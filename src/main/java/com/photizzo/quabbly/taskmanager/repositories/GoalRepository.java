package com.photizzo.quabbly.taskmanager.repositories;

import com.photizzo.quabbly.taskmanager.model.Goal;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GoalRepository extends CrudRepository<Goal,String> {
    Goal findByTenantIdAndId(String tenantId,String Id);
    List<Goal> findByTenantIdAndProjectIdAndTaskId(String tenantId,String projectId,String taskId);
    List<Goal> findByTenantId(String tenantId, Pageable pageable);

    List<Goal> findByTenantIdAndProjectId(String tenantId, String projectId);

}
