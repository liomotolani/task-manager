package com.photizzo.quabbly.taskmanager.repositories;

import com.photizzo.quabbly.taskmanager.model.Role;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends CrudRepository<Role, String> {

    Role findByTenantIdAndId(String tenantId, String Id);



    List<Role> findByTenantId(String tenantId, Pageable pageable);


}