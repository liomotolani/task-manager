package com.photizzo.quabbly.taskmanager.repositories;

import com.photizzo.quabbly.taskmanager.model.extras.Attachments;
import org.springframework.data.repository.CrudRepository;

public interface AttachmentRepository extends CrudRepository<Attachments,String> {

}
