package com.photizzo.quabbly.taskmanager.repositories.custom;

import com.photizzo.quabbly.taskmanager.model.Task;

import java.util.List;

public interface CustomRepository {

    public Task fetchTaskByTenantIdAndId(String tenantId, String id);

    public List<Task> fetchTasksByTenantId(String tenantId);




}
