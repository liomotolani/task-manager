package com.photizzo.quabbly.taskmanager.controller;


import com.photizzo.quabbly.taskmanager.dto.output.BasicResponseDTO;
import com.photizzo.quabbly.taskmanager.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("v1")
public class ProjectUserController extends Controller{

    @Autowired
    private ProjectService projectService;

    private static final int ITEMS_PER_PAGE = 50;

    @GetMapping("/{userId}/projects")
    public BasicResponseDTO projectList(@PathVariable(name ="userId") String userId) {
        return responseWithUpdatedHttpStatus(projectService.fetchProjects(userId));
    }
}
