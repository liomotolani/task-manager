package com.photizzo.quabbly.taskmanager.controller;

import com.photizzo.quabbly.taskmanager.dto.input.AttachUserToProjectInputDTO;
import com.photizzo.quabbly.taskmanager.dto.input.CreateProjectInputDTO;
import com.photizzo.quabbly.taskmanager.dto.input.CreateRoleInputDTO;
import com.photizzo.quabbly.taskmanager.dto.input.UpdateRoleInputDTO;
import com.photizzo.quabbly.taskmanager.dto.output.*;
import com.photizzo.quabbly.taskmanager.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping("v1/admin")
public class AdminController extends Controller {

    @Autowired
    private AdminService adminService;

    private static final int ITEMS_PER_PAGE = 50;

    @PostMapping("/role")
    public RoleResponseDTO createRole(@RequestBody @Valid CreateRoleInputDTO dto) {
        return responseWithUpdatedHttpStatus(adminService.createRole(dto));
    }

    @GetMapping("/capabilities")
    public CapabilitiesListResponseDTO fetchCapabilities() {
        return responseWithUpdatedHttpStatus(adminService.fetchCapabilities());
    }

    @PostMapping("/project/{id}/attach_user")
    public BasicResponseDTO attachUserToProject(@PathVariable("id") String projectId, @RequestBody @Valid AttachUserToProjectInputDTO dto) {
        return responseWithUpdatedHttpStatus(adminService.attachUserToProject(dto, projectId));
    }

    @PostMapping("/project/create")
    public ProjectResponseDTO createProject(@RequestBody @Valid CreateProjectInputDTO projectInputDTO) {
        return responseWithUpdatedHttpStatus(adminService.createProject(projectInputDTO));
    }

    @GetMapping("/project/{id}/attached_users")
    public BasicResponseDTO fetchProjectUsers(@PathVariable(name = "id") String projectId ){
        return responseWithUpdatedHttpStatus(adminService.fetchProjectUsers(projectId));
    }


    @GetMapping("/projects")
    public ProjectListResponseDTO projectList(@PageableDefault(value = ITEMS_PER_PAGE) Pageable pageable) {
        return responseWithUpdatedHttpStatus(adminService.fetchProjects(pageable));
    }

    @GetMapping("/roles")
    public RoleListResponseDTO roleList(@PageableDefault(value = ITEMS_PER_PAGE) Pageable pageable) {
        return responseWithUpdatedHttpStatus(adminService.fetchRoles(pageable));
    }
    @PutMapping("/role/{roleId}")
    public RoleResponseDTO updateRole(@PathVariable(name = "roleId") String roleId , @RequestBody @Valid UpdateRoleInputDTO updateRoleInputDTO){
        return responseWithUpdatedHttpStatus(adminService.updateRole(updateRoleInputDTO,roleId));
    }



}
