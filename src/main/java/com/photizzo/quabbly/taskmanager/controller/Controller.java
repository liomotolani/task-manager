package com.photizzo.quabbly.taskmanager.controller;

import com.photizzo.quabbly.taskmanager.dto.output.StandardResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;

import javax.servlet.http.HttpServletResponse;

@PreAuthorize("hasPermission(#projectId,'')")
class Controller {

    @Autowired
    private HttpServletResponse response;

    <T extends StandardResponseDTO> T responseWithUpdatedHttpStatus(T responseDTO) {
        switch (responseDTO.getStatus()) {
            case SUCCESS:
                response.setStatus(HttpStatus.OK.value());
                break;
            case NO_CONTENT:
                response.setStatus(HttpStatus.NO_CONTENT.value());
                break;
            case CONFLICT:
                response.setStatus(HttpStatus.CONFLICT.value());
                break;
            case CREATED:
                response.setStatus(HttpStatus.CREATED.value());
                break;
            case NOT_FOUND:
                response.setStatus(HttpStatus.NOT_FOUND.value());
                break;
            default:
                response.setStatus(HttpStatus.BAD_REQUEST.value());
        }

        return responseDTO;
    }

    @Deprecated
    <T extends StandardResponseDTO> void updateHttpStatus(T responseDTO, HttpServletResponse response) {
        switch (responseDTO.getStatus()) {
            case SUCCESS:
                response.setStatus(HttpStatus.OK.value());
                break;
            case CONFLICT:
                response.setStatus(HttpStatus.CONFLICT.value());
                break;
            case CREATED:
                response.setStatus(HttpStatus.CREATED.value());
                break;
            default:
                response.setStatus(HttpStatus.BAD_REQUEST.value());
        }
    }
}
