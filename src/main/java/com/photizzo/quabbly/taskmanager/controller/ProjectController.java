package com.photizzo.quabbly.taskmanager.controller;

import com.photizzo.quabbly.taskmanager.dto.input.*;
import com.photizzo.quabbly.taskmanager.dto.output.*;
import com.photizzo.quabbly.taskmanager.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping("v1/project/{projectId}")
class ProjectController extends Controller {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HttpServletResponse response;

    private static final int ITEMS_PER_PAGE = 50;

    @CrossOrigin
    @PutMapping("/update")
    public ProjectResponseDTO updateProject(@PathVariable(name = "projectId") String projectId,@RequestBody @Valid UpdateProjectInputDTO dto){
        return responseWithUpdatedHttpStatus(projectService.updateProject(projectId,dto));
    }

    @PutMapping()
    public ColumnResponseDTO addColumnToProject(@PathVariable(name = "projectId") String projectId, @RequestBody @Valid AddColumnInputDTO addColumnInputDTO){
        return responseWithUpdatedHttpStatus(projectService.addColumn(addColumnInputDTO ,projectId));
    }

    @GetMapping()
    public ProjectResponseDTO fetchProject(@PathVariable(name ="projectId") String projectId){
        return responseWithUpdatedHttpStatus(projectService.fetchProject(projectId));

    }


    @CrossOrigin
    @PostMapping("/goal")
    public GoalResponseDTO createGoal(@PathVariable(name = "projectId") String projectId, @RequestBody @Valid CreateGoalInputDTO dto) {
        return responseWithUpdatedHttpStatus(projectService.createGoal(dto,projectId));
    }

    @CrossOrigin
    @PutMapping("/goal/{goalId}/attach-task")
    public BasicResponseDTO attachTaskToGoal(@PathVariable(name = "goalId") String goalId,@RequestBody @Valid AttachTaskToGoalInputDTO dto) {
        return responseWithUpdatedHttpStatus(projectService.attachTaskToGoal(dto,goalId));
    }

    @GetMapping("/goals")
    public GoalListResponseDTO fetchList(@PathVariable(name ="projectId") String projectId) {
        return responseWithUpdatedHttpStatus(projectService.fetchGoals(projectId));
    }

    @PutMapping("/goal/{goalId}")
    public GoalResponseDTO updateGoal(@PathVariable(name ="projectId") String projectId,@PathVariable(name = "goalId") String goalId , @RequestBody @Valid UpdateGoalInputDTO updateGoalInputDTO){
        return responseWithUpdatedHttpStatus(projectService.updateGoal(updateGoalInputDTO,goalId,projectId));

    }

    @PutMapping("/column/{columnId}")
    public ColumnResponseDTO renameColumn(@PathVariable(name ="projectId") String projectId,@PathVariable(name ="columnId") String columnId, @RequestBody @Valid RenameColumnInputDTO renameColumnInputDTO ){

        return responseWithUpdatedHttpStatus(projectService.renameColumn(renameColumnInputDTO,columnId,projectId));

    }

    @GetMapping("/columns")
    public ColumnListResponseDTO columnList(@PathVariable(name ="projectId") String projectId){
        return responseWithUpdatedHttpStatus( projectService.fetchColumns(projectId));
    }

    @PostMapping("/task")
    public TaskResponseDTO createTask(@RequestBody @Valid CreateTaskInputDTO createTaskInputDTO,@PathVariable(name ="projectId") String projectId){

        return responseWithUpdatedHttpStatus(projectService.createTask(createTaskInputDTO , projectId));

    }

    @PostMapping("/task/{taskId}/log-hour")
    public BasicResponseDTO createLogHour(@PathVariable(name ="projectId") String projectId,@RequestBody @Valid LogHourInputDTO logHourInputDTO,@PathVariable(name = "taskId") String taskId ){

        return responseWithUpdatedHttpStatus(projectService.createLogHour(logHourInputDTO,taskId,projectId));

    }

    @GetMapping("/task/{taskId}/log-hour")
    public BasicResponseDTO fetchLogHour(@PathVariable(name = "taskId") String taskId ){
        return responseWithUpdatedHttpStatus(projectService.fetchLogHour(taskId));

    }

    @PostMapping("/fields")
    public FieldsResponseDTO createFields(@RequestBody @Valid CreateFieldsInputDTO createFieldsInputDTO,@PathVariable(name ="projectId") String projectId){

        return responseWithUpdatedHttpStatus(projectService.createFields(createFieldsInputDTO , projectId));

    }

    @GetMapping("/fields")
    public FieldListResponseDTO fieldList(@PathVariable(name ="projectId") String projectId){
        return responseWithUpdatedHttpStatus( projectService.fetchFields(projectId));
    }

    @PutMapping("/field/{fieldId}")
    public FieldsResponseDTO updateFields(@RequestBody @Valid UpdateFieldInputDTO updateFieldInputDTO,@PathVariable(name ="fieldId") String fieldId){
        return responseWithUpdatedHttpStatus(projectService.updateFields(updateFieldInputDTO , fieldId));

    }

    @Transactional
    @PutMapping("/task/{taskId}")
    public TaskResponseDTO updateTasks(@PathVariable(name ="projectId") String projectId,@PathVariable(name = "taskId") String taskId , @RequestBody @Valid UpdateTaskInputDTO updateTaskInputDTO){
        return responseWithUpdatedHttpStatus(projectService.updateTask(updateTaskInputDTO,taskId,projectId));

    }


    @GetMapping("/task/{taskId}")
    public TaskResponseDTO viewTask(@PathVariable(name ="projectId") String projectId,@PathVariable(name= "taskId") String taskId ){
        return responseWithUpdatedHttpStatus(projectService.fetchTask(taskId,projectId));

    }

    @GetMapping("/goal/{goalId}")
    public GoalResponseDTO viewGoal(@PathVariable(name= "goalId") String goalId ){
        return responseWithUpdatedHttpStatus(projectService.fetchGoal(goalId));

    }
    @GetMapping("/tasks")
    public TaskListResponseDTO taskList(@PathVariable(name ="projectId") String projectId){
        return responseWithUpdatedHttpStatus( projectService.fetchTasks(projectId));
    }

    @PutMapping("/column/reposition")
    public ColumnListResponseDTO repositionColumns(@PathVariable(name = "projectId") String projectId,@RequestBody @Valid RepositionColumnInputDTO dto){
        return responseWithUpdatedHttpStatus(projectService.repositionColumns(projectId,dto));


    }
    @CrossOrigin
    @PutMapping("/comment/{taskId}/{commentUuid}")
    public CommentListResponseDTO updateComment(@PathVariable(name ="projectId") String projectId,@PathVariable(name ="commentUuid") String commentUuid,@PathVariable(name ="taskId") String taskId, @RequestBody @Valid UpdateCommentInputDTO dto){
        return responseWithUpdatedHttpStatus(projectService.updateComment(dto,commentUuid,taskId,projectId));

    }
    @CrossOrigin
    @PutMapping("/subtask/{taskId}/{subtaskUuid}")
    public SubTaskListResponseDTO updateSubTask(@PathVariable(name ="projectId") String projectId,@PathVariable(name="taskId")String taskId,@PathVariable(name ="subtaskUuid")String subTaskUuid, @RequestBody @Valid UpdateSubTaskInputDTO dto){
        return responseWithUpdatedHttpStatus(projectService.updateSubTask(dto,subTaskUuid,taskId,projectId));

    }

    @CrossOrigin
    @PutMapping("/task/{taskId}/move")
    public TaskResponseDTO moveTask(@PathVariable(name ="projectId") String projectId,@PathVariable(name = "taskId")String taskId,@RequestBody @Valid MoveTaskInputDTO dto){
        return responseWithUpdatedHttpStatus(projectService.moveTask(dto,taskId,projectId));

    }
    @CrossOrigin
    @DeleteMapping("/comment/{taskId}/{commentUuid}")
    public BasicResponseDTO deleteComment(@PathVariable(name ="projectId") String projectId,@PathVariable(name = "taskId")String taskId,@PathVariable(name = "commentUuid")String commentUuid){
        return responseWithUpdatedHttpStatus(projectService.deleteComment(taskId,commentUuid,projectId));


    }

    @CrossOrigin
    @DeleteMapping("/attachment/{taskId}/{url}")
    public BasicResponseDTO deleteAttachment(@PathVariable(name ="projectId") String projectId,@PathVariable(name = "taskId")String taskId,@PathVariable(name = "url")String url){
       return responseWithUpdatedHttpStatus( projectService.deleteAttachment(taskId,url,projectId));
    }


    @CrossOrigin
    @DeleteMapping("/subtask/{taskId}/{subTaskUuid}/delete")
    public BasicResponseDTO deleteSubTask(@PathVariable(name ="projectId") String projectId,@PathVariable(name = "taskId")String taskId,@PathVariable(name = "subTaskUuid") String subTaskUuid){
        return responseWithUpdatedHttpStatus(projectService.deleteSubTask(taskId,subTaskUuid,projectId));
    }

    @CrossOrigin
    @DeleteMapping("/column/{columnId}/delete")
    public BasicResponseDTO deleteColumn(@PathVariable(name ="projectId") String projectId,@PathVariable(name = "columnId")String columnId){
        return responseWithUpdatedHttpStatus(projectService.deleteColumn(columnId,projectId));
    }

    @CrossOrigin
    @DeleteMapping("/task/{taskId}")
    public BasicResponseDTO deleteTask(@PathVariable(name ="projectId") String projectId,@PathVariable(name = "taskId")String taskId){
        return responseWithUpdatedHttpStatus(projectService.deleteTask(taskId,projectId));
    }

    @CrossOrigin
    @DeleteMapping()
    public BasicResponseDTO deleteProject(@PathVariable(name = "projectId") String projectId){
        return responseWithUpdatedHttpStatus(projectService.deleteProject(projectId));
    }

    @CrossOrigin
    @PatchMapping("/column/{columnId}/hide")
    public HideColumnResponseDTO UpdateColumnAsHidden(@PathVariable(name ="projectId") String projectId,@PathVariable(name = "columnId")String columnId,@RequestBody @Valid MarkColumnInputDTO dto){
        return responseWithUpdatedHttpStatus(projectService.markColumnAsHidden(dto,columnId,projectId));
    }

    @CrossOrigin
    @PatchMapping("/column/{columnId}/visible")
    public UnHideColumnResponseDTO UpdateColumnAsVisible(@PathVariable(name ="projectId") String projectId,@PathVariable(name = "columnId")String columnId,@RequestBody @Valid MarkColumnInputDTO dto){
        return responseWithUpdatedHttpStatus(projectService.markColumnAsVisible(dto,columnId,projectId));
    }


}
